﻿/*
*	Shree Soft Solutions - Pawn Shop Management System
*--------------------------------------------------------------------------------------
*	Database Script for New Installation
*	Author	:	Shree Soft Solutions
*	Date	:	06-MAY-2015
*	Version	:	1.0
*--------------------------------------------------------------------------------------
*/
--	Description	:	PawnShopManagementSystem Database Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE DATABASE [PSMS];
GO

--	Description	:	Use PawnShopManagementSystem Database
USE [PSMS]

--	Description	:	Language Table Creation
--	Reference	:	https://msdn.microsoft.com/en-us/library/ee825488%28v=cs.20%29.aspx
--	Reference	:	http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Language](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Code] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	UI Theme Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Theme](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Code] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Theme] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	UI Color Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Color](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Code] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Color] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Currency Table Creation
--	Reference	:	http://en.wikipedia.org/wiki/ISO_4217
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Currency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Code] [nvarchar](50) NULL,
	[Display] [nvarchar](50) NULL,
	[PrintText] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_Currency] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Country Table Creation
--	Reference	:	https://msdn.microsoft.com/en-us/library/ms707477%28v=vs.85%29.aspx
--	Reference	:	http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Code] [nvarchar] (50) NULL,
	[PrintText] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_Country] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	State Table Creation
--	Reference	:	http://en.wikipedia.org/wiki/ISO_3166-2:IN
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[State](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryId] [int] NULL,	
	[Name] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](50) NULL,
	[PrintText] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_State] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[State]  WITH CHECK ADD  CONSTRAINT [FK_State_Country] FOREIGN KEY([CountryId]) REFERENCES [dbo].[Country] ([Id]);
GO

--	Description	:	City Table Creation
--	Reference	:	http://en.wikipedia.org/wiki/List_of_districts_in_Tamil_Nadu
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[City](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StateId] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](50) NULL,
	[PrintText] [nvarchar](50) NULL,	
	[Status] [bit] NULL,
	CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_City] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[City]  WITH CHECK ADD  CONSTRAINT [FK_City_State] FOREIGN KEY([StateId]) REFERENCES [dbo].[State] ([Id]);
GO

--	Description	:	Gender Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Gender](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,	
	[Status] [bit] NULL,
	CONSTRAINT [PK_Gender] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Item Type Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[ItemType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Display] [nvarchar](50) NULL,
	[PrintText] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_ItemType] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_ItemType] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Item Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Item](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemTypeId] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](50) NULL,
	[Display] [nvarchar](50) NULL,
	[PrintText] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_Item] UNIQUE NONCLUSTERED ([ItemTypeId] ASC,[Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_ItemType] FOREIGN KEY([ItemTypeId]) REFERENCES [dbo].[ItemType] ([Id]);
GO

--	Description	:	Item Type Group Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[ItemTypeGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Display] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_ItemTypeGroup] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_ItemTypeGroup] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Item Type and Item Type Group - Mapping Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[ItemTypeGroupMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemTypeGroupId] [int] NULL,
	[ItemTypeId] [int] NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_ItemTypeGroupMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[ItemTypeGroupMapping]  WITH CHECK ADD  CONSTRAINT [FK_ItemGroupMapping_ItemType] FOREIGN KEY([ItemTypeId]) REFERENCES [dbo].[ItemType] ([Id]);
GO
ALTER TABLE [dbo].[ItemTypeGroupMapping]  WITH CHECK ADD  CONSTRAINT [FK_ItemGroupMapping_ItemTypeGroup] FOREIGN KEY([ItemTypeGroupId]) REFERENCES [dbo].[ItemTypeGroup] ([Id]);
GO

--	Description	:	Interest Type Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[InterestType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,	
	[Status] [bit] NULL,
	CONSTRAINT [PK_IntrestType] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_IntrestType] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO


--	Description	:	Interest Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Interest](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemTypeGroupId] [int] NULL,
	[InterestTypeId] [int] NULL,
	[MonthFrom] [int] NULL,
	[MonthTo] [int] NULL,
	[ROI] [decimal](6, 3) NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Interest] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[Interest]  WITH CHECK ADD  CONSTRAINT [FK_Interest_ItemTypeGroup] FOREIGN KEY([ItemTypeGroupId]) REFERENCES [dbo].[ItemTypeGroup] ([Id]);
GO
ALTER TABLE [dbo].[Interest]  WITH CHECK ADD  CONSTRAINT [FK_Interest_InterestType] FOREIGN KEY([InterestTypeId]) REFERENCES [dbo].[InterestType] ([Id]);
GO

--	Description	:	PawnShopManagementSystem Version Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[AppVersion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Display] [nvarchar](50) Null,
	[CreatedDate] [datetimeoffset](7) DEFAULT SYSDATETIMEOFFSET(),
	CONSTRAINT [PK_Version] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_Version] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Database Authentication Type Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[DBAuthenticationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_DBAuthenticationType] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_DBAuthenticationType] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Company Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Company](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Display] [nvarchar](200) NULL,
	[PrintText] [nvarchar] (200) NULL,
	[Address] [nvarchar](200) NULL,
	[AddressDisplay] [nvarchar](200) NULL,
	[AddressPrint] [nvarchar](200) NULL,
	[CityId] [int] NULL,
	[StateId] [int] NULL,
	[CountryId] [int] NULL,
	[PINCode] [nvarchar] (50) NULL,
	[PhoneNumber] [nvarchar] (50) NULL,
	[FaxNumber] [nvarchar] (50) NULL,
	[EmailId] [nvarchar] (50) NULL,
	[Website] [nvarchar] (50) NULL,
	
	[Logo] [image] NULL,
	[LicenceNumber] [nvarchar] (50) NULL,
	[DBHost] [nvarchar] (50) NULL,
	[DBName] [nvarchar] (50) NULL,
	[DBUsername] [nvarchar] (50) NULL,
	[DBPassword] [nvarchar] (50) NULL,
	[DBAuthenticationTypeId] [int] NULL,

	[CurrencyId] [int] NULL,
	[LoanPrefix] [nvarchar] (50) NULL,
	[LoanPostfix] [nvarchar] (50) NULL,
	[RepayPrefix] [nvarchar] (50) NULL,
	[RepayPostfix] [nvarchar] (50) NULL,
	[ReceiptHeader] [image] NULL,
	[ReceiptFooter] [image] NULL,
	[Signature] [image] NULL,

	[CloudBackupLocation] [nvarchar] (50) NULL,
	[CloudUsername] [nvarchar] (50) NULL,
	[CloudPassword] [nvarchar] (50) NULL,
	[LocalBackupLocation] [nvarchar] (50) NULL,
		
	[MatureMonths] [int] NULL,
	[GraceDays] [int] NULL,
	[AlertJob] [bit] NULL,
	[BackupJob] [bit] NULL,

	[LanguageId] [int] NULL,
	[ThemeId] [int] NULL,
	[ColorId] [int] Null,

	[CreatedBy] [nvarchar] (50) NULL,
	[CreatedDate] [datetimeoffset](7) DEFAULT SYSDATETIMEOFFSET(),
	[Status] [bit] NULL,
	CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_Company] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_City] FOREIGN KEY([CityId]) REFERENCES [dbo].[City] ([Id]);
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_State] FOREIGN KEY([StateId]) REFERENCES [dbo].[State] ([Id]);
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_Country] FOREIGN KEY([CountryId]) REFERENCES [dbo].[Country] ([Id]);
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_DBAuthenticationType] FOREIGN KEY([DBAuthenticationTypeId]) REFERENCES [dbo].[DBAuthenticationType] ([Id]);
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_Currency] FOREIGN KEY([CurrencyId]) REFERENCES [dbo].[Currency] ([Id]);
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_Language] FOREIGN KEY([LanguageId]) REFERENCES [dbo].[Language] ([Id]);
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_Theme] FOREIGN KEY([ThemeId]) REFERENCES [dbo].[Theme] ([Id]);
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_Color] FOREIGN KEY([ColorId]) REFERENCES [dbo].[Color] ([Id]);
GO

--	Description	:	Module Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Module](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_Module] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Role Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Display] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_Role] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	CoreUser Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[CoreUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NULL,
	[CompanyId] [int] NULL,
	[Username] [nvarchar] (50) NOT NULL,
	[Display] [nvarchar] (50) NULL,
	[PrintText] [nvarchar] (50) NULL,
	[Password] [nvarchar] (500) NULL,
	[LastPassword] [nvarchar] (50) NULL,
	[PasswordFiledCount] [int] NULL,
	[Firstname] [nvarchar] (50) NULL,
	[LastName] [nvarchar] (50) NULL,
	[GenderId] [int] NULL,
	[DOB] [datetime] NULL,
	[Photo] [image] NULL,
	[Thumb] [image] NULL,
	[LanguageId] [int] NULL,
	[ThemeId] [int] NULL,
	[ColorId] [int] Null,
	[CreatedBy] [nvarchar] (50) NULL,
	[CreatedDate] [datetimeoffset](7) DEFAULT SYSDATETIMEOFFSET(),
	[Status] [bit] NULL,	
	CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_User] UNIQUE NONCLUSTERED ([Username] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[CoreUser]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([RoleId]) REFERENCES [dbo].[Role] ([Id]);
GO
ALTER TABLE [dbo].[CoreUser]  WITH CHECK ADD  CONSTRAINT [FK_User_Company] FOREIGN KEY([CompanyId]) REFERENCES [dbo].[Company] ([Id]);
GO
ALTER TABLE [dbo].[CoreUser]  WITH CHECK ADD  CONSTRAINT [FK_User_Language] FOREIGN KEY([LanguageId]) REFERENCES [dbo].[Language] ([Id]);
GO
ALTER TABLE [dbo].[CoreUser]  WITH CHECK ADD  CONSTRAINT [FK_User_Theme] FOREIGN KEY([ThemeId]) REFERENCES [dbo].[Theme] ([Id]);
GO
ALTER TABLE [dbo].[CoreUser]  WITH CHECK ADD  CONSTRAINT [FK_User_Color] FOREIGN KEY([ColorId]) REFERENCES [dbo].[Color] ([Id]);
GO
ALTER TABLE [dbo].[CoreUser]  WITH CHECK ADD  CONSTRAINT [FK_User_Gender] FOREIGN KEY([GenderId]) REFERENCES [dbo].[Gender] ([Id]);
GO

--	Description	:	Authorisation Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Authorisation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](50) NOT NULL,
	[ModuleId] [nvarchar](50) Null,
	[CreatedDate] [datetimeoffset](7) DEFAULT SYSDATETIMEOFFSET(),
	[Status] [bit] NULL,
	CONSTRAINT [PK_Authorisation] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Activity Log Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[ActivityLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](200) Null,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetimeoffset](7) DEFAULT SYSDATETIMEOFFSET(),
	CONSTRAINT [PK_ActivityLog] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Notification Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Notification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[SMS] [bit] NULL,
	[Email] [bit] NULL,
	[Notice] [bit] NULL,
	CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_Notification] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	LoanState Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[LoanState](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_LoanState] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_LoanState] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	JewelState Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[JewelState](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_JewelState] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_JewelState] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	ProofType Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[ProofType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_ProofType] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_ProofType] UNIQUE NONCLUSTERED ([Name] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Customer Table Creation
--	Since		:	1.0
--	Date		:	2-DEC-2015
--	Author		:	Shree Soft Solutions
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50)  NULL,
	[LastName] [nvarchar](50)  NULL,
	[GenderId] [int] NULL,
	[DOB] [datetime] NULL,
	[MobileNumber] [nvarchar] (50) NULL,
	[EmailId] [nvarchar] (50) NULL,
	[Photo] [image] NULL,
	[Thumb] [image] NULL,
	[Occupation] [nvarchar] (50) NULL,
	[Address] [int] NULL,
	[CityId] [int] NULL,
	[StateId] [int] NULL,
	[CountryId] [int] NULL,
	[PINCode] [nvarchar](50) NULL,
	[ProofTypeId] [int]  NULL,
	[ProofId] [nvarchar] (50) NULL,
	CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_City] FOREIGN KEY([CityId]) REFERENCES [dbo].[City] ([Id]);
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_State] FOREIGN KEY([StateId]) REFERENCES [dbo].[State] ([Id]);
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Country] FOREIGN KEY([CountryId]) REFERENCES [dbo].[Country] ([Id]);
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_ProofType] FOREIGN KEY([ProofTypeId]) REFERENCES [dbo].[ProofType] ([Id]);
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Gender] FOREIGN KEY([GenderId]) REFERENCES [dbo].[Gender] ([Id]);
GO

--	Description	:	Loan Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Loan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BillNo] [nvarchar](50) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[OpenDate] [datetimeoffset](7) NULL,	
	[PrincipalAmount] [decimal](18, 2) NULL,
	[AdvanceAmount] [decimal](18,2) NULL,
	[NetPaidAmount] [decimal](18,2) NULL,

	[CurrentPrincipalAmount] [decimal](18, 2) NULL,
	[CurrentInterestAmount] [decimal](18, 2) NULL,
	[LoanStateId] [int] NULL,
	[LastTransaction] [datetimeoffset](7) NULL,
	[ExpireDate] [datetimeoffset](7) NULL,
	[ClosedDate] [datetimeoffset](7) NULL,

	[Description] [nvarchar] (50) NULL,
	[OpenStaffId] [int] NULL,
	[CloseStaffId] [int] NULL,
	CONSTRAINT [PK_Loan] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
	CONSTRAINT [UK_Loan] UNIQUE NONCLUSTERED ([BillNo] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[Loan]  WITH CHECK ADD  CONSTRAINT [FK_Loan_CustomerId] FOREIGN KEY([CustomerId]) REFERENCES [dbo].[Customer] ([Id]);
GO
ALTER TABLE [dbo].[Loan]  WITH CHECK ADD  CONSTRAINT [FK_Loan_LoanState] FOREIGN KEY([LoanStateId]) REFERENCES [dbo].[LoanState] ([Id]);
GO
ALTER TABLE [dbo].[Loan]  WITH CHECK ADD  CONSTRAINT [FK_Loan_OpenUser] FOREIGN KEY([OpenStaffId]) REFERENCES [dbo].[CoreUser] ([Id]);
GO
ALTER TABLE [dbo].[Loan]  WITH CHECK ADD  CONSTRAINT [FK_Loan_CloseUser] FOREIGN KEY([CloseStaffId]) REFERENCES [dbo].[CoreUser] ([Id]);
GO


--	Description	:	Jewel Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Jewel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LoanId] [int] NULL,
	[ItemTypeId] [int] NULL,
	[ItemId] [int] NULL,
	[Description] [nvarchar] (200) NULL,
	[JewelStateId] [int] NULL,
	[Quantity] [int] NULL,
	[NetWeight] [decimal](18, 2) NULL,
	[GrossWeight] [decimal](18,2) NULL,
	[MarketValueAmount] [decimal](18,2) NULL,
	[LoanAmount] [decimal](18,2) NULL,
	[Purity] [decimal](18,2) NULL,
	[HoldDate] [datetimeoffset](7) NULL,
	[ReleaseDate] [datetimeoffset](7) NULL,
	[HoldStaffId] [int] NULL,
	[ReleaseStaffId] [int] NULL,
	CONSTRAINT [PK_Jewel] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[Jewel]  WITH CHECK ADD  CONSTRAINT [FK_Jewel_Loan] FOREIGN KEY([LoanId]) REFERENCES [dbo].[Loan] ([Id]);
GO
ALTER TABLE [dbo].[Jewel]  WITH CHECK ADD  CONSTRAINT [FK_Jewel_ItemType] FOREIGN KEY([ItemTypeId]) REFERENCES [dbo].[ItemType] ([Id]);
GO
ALTER TABLE [dbo].[Jewel]  WITH CHECK ADD  CONSTRAINT [FK_Jewel_Item] FOREIGN KEY([ItemId]) REFERENCES [dbo].[Item] ([Id]);
GO
ALTER TABLE [dbo].[Jewel]  WITH CHECK ADD  CONSTRAINT [FK_Jewel_JewelState] FOREIGN KEY([JewelStateId]) REFERENCES [dbo].[JewelState] ([Id]);
GO
ALTER TABLE [dbo].[Jewel]  WITH CHECK ADD  CONSTRAINT [FK_Jewel_HoldUser] FOREIGN KEY([HoldStaffId]) REFERENCES [dbo].[CoreUser] ([Id]);
GO
ALTER TABLE [dbo].[Jewel]  WITH CHECK ADD  CONSTRAINT [FK_Jewel_ReleaseUser] FOREIGN KEY([ReleaseStaffId]) REFERENCES [dbo].[CoreUser] ([Id]);
GO

--	Description	:	Repay Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Repay](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LoanId] [int] NULL,
	[LoanAmount] [decimal](18,2) NULL,
	[LoanRepaidAmount] [decimal](18,2) NULL,
	[InterestAmount] [decimal](18,2) NULL,
	[InterestRepaidAmount] [decimal](18,2) NULL,
	[RepaidBy] [nvarchar] NULL,
	[RepayReceivedBy] [nvarchar] NULL,
	[RepayDate] [datetimeoffset](7) NULL,
	CONSTRAINT [PK_Repay] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF,  STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,  ALLOW_ROW_LOCKS  = ON,  ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[Repay]  WITH CHECK ADD  CONSTRAINT [FK_Repay_Loan] FOREIGN KEY([LoanId]) REFERENCES [dbo].[Loan] ([Id]);
GO

--	Description	:	Page Table Creation
--	Since		:	1.0
--	Date		:	26-June-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Page](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Sort] [int] NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO


--	Description	:	FieldGroup Table Creation
--	Since		:	1.0
--	Date		:	25-June-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[FieldGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PageId] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Sort] [int] NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_FieldGroup] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[FieldGroup]  WITH CHECK ADD  CONSTRAINT [FK_FieldGroup_Page] FOREIGN KEY([PageId]) REFERENCES [dbo].[Page] ([Id]);
GO

--	Description	:	AllowedValue Table Creation
--	Since		:	1.0
--	Date		:	25-June-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[AllowedValue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_AllowedValue] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	FieldType Table Creation
--	Since		:	1.0
--	Date		:	25-June-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[FieldType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_FieldType] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO

--	Description	:	Field Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Field](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FieldGroupId] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[AllowedValueId] [int] NULL,
	[FieldTypeId] [int] NULL,
	[Action] [int] NULL,
	[Sort] [int] NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Field] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
ALTER TABLE [dbo].[Field]  WITH CHECK ADD  CONSTRAINT [FK_Field_FieldGroup] FOREIGN KEY([FieldGroupId]) REFERENCES [dbo].[FieldGroup] ([Id]);
GO
ALTER TABLE [dbo].[Field]  WITH CHECK ADD  CONSTRAINT [FK_Field_AllowedValue] FOREIGN KEY([AllowedValueId]) REFERENCES [dbo].[AllowedValue] ([Id]);
GO
ALTER TABLE [dbo].[Field]  WITH CHECK ADD  CONSTRAINT [FK_Field_FieldType] FOREIGN KEY([FieldTypeId]) REFERENCES [dbo].[FieldType] ([Id]);
GO

--	Description	:	Operator Table Creation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
CREATE TABLE [dbo].[Filter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Status] [bit] NULL,
	CONSTRAINT [PK_Filter] PRIMARY KEY CLUSTERED ([Id] ASC)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
GO
INSERT INTO [dbo].[ActivityLog]([Message],[CreatedBy]) VALUES ('PawnShopManagementSystem Database and its Table Created','System');
GO

--	Description	:	Inserting Default Values for Language Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Afrikaans - South Africa', 'af-ZA', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Albanian - Albania', 'sq-AL', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Algeria', 'ar-DZ', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Bahrain', 'ar-BH', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Egypt', 'ar-EG', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Iraq', 'ar-IQ', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Jordan', 'ar-JO', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Kuwait', 'ar-KW', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Lebanon', 'ar-LB', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Libya', 'ar-LY', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Morocco', 'ar-MA', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Oman', 'ar-OM', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Qatar', 'ar-QA', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Saudi Arabia', 'ar-SA', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Syria', 'ar-SY', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Tunisia', 'ar-TN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - United Arab Emirates', 'ar-AE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Arabic - Yemen', 'ar-YE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Armenian - Armenia', 'hy-AM', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Azeri (Cyrillic) - Azerbaijan', 'Cy-az-AZ', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Azeri (Latin) - Azerbaijan', 'Lt-az-AZ', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Basque - Basque', 'eu-ES', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Belarusian - Belarus', 'be-BY', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Bulgarian - Bulgaria', 'bg-BG', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Catalan - Catalan', 'ca-ES', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Chinese - China', 'zh-CN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Chinese - Hong Kong SAR', 'zh-HK', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Chinese - Macau SAR', 'zh-MO', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Chinese - Singapore', 'zh-SG', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Chinese - Taiwan', 'zh-TW', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Chinese (Simplified)', 'zh-CHS', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Chinese (Traditional)', 'zh-CHT', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Croatian - Croatia', 'hr-HR', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Czech - Czech Republic', 'cs-CZ', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Danish - Denmark', 'da-DK', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Dhivehi - Maldives', 'div-MV', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Dutch - Belgium', 'nl-BE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Dutch - The Netherlands', 'nl-NL', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - Australia', 'en-AU', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - Belize', 'en-BZ', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - Canada', 'en-CA', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - Caribbean', 'en-CB', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - Ireland', 'en-IE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - Jamaica', 'en-JM', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - New Zealand', 'en-NZ', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - Philippines', 'en-PH', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - South Africa', 'en-ZA', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - Trinidad and Tobago', 'en-TT', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - United Kingdom', 'en-GB', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - United States', 'en-US', 1);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('English - Zimbabwe', 'en-ZW', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Estonian - Estonia', 'et-EE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Faroese - Faroe Islands', 'fo-FO', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Farsi - Iran', 'fa-IR', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Finnish - Finland', 'fi-FI', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('French - Belgium', 'fr-BE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('French - Canada', 'fr-CA', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('French - France', 'fr-FR', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('French - Luxembourg', 'fr-LU', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('French - Monaco', 'fr-MC', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('French - Switzerland', 'fr-CH', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Galician - Galician', 'gl-ES', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Georgian - Georgia', 'ka-GE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('German - Austria', 'de-AT', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('German - Germany', 'de-DE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('German - Liechtenstein', 'de-LI', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('German - Luxembourg', 'de-LU', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('German - Switzerland', 'de-CH', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Greek - Greece', 'el-GR', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Gujarati - India', 'gu-IN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Hebrew - Israel', 'he-IL', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Hindi - India', 'hi-IN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Hungarian - Hungary', 'hu-HU', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Icelandic - Iceland', 'is-IS', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Indonesian - Indonesia', 'id-ID', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Italian - Italy', 'it-IT', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Italian - Switzerland', 'it-CH', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Japanese - Japan', 'ja-JP', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Kannada - India', 'kn-IN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Kazakh - Kazakhstan', 'kk-KZ', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Konkani - India', 'kok-IN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Korean - Korea', 'ko-KR', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Kyrgyz - Kazakhstan', 'ky-KZ', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Latvian - Latvia', 'lv-LV', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Lithuanian - Lithuania', 'lt-LT', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Macedonian (FYROM)', 'mk-MK', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Malay - Brunei', 'ms-BN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Malay - Malaysia', 'ms-MY', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Marathi - India', 'mr-IN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Mongolian - Mongolia', 'mn-MN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Norwegian (Bokmål) - Norway', 'nb-NO', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Norwegian (Nynorsk) - Norway', 'nn-NO', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Polish - Poland', 'pl-PL', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Portuguese - Brazil', 'pt-BR', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Portuguese - Portugal', 'pt-PT', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Punjabi - India', 'pa-IN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Romanian - Romania', 'ro-RO', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Russian - Russia', 'ru-RU', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Sanskrit - India', 'sa-IN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Serbian (Cyrillic) - Serbia', 'Cy-sr-SP', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Serbian (Latin) - Serbia', 'Lt-sr-SP', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Slovak - Slovakia', 'sk-SK', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Slovenian - Slovenia', 'sl-SI', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Argentina', 'es-AR', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Bolivia', 'es-BO', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Chile', 'es-CL', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Colombia', 'es-CO', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Costa Rica', 'es-CR', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Dominican Republic', 'es-DO', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Ecuador', 'es-EC', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - El Salvador', 'es-SV', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Guatemala', 'es-GT', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Honduras', 'es-HN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Mexico', 'es-MX', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Nicaragua', 'es-NI', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Panama', 'es-PA', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Paraguay', 'es-PY', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Peru', 'es-PE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Puerto Rico', 'es-PR', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Spain', 'es-ES', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Uruguay', 'es-UY', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Spanish - Venezuela', 'es-VE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Swahili - Kenya', 'sw-KE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Swedish - Finland', 'sv-FI', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Swedish - Sweden', 'sv-SE', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Syriac - Syria', 'syr-SY', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Tamil - India', 'ta-IN', 1);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Tatar - Russia', 'tt-RU', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Telugu - India', 'te-IN', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Thai - Thailand', 'th-TH', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Turkish - Turkey', 'tr-TR', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Ukrainian - Ukraine', 'uk-UA', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Urdu - Pakistan', 'ur-PK', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Uzbek (Cyrillic) - Uzbekistan', 'Cy-uz-UZ', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Uzbek (Latin) - Uzbekistan', 'Lt-uz-UZ', 0);
INSERT INTO [dbo].[Language]([Name], [Code], [Status]) VALUES('Vietnamese - Vietnam', 'vi-VN', 0);
GO

--	Description	:	Inserting Default Values for Theme Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Theme]([Name], [Code], [Status]) VALUES('Dark', 'Dark', 1);
INSERT INTO [dbo].[Theme]([Name], [Code], [Status]) VALUES('Light', 'Light', 1);
GO

--	Description	:	Inserting Default Values for Color Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Black', 'Black', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('White', 'White', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Silver', 'Silver', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Blue', 'Blue', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Green', 'Green', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Lime', 'Lime', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Teal', 'Teal', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Orange', 'Orange', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Brown', 'Brown', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Pink', 'Pink', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Magenta', 'Magenta', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Purple', 'Purple', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Red', 'Red', 1);
INSERT INTO [dbo].[Color]([Name], [Code], [Status]) VALUES('Yellow', 'Yellow', 1);
GO

--	Description	:	Inserting Default Values for Currency Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('ADB Unit of Account', 'XUA', 'XUA', 'XUA',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Afghan afghani', 'AFN', 'AFN', 'AFN',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Albanian lek', 'ALL', 'ALL', 'ALL',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Algerian dinar', 'DZD', 'DZD', 'DZD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Angolan kwanza', 'AOA', 'AOA', 'AOA',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Argentine peso', 'ARS', 'ARS', 'ARS',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Armenian dram', 'AMD', 'AMD', 'AMD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Aruban florin', 'AWG', 'AWG', 'AWG',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Australian dollar', 'AUD', 'AUD', 'AUD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Azerbaijani manat', 'AZN', 'AZN', 'AZN',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Bahamian dollar', 'BSD', 'BSD', 'BSD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Bahraini dinar', 'BHD', 'BHD', 'BHD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Bangladeshi taka', 'BDT', 'BDT', 'BDT',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Barbados dollar', 'BBD', 'BBD', 'BBD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Belarusian ruble', 'BYR', 'BYR', 'BYR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Belize dollar', 'BZD', 'BZD', 'BZD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Bermudian dollar', 'BMD', 'BMD', 'BMD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Bhutanese ngultrum', 'BTN', 'BTN', 'BTN',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Bolivian Mvdol', 'BOV', 'BOV', 'BOV',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Boliviano', 'BOB', 'BOB', 'BOB',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Bosnia and Herzegovina convertible mark', 'BAM', 'BAM', 'BAM',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Botswana pula', 'BWP', 'BWP', 'BWP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Brazilian real', 'BRL', 'BRL', 'BRL',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Brunei dollar', 'BND', 'BND', 'BND',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Bulgarian lev', 'BGN', 'BGN', 'BGN',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Burundian franc', 'BIF', 'BIF', 'BIF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Cambodian riel', 'KHR', 'KHR', 'KHR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Canadian dollar', 'CAD', 'CAD', 'CAD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Cape Verde escudo', 'CVE', 'CVE', 'CVE',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Cayman Islands dollar', 'KYD', 'KYD', 'KYD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('CFA franc BCEAO', 'XOF', 'XOF', 'XOF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('CFA franc BEAC', 'XAF', 'XAF', 'XAF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('CFP franc', 'XPF', 'XPF', 'XPF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Chilean peso', 'CLP', 'CLP', 'CLP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Chinese yuan', 'CNY', 'CNY', 'CNY',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Code reserved for testing purposes', 'XTS', 'XTS', 'XTS',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Colombian peso', 'COP', 'COP', 'COP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Comoro franc', 'KMF', 'KMF', 'KMF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Congolese franc', 'CDF', 'CDF', 'CDF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Costa Rican colon', 'CRC', 'CRC', 'CRC',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Croatian kuna', 'HRK', 'HRK', 'HRK',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Cuban convertible peso', 'CUC', 'CUC', 'CUC',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Cuban peso', 'CUP', 'CUP', 'CUP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Czech koruna', 'CZK', 'CZK', 'CZK',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Danish krone', 'DKK', 'DKK', 'DKK',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Djiboutian franc', 'DJF', 'DJF', 'DJF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Dominican peso', 'DOP', 'DOP', 'DOP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('East Caribbean dollar', 'XCD', 'XCD', 'XCD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Egyptian pound', 'EGP', 'EGP', 'EGP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Eritrean nakfa', 'ERN', 'ERN', 'ERN',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Ethiopian birr', 'ETB', 'ETB', 'ETB',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Euro', 'EUR', 'EUR', 'EUR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('European Composite Unit', 'XBA', 'XBA', 'XBA',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('European Monetary Unit', 'XBB', 'XBB', 'XBB',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('European Unit of Account 17', 'XBD', 'XBD', 'XBD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('European Unit of Account 9', 'XBC', 'XBC', 'XBC',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Falkland Islands pound', 'FKP', 'FKP', 'FKP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Fiji dollar', 'FJD', 'FJD', 'FJD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Gambian dalasi', 'GMD', 'GMD', 'GMD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Georgian lari', 'GEL', 'GEL', 'GEL',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Ghanaian cedi', 'GHS', 'GHS', 'GHS',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Gibraltar pound', 'GIP', 'GIP', 'GIP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Gold', 'XAU', 'XAU', 'XAU',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Guatemalan quetzal', 'GTQ', 'GTQ', 'GTQ',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Guinean franc', 'GNF', 'GNF', 'GNF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Guyanese dollar', 'GYD', 'GYD', 'GYD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Haitian gourde', 'HTG', 'HTG', 'HTG',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Honduran lempira', 'HNL', 'HNL', 'HNL',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Hong Kong dollar', 'HKD', 'HKD', 'HKD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Hungarian forint', 'HUF', 'HUF', 'HUF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Icelandic króna', 'ISK', 'ISK', 'ISK',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Indian rupee', 'INR', 'INR', 'INR',1);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Indonesian rupiah', 'IDR', 'IDR', 'IDR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Iranian rial', 'IRR', 'IRR', 'IRR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Iraqi dinar', 'IQD', 'IQD', 'IQD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Israeli new shekel', 'ILS', 'ILS', 'ILS',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Jamaican dollar', 'JMD', 'JMD', 'JMD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Japanese yen', 'JPY', 'JPY', 'JPY',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Jordanian dinar', 'JOD', 'JOD', 'JOD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Kazakhstani tenge', 'KZT', 'KZT', 'KZT',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Kenyan shilling', 'KES', 'KES', 'KES',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Kuwaiti dinar', 'KWD', 'KWD', 'KWD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Kyrgyzstani som', 'KGS', 'KGS', 'KGS',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Lao kip', 'LAK', 'LAK', 'LAK',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Lebanese pound', 'LBP', 'LBP', 'LBP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Lesotho loti', 'LSL', 'LSL', 'LSL',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Liberian dollar', 'LRD', 'LRD', 'LRD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Libyan dinar', 'LYD', 'LYD', 'LYD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Macanese pataca', 'MOP', 'MOP', 'MOP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Macedonian denar', 'MKD', 'MKD', 'MKD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Malagasy ariary', 'MGA', 'MGA', 'MGA',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Malawian kwacha', 'MWK', 'MWK', 'MWK',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Malaysian ringgit', 'MYR', 'MYR', 'MYR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Maldivian rufiyaa', 'MVR', 'MVR', 'MVR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Mauritanian ouguiya', 'MRO', 'MRO', 'MRO',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Mauritian rupee', 'MUR', 'MUR', 'MUR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Mexican peso', 'MXN', 'MXN', 'MXN',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Mexican Unidad de Inversion', 'MXV', 'MXV', 'MXV',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Moldovan leu', 'MDL', 'MDL', 'MDL',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Mongolian tugrik', 'MNT', 'MNT', 'MNT',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Moroccan dirham', 'MAD', 'MAD', 'MAD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Mozambican metical', 'MZN', 'MZN', 'MZN',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Myanmar kyat', 'MMK', 'MMK', 'MMK',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Namibian dollar', 'NAD', 'NAD', 'NAD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Nepalese rupee', 'NPR', 'NPR', 'NPR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Netherlands Antillean guilder', 'ANG', 'ANG', 'ANG',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('New Taiwan dollar', 'TWD', 'TWD', 'TWD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('New Zealand dollar', 'NZD', 'NZD', 'NZD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Nicaraguan córdoba', 'NIO', 'NIO', 'NIO',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Nigerian naira', 'NGN', 'NGN', 'NGN',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('No currency ', 'XXX', 'XXX', 'XXX',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('North Korean won', 'KPW', 'KPW', 'KPW',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Norwegian krone', 'NOK', 'NOK', 'NOK',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Omani rial', 'OMR', 'OMR', 'OMR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Pakistani rupee', 'PKR', 'PKR', 'PKR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Palladium', 'XPD', 'XPD', 'XPD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Panamanian balboa', 'PAB', 'PAB', 'PAB',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Papua New Guinean kina', 'PGK', 'PGK', 'PGK',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Paraguayan guaraní', 'PYG', 'PYG', 'PYG',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Peruvian nuevo sol', 'PEN', 'PEN', 'PEN',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Philippine peso', 'PHP', 'PHP', 'PHP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Platinum', 'XPT', 'XPT', 'XPT',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Polish złoty', 'PLN', 'PLN', 'PLN',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Pound sterling', 'GBP', 'GBP', 'GBP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Qatari riyal', 'QAR', 'QAR', 'QAR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Romanian new leu', 'RON', 'RON', 'RON',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Russian ruble', 'RUB', 'RUB', 'RUB',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Rwandan franc', 'RWF', 'RWF', 'RWF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Saint Helena pound', 'SHP', 'SHP', 'SHP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Samoan tala', 'WST', 'WST', 'WST',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Saudi riyal', 'SAR', 'SAR', 'SAR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Serbian dinar', 'RSD', 'RSD', 'RSD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Seychelles rupee', 'SCR', 'SCR', 'SCR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Sierra Leonean leone', 'SLL', 'SLL', 'SLL',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Silver', 'XAG', 'XAG', 'XAG',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Singapore dollar', 'SGD', 'SGD', 'SGD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Solomon Islands dollar', 'SBD', 'SBD', 'SBD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Somali shilling', 'SOS', 'SOS', 'SOS',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('South African rand', 'ZAR', 'ZAR', 'ZAR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('South Korean won', 'KRW', 'KRW', 'KRW',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('South Sudanese pound', 'SSP', 'SSP', 'SSP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Special drawing rights', 'XDR', 'XDR', 'XDR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Sri Lankan rupee', 'LKR', 'LKR', 'LKR',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('SUCRE', 'XSU', 'XSU', 'XSU',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Sudanese pound', 'SDG', 'SDG', 'SDG',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Surinamese dollar', 'SRD', 'SRD', 'SRD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Swazi lilangeni', 'SZL', 'SZL', 'SZL',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Swedish krona kronor', 'SEK', 'SEK', 'SEK',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Swiss franc', 'CHF', 'CHF', 'CHF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Syrian pound', 'SYP', 'SYP', 'SYP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('São Tomé and Príncipe dobra', 'STD', 'STD', 'STD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Tajikistani somoni', 'TJS', 'TJS', 'TJS',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Tanzanian shilling', 'TZS', 'TZS', 'TZS',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Thai baht', 'THB', 'THB', 'THB',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Tongan paʻanga', 'TOP', 'TOP', 'TOP',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Trinidad and Tobago dollar', 'TTD', 'TTD', 'TTD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Tunisian dinar', 'TND', 'TND', 'TND',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Turkish lira', 'TRY', 'TRY', 'TRY',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Turkmenistani manat', 'TMT', 'TMT', 'TMT',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Ugandan shilling', 'UGX', 'UGX', 'UGX',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('UIC franc', 'XFU', 'XFU', 'XFU',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Ukrainian hryvnia', 'UAH', 'UAH', 'UAH',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Unidad de Fomento', 'CLF', 'CLF', 'CLF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Unidad de Valor Real', 'COU', 'COU', 'COU',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('United Arab Emirates dirham', 'AED', 'AED', 'AED',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('United States dollar', 'USD', 'USD', 'USD',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('United States dollar next day', 'USN', 'USN', 'USN',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('United States dollar same day', 'USS', 'USS', 'USS',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Uruguay Peso en Unidades Indexadas', 'UYI', 'UYI', 'UYI',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Uruguayan peso', 'UYU', 'UYU', 'UYU',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Uzbekistan som', 'UZS', 'UZS', 'UZS',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Vanuatu vatu', 'VUV', 'VUV', 'VUV',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Venezuelan bolívar', 'VEF', 'VEF', 'VEF',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Vietnamese dong', 'VND', 'VND', 'VND',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('WIR Euro complementary currency', 'CHE', 'CHE', 'CHE',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('WIR Franc complementary currency', 'CHW', 'CHW', 'CHW',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Yemeni rial', 'YER', 'YER', 'YER',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Zambian kwacha', 'ZMW', 'ZMW', 'ZMW',0);
INSERT INTO [dbo].[Currency]([Name], [Code], [Display], [PrintText], [Status]) VALUES('Zimbabwean dollar', 'ZWL', 'ZWL', 'ZWL',0);
GO

--	Description	:	Inserting Default Values for Country Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Afghanistan', 'AF', 'Afghanistan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Albania', 'AL', 'Albania', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Algeria', 'DZ', 'Algeria', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Andorra', 'AD', 'Andorra', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Angola', 'AO', 'Angola', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Antigua and Barbuda', 'AG', 'Antigua and Barbuda', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Argentina', 'AR', 'Argentina', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Armenia', 'AM', 'Armenia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Australia', 'AU', 'Australia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Austria', 'AT', 'Austria', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Azerbaijan', 'AZ', 'Azerbaijan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Bahamas', 'BS', 'Bahamas', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Bahrain', 'BH', 'Bahrain', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Bangladesh', 'BD', 'Bangladesh', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Barbados', 'BB', 'Barbados', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Belarus', 'BY', 'Belarus', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Belgium', 'BE', 'Belgium', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Belize', 'BZ', 'Belize', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Benin', 'BJ', 'Benin', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Bhutan', 'BT', 'Bhutan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Bolivia', 'BO', 'Bolivia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Bosnia And Herzegovina', 'BA', 'Bosnia And Herzegovina', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Botswana', 'BW', 'Botswana', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Brazil', 'BR', 'Brazil', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Brunei', 'BN', 'Brunei', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Bulgaria', 'BG', 'Bulgaria', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Burkina Faso', 'BF', 'Burkina Faso', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Burundi', 'BI', 'Burundi', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Cambodia', 'KH', 'Cambodia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Cameroon', 'CM', 'Cameroon', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Canada', 'CA', 'Canada', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Cape Verde', 'CV', 'Cape Verde', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Central African Republic', 'CF', 'Central African Republic', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Chad', 'TD', 'Chad', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Chile', 'CL', 'Chile', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('China', 'CN', 'China', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Colombia', 'CO', 'Colombia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Comoros', 'KM', 'Comoros', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Congo', 'CG', 'Congo', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Congo (DRC)', 'ZR', 'Congo (DRC)', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Costa Rica', 'CR', 'Costa Rica', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Cote dIvoire', 'CI', 'Cote dIvoire', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Croatia', 'HR', 'Croatia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Cuba', 'CU', 'Cuba', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Cyprus', 'CY', 'Cyprus', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Czech Republic', 'EZ', 'Czech Republic', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Denmark', 'DK', 'Denmark', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Djibouti', 'DJ', 'Djibouti', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Dominica', 'DM', 'Dominica', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Dominican Republic', 'DO', 'Dominican Republic', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Ecuador', 'EC', 'Ecuador', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Egypt', 'EG', 'Egypt', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('El Salvador', 'SV', 'El Salvador', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Equatorial Guinea', 'GQ', 'Equatorial Guinea', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Eritrea', 'ER', 'Eritrea', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Estonia', 'EE', 'Estonia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Ethiopia', 'ET', 'Ethiopia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Fiji Islands', 'FJ', 'Fiji Islands', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Finland', 'FI', 'Finland', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Former Yugoslav Republic of Macedonia', 'MK', 'Former Yugoslav Republic of Macedonia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('France', 'FR', 'France', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Gabon', 'GA', 'Gabon', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Gambia, The', 'GM', 'Gambia, The', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Georgia', 'GE', 'Georgia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Germany', 'DE', 'Germany', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Ghana', 'GH', 'Ghana', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Greece', 'GR', 'Greece', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Grenada', 'GD', 'Grenada', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Guatemala', 'GT', 'Guatemala', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Guinea', 'GN', 'Guinea', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Guinea-Bissau', 'GW', 'Guinea-Bissau', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Guyana', 'GY', 'Guyana', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Haiti', 'HT', 'Haiti', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Honduras', 'HN', 'Honduras', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Hungary', 'HU', 'Hungary', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Iceland', 'IS', 'Iceland', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('India', 'IN', 'India', 1);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Indonesia', 'ID', 'Indonesia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Iran', 'IR', 'Iran', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Iraq', 'IQ', 'Iraq', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Ireland', 'IE', 'Ireland', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Israel', 'IL', 'Israel', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Italy', 'IT', 'Italy', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Jamaica', 'JM', 'Jamaica', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Japan', 'JP', 'Japan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Jordan', 'JO', 'Jordan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Kazakhstan', 'KZ', 'Kazakhstan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Kenya', 'KE', 'Kenya', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Kiribati', 'KI', 'Kiribati', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Korea, Democratic Peoples Republic of', 'KP', 'Korea, Democratic Peoples Republic of', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Korea', 'KR', 'Korea', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Kuwait', 'KW', 'Kuwait', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Kyrgyzstan', 'KG', 'Kyrgyzstan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Laos', 'LA', 'Laos', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Latvia', 'LV', 'Latvia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Lebanon', 'LB', 'Lebanon', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Lesotho', 'LS', 'Lesotho', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Liberia', 'LR', 'Liberia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Libya', 'LY', 'Libya', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Liechtenstein', 'LI', 'Liechtenstein', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Lithuania', 'LT', 'Lithuania', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Luxembourg', 'LU', 'Luxembourg', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Madagascar', 'MG', 'Madagascar', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Malawi', 'MW', 'Malawi', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Malaysia', 'MY', 'Malaysia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Maldives', 'MV', 'Maldives', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Mali', 'ML', 'Mali', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Malta', 'MT', 'Malta', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Marshall Islands', 'MH', 'Marshall Islands', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Mauritania', 'MR', 'Mauritania', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Mauritius', 'MU', 'Mauritius', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Mexico', 'MX', 'Mexico', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Micronesia, Federated States of', 'FM', 'Micronesia, Federated States of', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Moldova', 'MD', 'Moldova', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Monaco', 'MC', 'Monaco', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Mongolia', 'MN', 'Mongolia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Morocco', 'MA', 'Morocco', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Mozambique', 'MZ', 'Mozambique', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Myanmar', 'MM', 'Myanmar', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Namibia', 'NA', 'Namibia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Nauru', 'NR', 'Nauru', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Nepal', 'NP', 'Nepal', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Netherlands, The', 'NL', 'Netherlands, The', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('New Zealand', 'NZ', 'New Zealand', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Nicaragua', 'NI', 'Nicaragua', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Niger', 'NE', 'Niger', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Nigeria', 'NG', 'Nigeria', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('North Korea', 'KP', 'North Korea', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Norway', 'NO', 'Norway', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Oman', 'OM', 'Oman', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Pakistan', 'PK', 'Pakistan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Palau', 'PW', 'Palau', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Panama', 'PA', 'Panama', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Papua New Guinea', 'PG', 'Papua New Guinea', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Paraguay', 'PY', 'Paraguay', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Peru', 'PE', 'Peru', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Phillippines', 'PH', 'Phillippines', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Poland', 'PL', 'Poland', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Portugal', 'PT', 'Portugal', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Qatar', 'QA', 'Qatar', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Romania', 'RO', 'Romania', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Russia', 'RU', 'Russia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Rwanda', 'RW', 'Rwanda', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('St. Kitts And Nevis', 'KN', 'St. Kitts And Nevis', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('St. Lucia', 'LC', 'St. Lucia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('St. Vincent And the Grenadines', 'VC', 'St. Vincent And the Grenadines', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('San Marino', 'SM', 'San Marino', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Samoa', 'WS', 'Samoa', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Sao Tome And Principe', 'ST', 'Sao Tome And Principe', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Saudi Arabia', 'SA', 'Saudi Arabia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Senegal', 'SN', 'Senegal', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Seychelles', 'SC', 'Seychelles', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Sierra Leone', 'SL', 'Sierra Leone', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Singapore', 'SG', 'Singapore', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Slovakia', 'SK', 'Slovakia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Slovenia', 'SI', 'Slovenia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Solomon Islands', 'SB', 'Solomon Islands', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Somalia', 'SO', 'Somalia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('South Africa', 'ZA', 'South Africa', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Spain', 'ES', 'Spain', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Sri Lanka', 'LK', 'Sri Lanka', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Sudan', 'SD', 'Sudan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Suriname', 'SR', 'Suriname', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Swaziland', 'SZ', 'Swaziland', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Sweden', 'SE', 'Sweden', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Switzerland', 'CH', 'Switzerland', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Syria', 'SY', 'Syria', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Tajikistan', 'TJ', 'Tajikistan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Tanzania', 'TZ', 'Tanzania', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Thailand', 'TH', 'Thailand', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Togo', 'TG', 'Togo', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Tonga', 'TO', 'Tonga', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Trinidad and Tobago', 'TT', 'Trinidad and Tobago', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Tunisia', 'TN', 'Tunisia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Turkey', 'TR', 'Turkey', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Turkmenistan', 'TM', 'Turkmenistan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Tuvalu', 'TV', 'Tuvalu', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Uganda', 'UG', 'Uganda', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Ukraine', 'UA', 'Ukraine', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('United Arab Emirates', 'AE', 'United Arab Emirates', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('United Kingdom', 'GB', 'United Kingdom', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('United States', 'US', 'United States', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Uruguay', 'UY', 'Uruguay', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Uzbekistan', 'UZ', 'Uzbekistan', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Vanuatu', 'VU', 'Vanuatu', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Vatican City', 'VA', 'Vatican City', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Venezuela', 'VE', 'Venezuela', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Vietnam', 'VN', 'Vietnam', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Yemen', 'YE', 'Yemen', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Federal Republic of Yugoslavia', 'YU', 'Federal Republic of Yugoslavia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Zambia', 'ZM', 'Zambia', 0);
INSERT INTO [dbo].[Country]([Name], [Code], [PrintText], [Status]) VALUES('Zimbabwe', 'ZW', 'Zimbabwe', 0);
GO

--	Description	:	Inserting Default Values for State Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Andhra Pradesh', 'IN-AP', 'Andhra Pradesh', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Arunachal Pradesh', 'IN-AR', 'Arunachal Pradesh', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Assam', 'IN-AS', 'Assam', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Bihar', 'IN-BR', 'Bihar', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Chhattisgarh', 'IN-CT', 'Chhattisgarh', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Goa', 'IN-GA', 'Goa', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Gujarat', 'IN-GJ', 'Gujarat', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Haryana', 'IN-HR', 'Haryana', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Himachal Pradesh', 'IN-HP', 'Himachal Pradesh', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Jammu and Kashmir', 'IN-JK', 'Jammu and Kashmir', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Jharkhand', 'IN-JH', 'Jharkhand', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Karnataka', 'IN-KA', 'Karnataka', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Kerala', 'IN-KL', 'Kerala', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Madhya Pradesh', 'IN-MP', 'Madhya Pradesh', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Maharashtra', 'IN-MH', 'Maharashtra', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Manipur', 'IN-MN', 'Manipur', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Meghalaya', 'IN-ML', 'Meghalaya', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Mizoram', 'IN-MZ', 'Mizoram', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Nagaland', 'IN-NL', 'Nagaland', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Odisha', 'IN-OR', 'Odisha', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Punjab', 'IN-PB', 'Punjab', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Rajasthan', 'IN-RJ', 'Rajasthan', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Sikkim', 'IN-SK', 'Sikkim', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Tamil Nadu', 'IN-TN', 'Tamil Nadu', 1 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Telangana', 'IN-TG', 'Telangana', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Tripura', 'IN-TR', 'Tripura', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Uttarakhand', 'IN-UT', 'Uttarakhand', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Uttar Pradesh', 'IN-UP', 'Uttar Pradesh', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'West Bengal', 'IN-WB', 'West Bengal', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Andaman and Nicobar Islands', 'IN-AN', 'Andaman and Nicobar Islands', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Chandigarh', 'IN-CH', 'Chandigarh', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Dadra and Nagar Haveli', 'IN-DN', 'Dadra and Nagar Haveli', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Daman and Diu', 'IN-DD', 'Daman and Diu', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Delhi', 'IN-DL', 'Delhi', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Lakshadweep', 'IN-LD', 'Lakshadweep', 0 From [dbo].[Country] where [Country].[Name] = 'India';
INSERT INTO [dbo].[State] ([CountryId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Puducherry', 'IN-PY', 'Puducherry', 0 From [dbo].[Country] where [Country].[Name] = 'India';

--	Description	:	Inserting Default Values for City Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Ariyalur', 'AY', 'Ariyalur',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Chennai', 'CH', 'Chennai',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Coimbatore', 'CO', 'Coimbatore',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Cuddalore', 'CU', 'Cuddalore',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Dharmapuri', 'DH', 'Dharmapuri',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Dindigul', 'DI', 'Dindigul',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Erode', 'ER', 'Erode',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Kanchipuram', 'KC', 'Kanchipuram',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Kanyakumari', 'KK', 'Kanyakumari',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Karur', 'KR', 'Karur',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Madurai', 'MA', 'Madurai',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Nagapattinam', 'NG', 'Nagapattinam',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'The Nilgiris', 'NI', 'The Nilgiris',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Namakkal', 'NM', 'Namakkal',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Perambalur', 'PE', 'Perambalur',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Pudukkottai', 'PU', 'Pudukkottai',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Ramanathapuram', 'RA', 'Ramanathapuram',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Salem', 'SA', 'Salem',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Sivagangai', 'SI', 'Sivagangai',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Tiruppur', 'TP', 'Tiruppur',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Tiruchirappalli', 'TC', 'Tiruchirappalli',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Theni', 'TH', 'Theni',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Tirunelveli', 'TI', 'Tirunelveli',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Thanjavur', 'TJ', 'Thanjavur',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Thoothukudi', 'TK', 'Thoothukudi',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Thiruvallur', 'TL', 'Thiruvallur',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Thiruvarur', 'TR', 'Thiruvarur',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Tiruvannamalai', 'TV', 'Tiruvannamalai',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Vellore', 'VE', 'Vellore',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
INSERT INTO [dbo].[City]([StateId], [Name], [Code], [PrintText], [Status]) SELECT [Id], 'Villupuram', 'VL', 'Villupuram',1 FROM [dbo].[State] WHERE [State].[Name] = 'Tamil Nadu' and [State].[CountryId] = (SELECT [Country].[Id] FROM [dbo].[Country] WHERE [Country].[Name] = 'India');
GO

--	Description	:	Inserting Default Values for Gender Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Gender]([Name],[Status])VALUES ('Male',1);
INSERT INTO [dbo].[Gender]([Name],[Status])VALUES ('Female',1);
GO


--	Description	:	Inserting Default Values for Item Type Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[ItemType]([Name],[Display],[PrintText],[Status])VALUES ('Gold','Gold','Gold',1);
INSERT INTO [dbo].[ItemType]([Name],[Display],[PrintText],[Status])VALUES ('Silver','Silver','Silver',1);
GO

--	Description	:	Inserting Default Values for Item Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Item]([ItemTypeId], [Name], [Code], [Display], [PrintText], [Status]) SELECT [ItemType].[Id], 'Stone Ring', 'GSR', 'Stone Ring', 'Stone Ring',1 FROM [dbo].[ItemType] WHERE [ItemType].[Name]='Gold';
INSERT INTO [dbo].[Item]([ItemTypeId], [Name], [Code], [Display], [PrintText], [Status]) SELECT [ItemType].[Id], 'Ladies Ring', 'GLR', 'Ladies Ring', 'Ladies Ring',1 FROM [dbo].[ItemType] WHERE [ItemType].[Name]='Gold';
INSERT INTO [dbo].[Item]([ItemTypeId], [Name], [Code], [Display], [PrintText], [Status]) SELECT [ItemType].[Id], 'Gents Ring', 'GGR', 'Gents Ring', 'Gents Ring',1 FROM [dbo].[ItemType] WHERE [ItemType].[Name]='Gold';
INSERT INTO [dbo].[Item]([ItemTypeId], [Name], [Code], [Display], [PrintText], [Status]) SELECT [ItemType].[Id], 'TV Ring', 'GTVR', 'TV Ring', 'TV Ring',1 FROM [dbo].[ItemType] WHERE [ItemType].[Name]='Gold';
INSERT INTO [dbo].[Item]([ItemTypeId], [Name], [Code], [Display], [PrintText], [Status]) SELECT [ItemType].[Id], 'Plate Ring', 'GPR', 'Plate Ring', 'Plate Ring',1 FROM [dbo].[ItemType] WHERE [ItemType].[Name]='Gold';
INSERT INTO [dbo].[Item]([ItemTypeId], [Name], [Code], [Display], [PrintText], [Status]) SELECT [ItemType].[Id], 'Enamel Ring', 'GER', 'Enamel Ring', 'Enamel Ring',1 FROM [dbo].[ItemType] WHERE [ItemType].[Name]='Gold';
INSERT INTO [dbo].[Item]([ItemTypeId], [Name], [Code], [Display], [PrintText], [Status]) SELECT [ItemType].[Id], 'Stone Ring', 'SSR', 'Stone Ring', 'Stone Ring',1 FROM [dbo].[ItemType] WHERE [ItemType].[Name]='Silver';
GO

--	Description	:	Inserting Default Values for Intrest Type Group Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[ItemTypeGroup]([Name], [Display], [Status]) VALUES('Gold','Gold',1);
INSERT INTO [dbo].[ItemTypeGroup]([Name], [Display], [Status]) VALUES('Silver','Silver',1);
INSERT INTO [dbo].[ItemTypeGroup]([Name], [Display], [Status]) VALUES('Gold_Silver','Gold & Silver',1);
GO

--	Description	:	Inserting Default Values for Intrest Type Group Mapping Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[ItemTypeGroupMapping]([ItemTypeGroupId], [ItemTypeId], [Status]) SELECT [ItemTypeGroup].[Id], [ItemType].[Id], 1 FROM [dbo].[ItemTypeGroup],[dbo].[ItemType] WHERE [ItemTypeGroup].[Name] = 'Gold' and [ItemType].[Name]='Gold';
INSERT INTO [dbo].[ItemTypeGroupMapping]([ItemTypeGroupId], [ItemTypeId], [Status]) SELECT [ItemTypeGroup].[Id], [ItemType].[Id], 1 FROM [dbo].[ItemTypeGroup],[dbo].[ItemType] WHERE [ItemTypeGroup].[Name] = 'Silver' and [ItemType].[Name]='Silver';
INSERT INTO [dbo].[ItemTypeGroupMapping]([ItemTypeGroupId], [ItemTypeId], [Status]) SELECT [ItemTypeGroup].[Id], [ItemType].[Id], 1 FROM [dbo].[ItemTypeGroup],[dbo].[ItemType] WHERE [ItemTypeGroup].[Name] = 'Gold_Silver' and [ItemType].[Name] in ('Gold','Silver');
GO

--	Description	:	Inserting Default Values for Intrest Type Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[InterestType]([Name], [Status]) VALUES('Simple Interest', 1);
GO

--	Description	:	Inserting Default Values for Interest Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Interest]([ItemTypeGroupId],[InterestTypeId],[MonthFrom],[MonthTo],[ROI],[Status]) SELECT [ItemTypeGroup].[Id], [InterestType].[Id], 1,12,12,1 FROM [dbo].[ItemTypeGroup],[dbo].[InterestType] WHERE [ItemTypeGroup].[Name] = 'Gold' AND [InterestType].[Name] ='Simple Interest';
INSERT INTO [dbo].[Interest]([ItemTypeGroupId],[InterestTypeId],[MonthFrom],[MonthTo],[ROI],[Status]) SELECT [ItemTypeGroup].[Id], [InterestType].[Id], 13,24,15,1 FROM [dbo].[ItemTypeGroup],[dbo].[InterestType] WHERE [ItemTypeGroup].[Name] = 'Gold' AND [InterestType].[Name] ='Simple Interest';
INSERT INTO [dbo].[Interest]([ItemTypeGroupId],[InterestTypeId],[MonthFrom],[MonthTo],[ROI],[Status]) SELECT [ItemTypeGroup].[Id], [InterestType].[Id], 1,12,13,1 FROM [dbo].[ItemTypeGroup],[dbo].[InterestType] where [ItemTypeGroup].[Name] = 'Silver' and [InterestType].[Name] ='Simple Interest';
INSERT INTO [dbo].[Interest]([ItemTypeGroupId],[InterestTypeId],[MonthFrom],[MonthTo],[ROI],[Status]) SELECT [ItemTypeGroup].[Id], [InterestType].[Id], 13,24,14,1 FROM [dbo].[ItemTypeGroup],[dbo].[InterestType] where [ItemTypeGroup].[Name] = 'Silver' and [InterestType].[Name] ='Simple Interest';
INSERT INTO [dbo].[Interest]([ItemTypeGroupId],[InterestTypeId],[MonthFrom],[MonthTo],[ROI],[Status]) SELECT [ItemTypeGroup].[Id], [InterestType].[Id], 1,12,12,1 FROM [dbo].[ItemTypeGroup],[dbo].[InterestType] where [ItemTypeGroup].[Name] = 'Gold_Silver' and [InterestType].[Name] ='Simple Interest';
INSERT INTO [dbo].[Interest]([ItemTypeGroupId],[InterestTypeId],[MonthFrom],[MonthTo],[ROI],[Status]) SELECT [ItemTypeGroup].[Id], [InterestType].[Id], 13,24,15,1 FROM [dbo].[ItemTypeGroup],[dbo].[InterestType] where [ItemTypeGroup].[Name] = 'Gold_Silver' and [InterestType].[Name] ='Simple Interest';
GO

--	Description	:	Inserting Default Values for Database Authentication Type Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[DBAuthenticationType]([Name],[Status]) VALUES('SQL Server Authentication',1);
INSERT INTO [dbo].[DBAuthenticationType]([Name],[Status]) VALUES('Windows Authentication',0);
GO

--	Description	:	Inserting Default Values for Company Information Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Company]([Name], [Display], [PrintText], [Address], [AddressDisplay], [AddressPrint], [CityId], [StateId], [CountryId], [PINCode], [PhoneNumber], [FaxNumber], [EmailId], [Website], [Logo], [LicenceNumber], [DBHost], [DBName], [DBUsername], [DBPassword],[DBAuthenticationTypeId], [CurrencyId], [LoanPrefix], [LoanPostfix], [RepayPrefix],[RepayPostfix], [ReceiptHeader], [ReceiptFooter], [Signature], [CloudBackupLocation], [CloudUsername], [CloudPassword], [LocalBackupLocation], [MatureMonths], [GraceDays], [AlertJob], [BackupJob], [LanguageId], [ThemeId], [ColorId], [CreatedBy], [Status]) SELECT 'Thiru Annamalayar Pawn Bisness', 'Thiru Annamalayar Pawn Bisness', 'Thiru Annamalayar Pawn Bisness', 'No 4 Anaikat Road, Shree Sundharavinayagar Vaniga Valagam, Walajapettai', 'No 4 Anaikat Road, Shree Sundharavinayagar Vaniga Valagam, Walajapettai-632 513', 'No 4 Anaikat Road, Shree Sundharavinayagar Vaniga Valagam, Walajapettai-632 513', [City].[Id], [State].[Id],	[Country].[Id], '632513', NULL, NULL, NULL, NULL, NULL, NULL, 'localhost', 'PSMS', 'sa', '28501828',[DBAuthenticationType].[Id], [Currency].[Id],'P.T', '/2015', 'P.R', '/2015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 7, 1, 1, [Language].[Id], [Theme].[Id], [Color].[Id], 'System', 1 FROM [dbo].[City], [dbo].[State], [dbo].[Country], [dbo].[DBAuthenticationType], [dbo].[Currency], [dbo].[Language], [dbo].[Theme], [dbo].[Color] WHERE [City].[Name] = 'Vellore' AND [State].[Name] = 'Tamil Nadu' AND [Country].[Name] = 'India' AND [DBAuthenticationType].[Name] = 'SQL Server Authentication' AND [Currency].[Name] = 'Indian rupee' AND [Language].[Name] = 'Tamil - India' AND [Theme].[Name] = 'Light' AND [Color].[Name] = 'Blue';
GO

--	Description	:	Inserting Default Values for Module Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Module]([Name],[Status]) VALUES('Core',1);
INSERT INTO [dbo].[Module]([Name],[Status]) VALUES('Purchase',0);
INSERT INTO [dbo].[Module]([Name],[Status]) VALUES('Security',0);
INSERT INTO [dbo].[Module]([Name],[Status]) VALUES('Settings',0);
INSERT INTO [dbo].[Module]([Name],[Status]) VALUES('Backup and Restore',0);
INSERT INTO [dbo].[Module]([Name],[Status]) VALUES('Job',0);
INSERT INTO [dbo].[Module]([Name],[Status]) VALUES('Reports',0);
INSERT INTO [dbo].[Module]([Name],[Status]) VALUES('Search',0);
GO

--	Description	:	Inserting Default Values for Database Authentication Type Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Role]([Name], [Display], [Status]) VALUES('Admin', 'Admin', 1);
INSERT INTO [dbo].[Role]([Name], [Display], [Status]) VALUES('User', 'User', 0);
GO

--	Description	:	Inserting Admin User Values for CoreUser Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[CoreUser]([RoleId], [CompanyId], [Username], [Display], [PrintText], [Password], [LastPassword], [PasswordFiledCount], [Firstname], [LastName], [GenderId], [DOB], [Photo], [Thumb], [LanguageId], [ThemeId], [ColorId], [CreatedBy], [Status]) SELECT [Role].[Id], [Company].[Id], 'System', 'System', 'System', 'NGcQ4tW504mFXhZwsnvNgrfLDi1XlgU+TFSIgtIp3G5Hu54e29h+VSsP1J1fEGLiu2D314lzoKauouHCq1H+KQ==', NULL, 0, 'System', 'Administrator', [Gender].[Id], NULL, NULL, NULL, [Language].[Id], [Theme].[Id], [Color].[Id], 'System', 1 FROM [dbo].[Role], [dbo].[Company], [dbo].[Language], [dbo].[Theme], [dbo].[Color],[dbo].[Gender] WHERE [Role].[Name] = 'Admin' AND [Company].[Name] = 'Thiru Annamalayar Pawn Bisness' AND [Language].[Name] = 'Tamil - India' AND [Theme].[Name] = 'Light' AND [Color].[Name] = 'Blue' AND [Gender].[Name] = 'Male';
GO

--	Description	:	Inserting Default values for Authorisation Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Authorisation] ([RoleId],[ModuleId],[Status]) SELECT [Role].[Id], [Module].[Id], 1 FROM [dbo].[Role], [dbo].[Module] WHERE [Role].[Name] = 'Admin';
GO

--	Description	:	Inserting Default values for Notification Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Notification]([Name],[SMS],[Email],[Notice]) VALUES ('NewLoan',0,0,0);
INSERT INTO [dbo].[Notification]([Name],[SMS],[Email],[Notice]) VALUES ('Repay',0,0,0);
INSERT INTO [dbo].[Notification]([Name],[SMS],[Email],[Notice]) VALUES ('Redemption',0,0,0);
INSERT INTO [dbo].[Notification]([Name],[SMS],[Email],[Notice]) VALUES ('Alert',0,0,0);
GO

--	Description	:	Inserting Default values for Loan State Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[LoanState]([Name], [Status]) VALUES('Opened',1);
INSERT INTO [dbo].[LoanState]([Name], [Status]) VALUES('ReadyToRedeem',1);
INSERT INTO [dbo].[LoanState]([Name], [Status]) VALUES('Closed',1);
INSERT INTO [dbo].[LoanState]([Name], [Status]) VALUES('Grace',1);
INSERT INTO [dbo].[LoanState]([Name], [Status]) VALUES('Expired',1);
GO

--	Description	:	Inserting Default values for Jewel State Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[JewelState]([Name], [Status]) VALUES('OnHold',1)
INSERT INTO [dbo].[JewelState]([Name], [Status]) VALUES('Released',1)
GO

--	Description	:	Inserting Default values for Proof Type Table
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[ProofType]([Name],[Status]) VALUES ('Driving Licence',1);
INSERT INTO [dbo].[ProofType]([Name],[Status]) VALUES ('Voter ID',1);
INSERT INTO [dbo].[ProofType]([Name],[Status]) VALUES ('Pan Card',1);
INSERT INTO [dbo].[ProofType]([Name],[Status]) VALUES ('Passport',1);
INSERT INTO [dbo].[ProofType]([Name],[Status]) VALUES ('Aadhar Card',1);
INSERT INTO [dbo].[ProofType]([Name],[Status]) VALUES ('Ration Card',1);
INSERT INTO [dbo].[ProofType]([Name],[Status]) VALUES ('EB Bill',1);
INSERT INTO [dbo].[ProofType]([Name],[Status]) VALUES ('Rental Agreement',1);
INSERT INTO [dbo].[ProofType]([Name],[Status]) VALUES ('Telephone Bill',1);
INSERT INTO [dbo].[ProofType]([Name],[Status]) VALUES ('Bank Pass Book',1);
GO

--	Description	:	Inserting Default values for FieldGroup Table
--	Since		:	1.0
--	Date		:	25-June-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Page] ([Name], [Status]) VALUES('Mortgage',1);
INSERT INTO [dbo].[Page] ([Name], [Status]) VALUES('Repay',1);
INSERT INTO [dbo].[Page] ([Name], [Status]) VALUES('Redemption',1);
INSERT INTO [dbo].[Page] ([Name], [Status]) VALUES('Search',1);
INSERT INTO [dbo].[Page] ([Name], [Status]) VALUES('Alert',1);
INSERT INTO [dbo].[Page] ([Name], [Status]) VALUES('Status',1);

--	Description	:	Inserting Default values for FieldGroup Table
--	Since		:	1.0
--	Date		:	25-June-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[FieldGroup] ([PageId], [Name], [Status]) SELECT [Page].[Id], 'BillDetail',1 FROM [dbo].[Page] WHERE [Page].[Name] = 'Mortgage';
INSERT INTO [dbo].[FieldGroup] ([PageId], [Name], [Status]) SELECT [Page].[Id], 'CustomerDetail',1 FROM [dbo].[Page] WHERE [Page].[Name] = 'Mortgage';
INSERT INTO [dbo].[FieldGroup] ([PageId], [Name], [Status]) SELECT [Page].[Id], 'JewelDetail',1 FROM [dbo].[Page] WHERE [Page].[Name] = 'Mortgage';
INSERT INTO [dbo].[FieldGroup] ([PageId], [Name], [Status]) SELECT [Page].[Id], 'LoanDetail',1 FROM [dbo].[Page] WHERE [Page].[Name] = 'Mortgage';
GO

--	Description	:	Inserting Default values for AllowedValue Table
--	Since		:	1.0
--	Date		:	25-June-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('City',1);
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('Country',1);
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('Currency',1);
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('Date',1);
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('Decimal',1);
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('Gender',1);
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('Image',1);
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('Integer',1);
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('ItemType',1);
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('Item',1);
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('State',1);
INSERT INTO [dbo].[AllowedValue] ([Name], [Status]) VALUES('String',1);

--	Description	:	Inserting Default values for FieldType Table
--	Since		:	1.0
--	Date		:	25-June-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[FieldType] ([Name], [Status]) VALUES('Button',1);
INSERT INTO [dbo].[FieldType] ([Name], [Status]) VALUES('ComboBox',1);
INSERT INTO [dbo].[FieldType] ([Name], [Status]) VALUES('CaptureImage',1);
INSERT INTO [dbo].[FieldType] ([Name], [Status]) VALUES('Date',1);
INSERT INTO [dbo].[FieldType] ([Name], [Status]) VALUES('DateTime',1);
INSERT INTO [dbo].[FieldType] ([Name], [Status]) VALUES('Grid',1);
INSERT INTO [dbo].[FieldType] ([Name], [Status]) VALUES('Image',1);
INSERT INTO [dbo].[FieldType] ([Name], [Status]) VALUES('ScanFingerPrint',1);
INSERT INTO [dbo].[FieldType] ([Name], [Status]) VALUES('TextArea',1);
INSERT INTO [dbo].[FieldType] ([Name], [Status]) VALUES('TextField',1);
INSERT INTO [dbo].[FieldType] ([Name], [Status]) VALUES('UploadImage',1);
GO

--	Description	:	Inserting Default values for Field Table
--	Since		:	1.0
--	Date		:	25-June-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'Series', 1, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'BillDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='String';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'BillNo', 2, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'BillDetail' AND [FieldType].[Name] = 'TextField'AND [AllowedValue].[Name] ='String';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'OpenDate', 3, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'BillDetail' AND [FieldType].[Name] = 'Date' AND [AllowedValue].[Name] ='Date';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'ExpireDate', 4, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'BillDetail' AND [FieldType].[Name] = 'Date' AND [AllowedValue].[Name] ='Date';

INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerFirstName', 1, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='String';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerLastName', 2, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='String';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerGender', 3, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'ComboBox' AND [AllowedValue].[Name] ='Gender';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerDOB', 4, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'Date' AND [AllowedValue].[Name] ='Date';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerMobileNumber', 5, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='String';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerEmailId', 6, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='String';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerAddress', 7, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'TextArea' AND [AllowedValue].[Name] ='String';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerCountryId', 8, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'ComboBox' AND [AllowedValue].[Name] ='Country';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerStateId', 9, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'ComboBox' AND [AllowedValue].[Name] ='State';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerCityId', 10, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'ComboBox' AND [AllowedValue].[Name] ='City';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerPINCode', 11, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='String';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerPhoto', 12, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'CaptureImage' AND [AllowedValue].[Name] ='Image';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerThumb', 13, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'ScanFingerPrint' AND [AllowedValue].[Name] ='Image';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerOccupation', 14, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='String';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerProofTypeId', 15, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'ComboBox' AND [AllowedValue].[Name] ='ProofType';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'CustomerProofId', 16, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'CustomerDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='String';

INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'ItemTypeId', 1, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'JewelDetail' AND [FieldType].[Name] = 'ComboBox' AND [AllowedValue].[Name] ='ItemType';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'ItemId', 2, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'JewelDetail' AND [FieldType].[Name] = 'ComboBox' AND [AllowedValue].[Name] ='Item';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'Description', 3, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'JewelDetail' AND [FieldType].[Name] = 'TextArea' AND [AllowedValue].[Name] ='String';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'Quantity', 4, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'JewelDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Integer';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'NetWeight', 5, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'JewelDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Decimal';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'GrossWeight', 6, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'JewelDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Decimal';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'MarketValueAmount', 7, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'JewelDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Currency';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'LoanAmount', 8, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'JewelDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Currency';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'Purity', 9, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'JewelDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Decimal';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'AddItem', 9, NULL, [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'JewelDetail' AND [FieldType].[Name] = 'Button';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'JewelGrid', 9, NULL, [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'JewelDetail' AND [FieldType].[Name] = 'Grid';

INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'TotalGrossWeight', 1, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'LoanDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Decimal';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'TotalNetWeight', 2, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'LoanDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Decimal';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'TotalMarketValueAmount', 3, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'LoanDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Currency';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'TotalLoanAmount', 4, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'LoanDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Currency';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'PrincipalAmount', 5, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'LoanDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Currency';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'AdvanceAmount',  6, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'LoanDetail' AND [FieldType].[Name] = 'TextField' AND [AllowedValue].[Name] ='Currency';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'Description',  7, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'LoanDetail' AND [FieldType].[Name] = 'TextArea' AND [AllowedValue].[Name] ='String';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'NetPaidAmount',  7, [AllowedValue].[Id], [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[AllowedValue], [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'LoanDetail' AND [FieldType].[Name] = 'TextArea' AND [AllowedValue].[Name] ='Currency';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'Submit',  7, NULL, [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'LoanDetail' AND [FieldType].[Name] = 'Button';
INSERT INTO [dbo].[Field] ([FieldGroupId], [Name], [Sort], [AllowedValueId], [FieldTypeId], [Action], [Status]) SELECT [FieldGroup].[Id], 'Clear',  7, NULL, [FieldType].[Id], NULL, 1 FROM [dbo].[FieldGroup] , [dbo].[FieldType] WHERE [FieldGroup].[Name] = 'LoanDetail' AND [FieldType].[Name] = 'Button';
GO

--	Description	:	Inserting Default values for Operator Table for Filter
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[Filter]([Name], [Status]) VALUES('EqualTo',1);
INSERT INTO [dbo].[Filter]([Name], [Status]) VALUES('LessThan',1);
INSERT INTO [dbo].[Filter]([Name], [Status]) VALUES('LessThanOrEqualTo',1);
INSERT INTO [dbo].[Filter]([Name], [Status]) VALUES('GreaterThan',1);
INSERT INTO [dbo].[Filter]([Name], [Status]) VALUES('GreaterThanOrEqualTo',1);
INSERT INTO [dbo].[Filter]([Name], [Status]) VALUES('NoEqualTo',1);
INSERT INTO [dbo].[Filter]([Name], [Status]) VALUES('StartsWith',1);
INSERT INTO [dbo].[Filter]([Name], [Status]) VALUES('EndsWith',1);
INSERT INTO [dbo].[Filter]([Name], [Status]) VALUES('Contains',1);
INSERT INTO [dbo].[Filter]([Name], [Status]) VALUES('In',1);
GO

--	Description	:	Inserting Values at Version Table and Notification for New Installation
--	Since		:	1.0
--	Date		:	17-MAY-2015
--	Author		:	Shree Soft Soultions
INSERT INTO [dbo].[AppVersion]([Name], [Display]) VALUES('New Installation 1.0','1.0');
GO
INSERT INTO [dbo].[ActivityLog]([Message],[CreatedBy]) VALUES ('PawnShopManagementSystem Database Tables Default Values Insertion Completed','System');
GO