﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.DAL;
using PSMS.Log4NetLibrary;
using System.Configuration;
using PSMS.Entities;
using System.Data.SqlClient;

namespace PSMS.BAL
{
    public class CustomerBO
    {
        private String connectionString;
        private CustomerDAO customerDAO;        
        private ILogService log = new LogService(typeof(CustomerBO));

        public CustomerBO() 
        {
            log.Info("Entering into CustomerBO Default Constructor");
            customerDAO = new CustomerDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Customer> getAllCustomers()
        {
            log.Info("Entering into CustomerBO getAllCustomers Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return customerDAO.getAllCustomers(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Customer getCustomerById(int customerId)
        {
            log.Info("Entering into CustomerBO getCustomerById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return customerDAO.getCustomerById(connection, customerId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
