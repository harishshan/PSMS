﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using PSMS.DAL;
using System.Configuration;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class FieldTypeBO
    {
        private String connectionString;
        private FieldTypeDAO fieldTypeDAO;
        private ILogService log = new LogService(typeof(FieldTypeBO));

        public FieldTypeBO() 
        {
            log.Info("Entering into FieldTypeBO Default Constructor");
            fieldTypeDAO = new FieldTypeDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<FieldType> getAllFieldTypes()
        {
            log.Info("Entering into FieldTypeBO getAllFieldTypes Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return fieldTypeDAO.getAllFieldTypes(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldTypeBO getAllFieldTypes Method");
            }
        }

        public FieldType getFieldTypeById(int fieldTypeId)
        {
            log.Info("Entering into FieldTypeBO getFieldTypeById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return fieldTypeDAO.getFieldTypeById(connection, fieldTypeId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldTypeBO getFieldTypeById Method");
            }
        }        
    }
}
