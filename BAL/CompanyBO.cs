﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.DAL;
using PSMS.Log4NetLibrary;
using System.Configuration;
using System.Data.SqlClient;
using PSMS.Entities;

namespace PSMS.BAL
{
    public class CompanyBO
    {
        private String connectionString;
        private CompanyDAO companyDAO;        
        private ILogService log = new LogService(typeof(CompanyBO));

        public CompanyBO() 
        {
            log.Info("Entering into CompanyDB Default Constructor");
            companyDAO = new CompanyDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Company> getAllCompanies()
        {
            log.Info("Entering into CompanyBO getAllCompanies Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return companyDAO.getAllCompanies(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CompanyBO getAllCompanies Method");
            }
        }
        public ICollection<Company> getAllCompaniesForLoginPage()
        {
            log.Info("Entering into CompanyBO getAllCompanies Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    ICollection<Company> companies = companyDAO.getAllCompanies(connection);
                    foreach (Company company in companies)
                    {
                        setLanguageThemeColor(company);
                    }                    
                    return companies;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CompanyBO getAllCompaniesForLoginPage Method");
            }
        }
        public Company getCompanyById(int companyId)
        {
            log.Info("Entering into CompanyBO getCompanyById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return companyDAO.getCompanyById(connection, companyId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CompanyBO getCompanyById Method");
            }
        }
        public Company getCompanyDetailsById(int companyId)
        {
            log.Info("Entering into CompanyBO getCompanyDetailsById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return setLanguageThemeColor(companyDAO.getCompanyById(connection, companyId));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CompanyBO getCompanyDetailsById Method");
            }
        }
        private Company setLanguageThemeColor(Company company)
        {
            log.Info("Entering into CompanyBO setLanguageThemeColor Method");
            try
            {
                LanguageBO languageBO = new LanguageBO();
                ThemeBO themeBO = new ThemeBO();
                ColorBO colorBO = new ColorBO();
                if (company.LanguageId != null && company.LanguageId.Id > 0)
                {                                
                    Language language=languageBO.getLanguageById(company.LanguageId.Id);
                    company.LanguageId = language;
                }
                if (company.ThemeId != null && company.ThemeId.Id > 0)
                {
                    Theme theme = themeBO.getThemeById(company.ThemeId.Id);
                    company.ThemeId = theme;
                }
                if (company.ColorId != null && company.ColorId.Id > 0)
                {
                    Color color = colorBO.getColorById(company.ColorId.Id);
                    company.ColorId = color;
                }
                return company;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CompanyBO setLanguageThemeColor Method");
            }
        }
    }
}
