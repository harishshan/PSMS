﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Configuration;
using PSMS.Log4NetLibrary;
using PSMS.DAL;

namespace PSMS.BAL
{
    public class CoreUserBO
    {
        private String connectionString;
        private CoreUserDAO coreUserDAO;        
        private ILogService log = new LogService(typeof(CoreUserBO));

        public CoreUserBO() 
        {
            log.Info("Entering into CoreUserBO Default Constructor");
            coreUserDAO = new CoreUserDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
            log.Info("Exiting from CoreUserBO Default Constructor");
        }

        public ICollection<CoreUser> getAllCoreUsers()
        {
            log.Info("Entering into CoreUserBO getAllCoreUsers Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return coreUserDAO.getAllCoreUsers(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CoreUserBO getAllCoreUsers Method");
            }
        }

        public CoreUser getCoreUserById(int coreUserId)
        {
            log.Info("Entering into CoreUserBO getCoreUserById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return coreUserDAO.getCoreUserById(connection, coreUserId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CoreUserBO getCoreUserById Method");
            }
        }
        public CoreUser getCoreUserByUsername(String username)
        {
            log.Info("Entering into CoreUserBO getCoreUserByUsername Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return coreUserDAO.getCoreUserByUsername(connection, username);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CoreUserBO getCoreUserByUsername Method");
            }
        }
    }
}
