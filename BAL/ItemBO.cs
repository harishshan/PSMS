﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.DAL;
using PSMS.Log4NetLibrary;
using System.Configuration;
using PSMS.Entities;
using System.Data.SqlClient;

namespace PSMS.BAL
{
    public class ItemBO
    {
        private String connectionString;
        private ItemDAO itemDAO;        
        private ILogService log = new LogService(typeof(ItemBO));

        public ItemBO() 
        {
            log.Info("Entering into ItemBO Default Constructor");
            itemDAO = new ItemDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Item> getAllItems()
        {
            log.Info("Entering into ItemBO getAllItems Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return itemDAO.getAllItems(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Item getItemById(int itemId)
        {
            log.Info("Entering into ItemBO getItemById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return itemDAO.getItemById(connection, itemId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
