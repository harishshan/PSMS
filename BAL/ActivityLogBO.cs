﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using PSMS.DAL;
using System.Configuration;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class ActivityLogBO
    {
        private String connectionString;
        private ActivityLogDAO activityLogDAO;        
        private ILogService log = new LogService(typeof(ActivityLogBO));

        public ActivityLogBO() 
        {
            log.Info("Entering into ActivityLogBO Default Constructor");
            activityLogDAO = new ActivityLogDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<ActivityLog> getAllActivityLogs()
        {
            log.Info("Entering into ActivityLogBO getAllActivityLogs Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return activityLogDAO.getAllActivityLogs(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActivityLog getActivityLogById(int activityLogId)
        {
            log.Info("Entering into ActivityLogBO getActivityLogById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return activityLogDAO.getActivityLogById(connection, activityLogId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
