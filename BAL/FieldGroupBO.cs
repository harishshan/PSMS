﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Configuration;
using PSMS.DAL;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class FieldGroupBO
    {
        private String connectionString;
        private FieldGroupDAO fieldGroupDAO;        
        private ILogService log = new LogService(typeof(FieldGroupBO));

        public FieldGroupBO() 
        {
            log.Info("Entering into FieldGroupBO Default Constructor");
            fieldGroupDAO = new FieldGroupDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<FieldGroup> getAllFieldGroups()
        {
            log.Info("Entering into FieldGroupBO getAllFieldGroups Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return fieldGroupDAO.getAllFieldGroups(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupBO getAllFieldGroups Method");
            }
        }

        public FieldGroup getFieldGroupById(int fieldGroupId)
        {
            log.Info("Entering into FieldGroupBO getFieldGroupById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return fieldGroupDAO.getFieldGroupById(connection, fieldGroupId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupBO getFieldGroupById Method");
            }
        }
        public ICollection<FieldGroup> getFieldGroupsByPageId(int pageId)
        {
            log.Info("Entering into FieldGroupBO getFieldGroupByPageId Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return fieldGroupDAO.getFieldGroupsByPageId(connection, pageId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupBO getFieldGroupByPageId Method");
            }
        }
        public ICollection<FieldGroup> getFieldGroupsDetailsByPageId(int pageId)
        {
            log.Info("Entering into FieldGroupBO getFieldGroupsDetailsByPageId Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    ICollection<FieldGroup> fieldGroups = fieldGroupDAO.getFieldGroupsByPageId(connection, pageId);
                    foreach (FieldGroup fieldGroup in fieldGroups)
                    {
                        setField(fieldGroup);
                    }
                    return fieldGroups;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupBO getFieldGroupsDetailsByPageId Method");
            }
        }
        public ICollection<FieldGroup> getFieldGroupsAllDetailsByPageId(int pageId)
        {
            log.Info("Entering into FieldGroupBO getFieldGroupsAllDetailsByPageId Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    ICollection<FieldGroup> fieldGroups = fieldGroupDAO.getFieldGroupsByPageId(connection, pageId);
                    foreach (FieldGroup fieldGroup in fieldGroups)
                    {
                        setFieldDetails(fieldGroup);
                    }
                    return fieldGroups;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupBO getFieldGroupsAllDetailsByPageId Method");
            }
        }
        public ICollection<FieldGroup> getFieldGroupsAllDetailsSortedByPageId(int pageId)
        {
            log.Info("Entering into FieldGroupBO getFieldGroupsAllDetailsSortedByPageId Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    ICollection<FieldGroup> fieldGroups = fieldGroupDAO.getFieldGroupsSortedByPageId(connection, pageId);
                    foreach (FieldGroup fieldGroup in fieldGroups)
                    {
                        setFieldDetails(fieldGroup);
                    }
                    return fieldGroups;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupBO getFieldGroupsAllDetailsSortedByPageId Method");
            }
        }
        private FieldGroup setField(FieldGroup fieldGroup)
        {
            log.Info("Entering into FieldGroupBO setField Method");
            try
            {
                FieldBO fieldBO = new FieldBO();
                if (fieldGroup != null)
                {
                    ICollection<Field> fields = fieldBO.getFieldsByFieldGroupId(fieldGroup.Id);
                    fieldGroup.FieldIds = fields;
                }
                return fieldGroup;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupBO setField Method");
            }
        }
        private FieldGroup setFieldDetails(FieldGroup fieldGroup)
        {
            log.Info("Entering into FieldGroupBO setFieldDetails Method");
            try
            {
                FieldBO fieldBO = new FieldBO();
                if (fieldGroup != null)
                {
                    ICollection<Field> fields = fieldBO.getFieldsDetailsByFieldGroupId(fieldGroup.Id);
                    fieldGroup.FieldIds = fields;
                }
                return fieldGroup;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupBO setFieldDetails Method");
            }
        }
        private FieldGroup setFieldDetailsSorted(FieldGroup fieldGroup)
        {
            log.Info("Entering into FieldGroupBO setFieldDetailsSorted Method");
            try
            {
                FieldBO fieldBO = new FieldBO();
                if (fieldGroup != null)
                {
                    ICollection<Field> fields = fieldBO.getFieldsDetailsSortedByFieldGroupId(fieldGroup.Id);
                    fieldGroup.FieldIds = fields;
                }
                return fieldGroup;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupBO setFieldDetailsSorted Method");
            }
        }
    }
}
