﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using PSMS.DAL;
using System.Configuration;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class NotificationBO
    {
        private String connectionString;
        private NotificationDAO notificationDAO;        
        private ILogService log = new LogService(typeof(NotificationBO));

        public NotificationBO() 
        {
            log.Info("Entering into NotificationBO Default Constructor");
            notificationDAO = new NotificationDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Notification> getAllNotifications()
        {
            log.Info("Entering into NotificationBO getAllNotifications Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return notificationDAO.getAllNotifications(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Notification getNotificationById(int notificationId)
        {
            log.Info("Entering into NotificationBO getNotificationById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return notificationDAO.getNotificationById(connection, notificationId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
