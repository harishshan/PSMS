﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.DAL;
using PSMS.Entities;
using System.Configuration;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class ColorBO
    {
        private String connectionString;
        private ColorDAO colorDAO;        
        private ILogService log = new LogService(typeof(ColorBO));

        public ColorBO() 
        {
            log.Info("Entering into ColorBO Default Constructor");
            colorDAO = new ColorDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Color> getAllColors()
        {
            log.Info("Entering into ColorBO getAllColors Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return colorDAO.getAllColors(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Color getColorById(int colorId)
        {
            log.Info("Entering into ColorBO getColorById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return colorDAO.getColorById(connection, colorId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}