﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.DAL;
using System.Configuration;
using PSMS.Entities;
using System.Data.SqlClient;

namespace PSMS.BAL
{
    public class JewelStateBO
    {
        private String connectionString;
        private JewelStateDAO jewelStateDAO;        
        private ILogService log = new LogService(typeof(JewelStateBO));

        public JewelStateBO() 
        {
            log.Info("Entering into JewelStateBO Default Constructor");
            jewelStateDAO = new JewelStateDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<JewelState> getAllJewelStates()
        {
            log.Info("Entering into JewelStateBO getAllJewelStates Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return jewelStateDAO.getAllJewelStates(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JewelState getJewelStateById(int jewelStateId)
        {
            log.Info("Entering into JewelStateBO getJewelStateById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return jewelStateDAO.getJewelStateById(connection, jewelStateId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
