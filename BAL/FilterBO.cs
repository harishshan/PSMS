﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Configuration;
using PSMS.DAL;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class FilterBO
    {
        private String connectionString;
        private FilterDAO filterDAO;        
        private ILogService log = new LogService(typeof(FilterBO));

        public FilterBO() 
        {
            log.Info("Entering into FilterBO Default Constructor");
            filterDAO = new FilterDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Filter> getAllFilters()
        {
            log.Info("Entering into FilterBO getAllFilters Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return filterDAO.getAllFilters(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Filter getFilterById(int filterId)
        {
            log.Info("Entering into FilterBO getFilterById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return filterDAO.getFilterById(connection, filterId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
