﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Configuration;
using PSMS.DAL;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class LoanBO
    {
        private String connectionString;
        private LoanDAO loanDAO;        
        private ILogService log = new LogService(typeof(LoanBO));

        public LoanBO() 
        {
            log.Info("Entering into LoanBO Default Constructor");
            loanDAO = new LoanDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Loan> getAllLoans()
        {
            log.Info("Entering into LoanBO getAllLoans Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return loanDAO.getAllLoans(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Loan getLoanById(int loanId)
        {
            log.Info("Entering into LoanBO getLoanById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return loanDAO.getLoanById(connection, loanId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
