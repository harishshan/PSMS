﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.DAL;
using PSMS.Log4NetLibrary;
using System.Configuration;
using PSMS.Entities;
using System.Data.SqlClient;

namespace PSMS.BAL
{
    public class ItemTypeGroupMappingBO
    {
        private String connectionString;
        private ItemTypeGroupMappingDAO itemTypeGroupMappingDAO;        
        private ILogService log = new LogService(typeof(ItemTypeGroupMappingBO));

        public ItemTypeGroupMappingBO() 
        {
            log.Info("Entering into ItemTypeGroupMappingBO Default Constructor");
            itemTypeGroupMappingDAO = new ItemTypeGroupMappingDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<ItemTypeGroupMapping> getAllItemTypeGroupMappings()
        {
            log.Info("Entering into ItemTypeGroupMappingBO getAllItemTypeGroupMappings Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return itemTypeGroupMappingDAO.getAllItemTypeGroupMappings(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ItemTypeGroupMapping getItemTypeGroupMappingById(int itemTypeGroupMappingId)
        {
            log.Info("Entering into ItemTypeGroupMappingBO getItemTypeGroupMappingById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return itemTypeGroupMappingDAO.getItemTypeGroupMappingById(connection, itemTypeGroupMappingId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
