﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.DAL;
using PSMS.Entities;
using System.Configuration;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class LanguageBO
    {
        private String connectionString;
        private LanguageDAO languageDAO;        
        private ILogService log = new LogService(typeof(LanguageBO));

        public LanguageBO() 
        {
            log.Info("Entering into LanguageBO Default Constructor");
            languageDAO = new LanguageDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Language> getAllLanguages()
        {
            log.Info("Entering into LanguageBO getAllLanguages Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return languageDAO.getAllLanguages(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Language getLanguageById(int languageId)
        {
            log.Info("Entering into LanguageBO getLanguageById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return languageDAO.getLanguageById(connection, languageId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
