﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.DAL;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Configuration;

namespace PSMS.BAL
{
    public class StateBO
    {
        private String connectionString;
        private StateDAO stateDAO;        
        private ILogService log = new LogService(typeof(StateBO));

        public StateBO() 
        {
            log.Info("Entering into StateBO Default Constructor");
            stateDAO = new StateDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<State> getAllStates()
        {
            log.Info("Entering into StateBO getAllStates Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return stateDAO.getAllStates(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public State getStateById(int stateId)
        {
            log.Info("Entering into StateBO getStateById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return stateDAO.getStateById(connection, stateId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
