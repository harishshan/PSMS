﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.DAL;
using System.Configuration;
using System.Data.SqlClient;
using PSMS.Entities;

namespace PSMS.BAL
{
    public class DBAuthenticationTypeBO
    {
        private String connectionString;
        private DBAuthenticationTypeDAO dbAuthenticationTypeDAO;        
        private ILogService log = new LogService(typeof(DBAuthenticationTypeBO));

        public DBAuthenticationTypeBO() 
        {
            log.Info("Entering into DBAuthenticationTypeBO Default Constructor");
            dbAuthenticationTypeDAO = new DBAuthenticationTypeDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<DBAuthenticationType> getAllDBAuthenticationTypes()
        {
            log.Info("Entering into DBAuthenticationTypeBO getAllDBAuthenticationTypes Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return dbAuthenticationTypeDAO.getAllDBAuthenticationTypes(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DBAuthenticationType getDBAuthenticationTypeById(int dbAuthenticationTypeId)
        {
            log.Info("Entering into DBAuthenticationTypeBO getDBAuthenticationTypeById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return dbAuthenticationTypeDAO.getDBAuthenticationTypeById(connection, dbAuthenticationTypeId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
