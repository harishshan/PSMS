﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.DAL;
using PSMS.Log4NetLibrary;
using System.Configuration;
using PSMS.Entities;
using System.Data.SqlClient;

namespace PSMS.BAL
{
    public class GenderBO
    {
        private String connectionString;
        private GenderDAO genderDAO;        
        private ILogService log = new LogService(typeof(GenderBO));

        public GenderBO() 
        {
            log.Info("Entering into GenderBO Default Constructor");
            genderDAO = new GenderDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Gender> getAllGenders()
        {
            log.Info("Entering into GenderBO getAllGenders Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return genderDAO.getAllGenders(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Gender getGenderById(int genderId)
        {
            log.Info("Entering into GenderBO getGenderById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return genderDAO.getGenderById(connection, genderId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
