﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.DAL;
using PSMS.Log4NetLibrary;
using System.Configuration;
using PSMS.Entities;
using System.Data.SqlClient;

namespace PSMS.BAL
{
    public class InterestBO
    {
        private String connectionString;
        private InterestDAO interestDAO;        
        private ILogService log = new LogService(typeof(InterestBO));

        public InterestBO() 
        {
            log.Info("Entering into InterestBO Default Constructor");
            interestDAO = new InterestDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Interest> getAllInterests()
        {
            log.Info("Entering into InterestBO getAllInterests Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return interestDAO.getAllInterests(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Interest getInterestById(int interestId)
        {
            log.Info("Entering into InterestBO getInterestById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return interestDAO.getInterestById(connection, interestId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
