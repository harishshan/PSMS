﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using PSMS.Log4NetLibrary;
using PSMS.DAL;
using System.Configuration;

namespace PSMS.BAL
{
    public class PageBO
    {
        private String connectionString;
        private PageDAO pageDAO;        
        private ILogService log = new LogService(typeof(PageBO));

        public PageBO() 
        {
            log.Info("Entering into PageBO Default Constructor");
            pageDAO = new PageDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Page> getAllPages()
        {
            log.Info("Entering into PageBO getAllPages Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return pageDAO.getAllPages(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageBO getAllPages Method");
            }
        }

        public ICollection<Page> getAllPageDetails()
        {
            log.Info("Entering into PageBO getAllPageDetails Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    ICollection<Page> pages = pageDAO.getAllPages(connection);
                    foreach (Page page in pages)
                    {
                        setFieldGroup(page);
                    }
                    return pages;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageBO getAllPageDetails Method");
            } 
        }
        public ICollection<Page> getAllPageAllDetails()
        {
            log.Info("Entering into PageBO getAllPageDetails Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    ICollection<Page> pages = pageDAO.getAllPages(connection);
                    foreach (Page page in pages)
                    {
                        setFieldGroupDetails(page);
                    }
                    return pages;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageBO getAllPageDetails Method");
            }
        }
        public ICollection<Page> getAllPageAllDetailsSorted()
        {
            log.Info("Entering into PageBO getAllPageAllDetailsSorted Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    ICollection<Page> pages = pageDAO.getAllPagesSorted(connection);
                    foreach (Page page in pages)
                    {
                        setFieldGroupDetailsSorted(page);
                    }
                    return pages;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageBO getAllPageAllDetailsSorted Method");
            }
        }
        public Page getPageById(int pageId)
        {
            log.Info("Entering into PageBO getPageById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return pageDAO.getPageById(connection, pageId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageBO getPageById Method");
            }
        }
        private Page setFieldGroup(Page page)
        {
            log.Info("Entering into PageBO setFieldGroup Method");
            try
            {
                FieldGroupBO fieldGroupBO = new FieldGroupBO();
                if (page!= null)
                {
                    ICollection<FieldGroup> fieldGroups = fieldGroupBO.getFieldGroupsByPageId(page.Id);
                    page.FieldGroupIds = fieldGroups;
                }
                return page;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageBO setFieldGroup Method");
            }
        }
        private Page setFieldGroupDetails(Page page)
        {
            log.Info("Entering into PageBO setFieldGroupDetails Method");
            try
            {
                FieldGroupBO fieldGroupBO = new FieldGroupBO();
                if (page != null)
                {
                    ICollection<FieldGroup> fieldGroups = fieldGroupBO.getFieldGroupsAllDetailsByPageId(page.Id);
                    page.FieldGroupIds = fieldGroups;
                }
                return page;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageBO setFieldGroupDetails Method");
            }
        }
        private Page setFieldGroupDetailsSorted(Page page)
        {
            log.Info("Entering into PageBO setFieldGroupDetailsSorted Method");
            try
            {
                FieldGroupBO fieldGroupBO = new FieldGroupBO();
                if (page != null)
                {
                    ICollection<FieldGroup> fieldGroups = fieldGroupBO.getFieldGroupsAllDetailsSortedByPageId(page.Id);
                    page.FieldGroupIds = fieldGroups;
                }
                return page;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageBO setFieldGroupDetailsSorted Method");
            }
        }
    }
}
