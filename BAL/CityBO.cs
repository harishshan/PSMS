﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.DAL;
using System.Configuration;
using PSMS.Entities;
using System.Data.SqlClient;

namespace PSMS.BAL
{
    public class CityBO
    {
        private String connectionString;
        private CityDAO cityDAO;
        private ILogService log = new LogService(typeof(CityBO));

        public CityBO() 
        {
            log.Info("Entering into CityBO Default Constructor");
            cityDAO = new CityDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<City> getAllCities()
        {
            log.Info("Entering into CityBO getAllColors Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return cityDAO.getAllCities(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public City getCityById(int cityId)
        {
            log.Info("Entering into CityBO getCityById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return cityDAO.getCityById(connection, cityId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
