﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Configuration;
using PSMS.DAL;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class RepayBO
    {
        private String connectionString;
        private RepayDAO repayDAO;        
        private ILogService log = new LogService(typeof(RepayBO));

        public RepayBO() 
        {
            log.Info("Entering into RepayBO Default Constructor");
            repayDAO = new RepayDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Repay> getAllRepays()
        {
            log.Info("Entering into RepayBO getAllRepays Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return repayDAO.getAllRepays(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Repay getRepayById(int repayId)
        {
            log.Info("Entering into RepayBO getRepayById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return repayDAO.getRepayById(connection, repayId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
