﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.DAL;
using PSMS.Log4NetLibrary;
using System.Configuration;
using PSMS.Entities;
using System.Data.SqlClient;

namespace PSMS.BAL
{
    public class CurrencyBO
    {
        private String connectionString;
        private CurrencyDAO currencyDAO;        
        private ILogService log = new LogService(typeof(CurrencyBO));

        public CurrencyBO() 
        {
            log.Info("Entering into CurrencyBO Default Constructor");
            currencyDAO = new CurrencyDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Currency> getAllCurrencies()
        {
            log.Info("Entering into CurrencyBO getAllCurrencies Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return currencyDAO.getAllCurrencies(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Currency getCurrencyById(int currencyId)
        {
            log.Info("Entering into CurrencyBO getCurrencyById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return currencyDAO.getCurrencyById(connection, currencyId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
