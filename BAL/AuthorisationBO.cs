﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using PSMS.DAL;
using System.Configuration;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class AuthorisationBO
    {
        private String connectionString;
        private AuthorisationDAO authorisationDAO;        
        private ILogService log = new LogService(typeof(AuthorisationBO));

        public AuthorisationBO() 
        {
            log.Info("Entering into AuthorisationBO Default Constructor");
            authorisationDAO = new AuthorisationDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Authorisation> getAllAuthorisations()
        {
            log.Info("Entering into AuthorisationBO getAllAuthorisations Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return authorisationDAO.getAllAuthorisations(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Authorisation getAuthorisationById(int authorisationId)
        {
            log.Info("Entering into AuthorisationBO getAuthorisationById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return authorisationDAO.getAuthorisationById(connection, authorisationId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
