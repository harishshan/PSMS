﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.DAL;
using System.Configuration;
using System.Data.SqlClient;
using PSMS.Entities;

namespace PSMS.BAL
{
    public class InterestTypeBO
    {
        private String connectionString;
        private InterestTypeDAO interestTypeDAO;        
        private ILogService log = new LogService(typeof(InterestTypeBO));

        public InterestTypeBO() 
        {
            log.Info("Entering into InterestTypeBO Default Constructor");
            interestTypeDAO = new InterestTypeDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<InterestType> getAllInterestTypes()
        {
            log.Info("Entering into InterestTypeBO getAllInterestTypes Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return interestTypeDAO.getAllInterestTypes(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InterestType getInterestTypeById(int interestTypeId)
        {
            log.Info("Entering into InterestTypeBO getInterestTypeById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return interestTypeDAO.getInterestTypeById(connection, interestTypeId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
