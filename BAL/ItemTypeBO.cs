﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.DAL;
using PSMS.Log4NetLibrary;
using System.Configuration;
using System.Data.SqlClient;
using PSMS.Entities;

namespace PSMS.BAL
{
    public class ItemTypeBO
    {
        private String connectionString;
        private ItemTypeDAO itemTypeDAO;        
        private ILogService log = new LogService(typeof(ItemTypeBO));

        public ItemTypeBO() 
        {
            log.Info("Entering into ItemTypeBO Default Constructor");
            itemTypeDAO = new ItemTypeDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<ItemType> getAllItemTypes()
        {
            log.Info("Entering into ItemTypeBO getAllItemTypes Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return itemTypeDAO.getAllItemTypes(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ItemType getItemTypeById(int itemTypeId)
        {
            log.Info("Entering into ColorBO getColorById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return itemTypeDAO.getItemTypeById(connection, itemTypeId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
