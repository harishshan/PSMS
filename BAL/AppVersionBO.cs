﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.DAL;
using PSMS.Log4NetLibrary;
using System.Configuration;
using System.Data.SqlClient;
using PSMS.Entities;

namespace PSMS.BAL
{
    public class AppVersionBO
    {
        private String connectionString;
        private AppVersionDAO appVersionDAO;        
        private ILogService log = new LogService(typeof(AppVersionBO));

        public AppVersionBO() 
        {
            log.Info("Entering into AppVersionBO Default Constructor");
            appVersionDAO = new AppVersionDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<AppVersion> getAllAppVersions()
        {
            log.Info("Entering into AppVersionBO getAllAppVersions Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return appVersionDAO.getAllAppVersions(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AppVersion getAppVersionById(int appVersionId)
        {
            log.Info("Entering into AppVersionBO getAppVersionById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return appVersionDAO.getAppVersionById(connection, appVersionId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
