﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.DAL;
using System.Configuration;
using PSMS.Entities;
using System.Data.SqlClient;

namespace PSMS.BAL
{
    public class ItemTypeGroupBO
    {
        private String connectionString;
        private ItemTypeGroupDAO itemTypeGroupDAO;        
        private ILogService log = new LogService(typeof(ItemTypeGroupBO));

        public ItemTypeGroupBO() 
        {
            log.Info("Entering into ItemTypeGroupBO Default Constructor");
            itemTypeGroupDAO = new ItemTypeGroupDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<ItemTypeGroup> getAllItemTypeGroups()
        {
            log.Info("Entering into ItemTypeGroupBO getAllItemTypeGroups Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return itemTypeGroupDAO.getAllItemTypeGroups(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ItemTypeGroup getItemTypeGroupById(int itemTypeGroupId)
        {
            log.Info("Entering into ItemTypeGroupBO getItemTypeGroupById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return itemTypeGroupDAO.getItemTypeGroupById(connection, itemTypeGroupId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
