﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.DAL;
using PSMS.Log4NetLibrary;
using System.Configuration;
using PSMS.Entities;
using System.Data.SqlClient;

namespace PSMS.BAL
{
    public class FieldBO
    {
        private String connectionString;
        private FieldDAO fieldDAO;        
        private ILogService log = new LogService(typeof(FieldBO));

        public FieldBO() 
        {
            log.Info("Entering into FieldBO Default Constructor");
            fieldDAO = new FieldDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Field> getAllFields()
        {
            log.Info("Entering into FieldBO getAllFields Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return fieldDAO.getAllFields(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldBO getAllFields Method");
            }
        }

        public Field getFieldById(int fieldId)
        {
            log.Info("Entering into FieldBO getFieldById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return fieldDAO.getFieldById(connection, fieldId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldBO getFieldById Method");
            }
        }
        public ICollection<Field> getFieldsByFieldGroupId(int fieldGroupId)
        {
            log.Info("Entering into FieldBO getFieldByFieldGroupId Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return fieldDAO.getFieldsByFieldGroupId(connection, fieldGroupId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldBO getFieldsByFieldGroupId Method");
            }
        }
        public ICollection<Field> getFieldsDetailsByFieldGroupId(int fieldGroupId)
        {
            log.Info("Entering into FieldBO getFieldByFieldGroupId Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    ICollection<Field> fields = fieldDAO.getFieldsByFieldGroupId(connection, fieldGroupId);
                    foreach (Field field in fields)
                    {
                        setFieldTypeAllowedValue(field);
                    }
                    return fields;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldBO getFieldsByFieldGroupId Method");
            }
        }
        public ICollection<Field> getFieldsDetailsSortedByFieldGroupId(int fieldGroupId)
        {
            log.Info("Entering into FieldBO getFieldByFieldGroupId Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    ICollection<Field> fields = fieldDAO.getFieldsSortedByFieldGroupId(connection, fieldGroupId);
                    foreach (Field field in fields)
                    {
                        setFieldTypeAllowedValue(field);
                    }
                    return fields;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldBO getFieldsByFieldGroupId Method");
            }
        }
        private Field setFieldTypeAllowedValue(Field field)
        {
            log.Info("Entering into FieldGroupBO setField Method");
            try
            {
                FieldTypeBO fieldTypeBO = new FieldTypeBO();
                AllowedValueBO allowedValueBO = new AllowedValueBO();
                if (field != null && field.FieldTypeId != null && field.FieldTypeId.Id >0)
                {
                    FieldType fieldType = fieldTypeBO.getFieldTypeById(field.FieldTypeId.Id);
                    field.FieldTypeId = fieldType;
                }
                if(field != null && field.AllowedValueId != null && field.AllowedValueId.Id >0 )
                {
                    AllowedValue allowedValue = allowedValueBO.getAllowedValueById(field.AllowedValueId.Id);
                    field.AllowedValueId = allowedValue;
                }
                return field;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageBO setLanguageThemeColor Method");
            }
        }
    }
}