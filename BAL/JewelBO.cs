﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Configuration;
using PSMS.DAL;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class JewelBO
    {
        private String connectionString;
        private JewelDAO jewelDAO;        
        private ILogService log = new LogService(typeof(JewelBO));

        public JewelBO() 
        {
            log.Info("Entering into JewelBO Default Constructor");
            jewelDAO = new JewelDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Jewel> getAllJewels()
        {
            log.Info("Entering into JewelBO getAllJewels Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return jewelDAO.getAllJewels(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Jewel getJewelById(int jewelId)
        {
            log.Info("Entering into JewelBO getJewelById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return jewelDAO.getJewelById(connection, jewelId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
