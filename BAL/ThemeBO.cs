﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.DAL;
using System.Configuration;
using System.Data.SqlClient;
using PSMS.Entities;

namespace PSMS.BAL
{
    public class ThemeBO
    {
        private String connectionString;
        private ThemeDAO themeDAO;        
        private ILogService log = new LogService(typeof(ThemeBO));

        public ThemeBO() 
        {
            log.Info("Entering into ThemeBO Default Constructor");
            themeDAO = new ThemeDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Theme> getAllThemes()
        {
            log.Info("Entering into ThemeBO getAllThemes Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return themeDAO.getAllThemes(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Theme getThemeById(int themeId)
        {
            log.Info("Entering into ThemeBO getThemeById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return themeDAO.getThemeById(connection, themeId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}