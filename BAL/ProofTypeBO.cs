﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Configuration;
using PSMS.DAL;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class ProofTypeBO
    {
        private String connectionString;
        private ProofTypeDAO proofTypeDAO;        
        private ILogService log = new LogService(typeof(ProofTypeBO));

        public ProofTypeBO() 
        {
            log.Info("Entering into ProofTypeBO Default Constructor");
            proofTypeDAO = new ProofTypeDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<ProofType> getAllProofTypes()
        {
            log.Info("Entering into ProofTypeBO getAllProofTypes Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return proofTypeDAO.getAllProofTypes(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProofType getProofTypeById(int proofTypeId)
        {
            log.Info("Entering into ProofTypeBO getProofTypeById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return proofTypeDAO.getProofTypeById(connection, proofTypeId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
