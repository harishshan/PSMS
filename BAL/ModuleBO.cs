﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Configuration;
using PSMS.DAL;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class ModuleBO
    {
        private String connectionString;
        private ModuleDAO moduleDAO;        
        private ILogService log = new LogService(typeof(ModuleBO));

        public ModuleBO() 
        {
            log.Info("Entering into ModuleBO Default Constructor");
            moduleDAO = new ModuleDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Module> getAllModules()
        {
            log.Info("Entering into ModuleBO getAllModules Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return moduleDAO.getAllModules(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Module getModuleById(int moduleId)
        {
            log.Info("Entering into ModuleBO getModuleById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return moduleDAO.getModuleById(connection, moduleId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
