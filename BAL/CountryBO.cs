﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.DAL;
using PSMS.Log4NetLibrary;
using System.Configuration;
using PSMS.Entities;
using System.Data.SqlClient;

namespace PSMS.BAL
{
    public class CountryBO
    {
        private String connectionString;
        private CountryDAO countryDAO;        
        private ILogService log = new LogService(typeof(CountryBO));

        public CountryBO() 
        {
            log.Info("Entering into CountryBO Default Constructor");
            countryDAO = new CountryDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Country> getAllCountries()
        {
            log.Info("Entering into CountryBO getAllCountries Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return countryDAO.getAllCountries(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Country getCountryById(int countryId)
        {
            log.Info("Entering into CountryBO getCountryById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return countryDAO.getCountryById(connection, countryId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
