﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Configuration;
using PSMS.Log4NetLibrary;
using PSMS.DAL;

namespace PSMS.BAL
{
    public class RoleBO
    {
        private String connectionString;
        private RoleDAO roleDAO;        
        private ILogService log = new LogService(typeof(RoleBO));

        public RoleBO() 
        {
            log.Info("Entering into RoleBO Default Constructor");
            roleDAO = new RoleDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<Role> getAllRoles()
        {
            log.Info("Entering into RoleBO getAllRoles Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return roleDAO.getAllRoles(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Role getRoleById(int roleId)
        {
            log.Info("Entering into RoleBO getRoleById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return roleDAO.getRoleById(connection, roleId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
