﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Configuration;
using PSMS.DAL;
using PSMS.Log4NetLibrary;

namespace PSMS.BAL
{
    public class AllowedValueBO
    {
        private String connectionString;
        private AllowedValueDAO allowedValueDAO;
        private ILogService log = new LogService(typeof(AllowedValueBO));

        public AllowedValueBO() 
        {
            log.Info("Entering into AllowedValueBO Default Constructor");
            allowedValueDAO = new AllowedValueDAO();
            connectionString = ConfigurationManager.ConnectionStrings["PSMSConnectionString"].ConnectionString;
        }

        public ICollection<AllowedValue> getAllAllowedValues()
        {
            log.Info("Entering into AllowedValueBO getAllAllowedValues Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return allowedValueDAO.getAllAllowedValues(connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AllowedValue getAllowedValueById(int allowedValueId)
        {
            log.Info("Entering into AllowedValueBO getAllowedValueById Method");
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return allowedValueDAO.getAllowedValueById(connection, allowedValueId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
