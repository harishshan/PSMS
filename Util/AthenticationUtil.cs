﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace PSMS.Util
{
    public class AthenticationUtil
    {
        public static String CreateSHAHash(String Password, String Salt)
        {
            try
            {
                SHA512Managed HashTool = new SHA512Managed();
                Byte[] PasswordAsByte = Encoding.UTF8.GetBytes(String.Concat(Password, Salt));
                Byte[] EncryptedBytes = HashTool.ComputeHash(PasswordAsByte);
                HashTool.Clear();
                return Convert.ToBase64String(EncryptedBytes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String Encrypt(String Password)
        {
            try
            {
                return CreateSHAHash(Password, AuthenticationConstants.Salt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
