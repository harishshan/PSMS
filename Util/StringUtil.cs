﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Util
{
    public class StringUtil
    {
        public static Boolean isNotEmpty(String str)
        {
            if (str != null && str.Length > 0 && !str.Equals(""))
            {
                return true;
            }
            return false;
        }
        public static Boolean isEmpty(String str)
        {
            if (str == null || str.Length <= 0 || str.Equals(""))
            {
                return true;
            }
            return false;
        }
    }
}
