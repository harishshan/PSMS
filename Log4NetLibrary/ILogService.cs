﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Log4NetLibrary
{
    public interface ILogService
    {
        void Fatal(object message);
        void Error(object message);
        void Warn(object message);
        void Info(object message);
        void Debug(object message);
        void Error(object message, Exception exception);
    }
}
