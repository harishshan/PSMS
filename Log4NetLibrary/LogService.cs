﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.IO;

namespace PSMS.Log4NetLibrary
{
    public class LogService : ILogService
    {
        readonly ILog logger;
        static LogService()
        {
            var log4NetConfigDirectory = AppDomain.CurrentDomain.RelativeSearchPath ?? AppDomain.CurrentDomain.BaseDirectory;
            var log4NetConfigFilePath = Path.Combine(log4NetConfigDirectory, "log4net.config");
            log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(log4NetConfigFilePath));
        }
        public LogService(Type logClass)
        {
            logger = LogManager.GetLogger(logClass);
        }

        public void Fatal(object errorMessage)
        {
            if (logger.IsFatalEnabled)
                logger.Fatal(errorMessage);
        }
        public void Error(object errorMessage)
        {
            if (logger.IsErrorEnabled)
                logger.Error(errorMessage);            
        }
        public void Error(object message, Exception exception)
        {
            if (logger.IsErrorEnabled)
                logger.Error(message, exception);
        }
        public void Warn(object message)
        {
            if (logger.IsWarnEnabled)
                logger.Warn(message);
        }
        public void Info(object message)
        {
            if (logger.IsInfoEnabled)
                logger.Info(message);
        }
        public void Debug(object message)
        {
            if (logger.IsDebugEnabled)
                logger.Debug(message);            
        }
    }
}
