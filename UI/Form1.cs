﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PSMS.MultiLanguage;

namespace PSMS.UI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            LabelManager labelManager = new LabelManager(typeof(Form1), "PSMS.UI.Resource.Labels");
            labelManager.SetLanguage("ta");
            this.Text = labelManager.getLabel("CompanyName");
        }
    }
}
