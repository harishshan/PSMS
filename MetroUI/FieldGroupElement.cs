﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using PSMS.Entities;

namespace PSMS.MetroUI
{
    public class FieldGroupElement
    {
        public FieldGroupElement()
        {
        }
        public FieldGroupElement(GroupBox groupBox)
        {
            this.groupBox = groupBox;
        }
        public FieldGroupElement(Dictionary<Field, FieldElement> fieldElementMap)
        {
            this.fieldElementMap = fieldElementMap;
        }
        public FieldGroupElement(GroupBox groupBox, Dictionary<Field, FieldElement> fieldElementMap)
        {
            this.groupBox = groupBox;
            this.fieldElementMap = fieldElementMap;
        }
        public void addFieldElement(Field field, FieldElement fieldElement)
        {
            if (fieldElementMap == null)
            {
                fieldElementMap = new Dictionary<Field, FieldElement>();
            }
            fieldElementMap.Add(field, fieldElement);
        }
        public GroupBox groupBox { get; set; }
        public Dictionary<Field, FieldElement> fieldElementMap { get; set; }
        
    }
}
