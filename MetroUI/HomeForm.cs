﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
using MetroFramework;
using PSMS.Log4NetLibrary;
using PSMS.MultiLanguage;
using PSMS.BAL;
using PSMS.Entities;
using PSMS.Util;
using MetroFramework.Components;
using MetroFramework.Controls;
using System.Collections;

namespace PSMS.MetroUI
{
    public partial class HomeForm : MetroForm
    {
        ILogService log = new LogService(typeof(HomeForm));
        LabelManager labelManager = new LabelManager(typeof(HomeForm), "PSMS.MetroUI.Resource.Labels");
        private MetroStyleManager metroStyleManager;
        private CoreUser coreUser;
        public MetroTabControl metroTabControl;
        public HomeForm(CoreUser coreUser)
        {
            try
            {
                log.Info("Entring into HomeForm Constructor");
                this.coreUser = coreUser;
                InitializeComponent();
                Start();
            }
            catch (Exception ex)
            {
                log.Error("Exception :", ex);
            }
            finally
            {
                log.Info("Exiting from HomeForm Constructor");
            }            
        }
        private void Start()
        {
            try
            {
                log.Info("Entering into HomeForm Start Method");
                
                //Init MetroStyleManager
                metroStyleManager = new MetroStyleManager(this.components);
                MetroUIUtil.initMetroStyleManager(this, metroStyleManager);
                MetroUIUtil.setLabelManager(coreUser, labelManager);
                MetroUIUtil.setMetroStyleManager(coreUser, metroStyleManager);
                MetroUIUtil.setNameAndTitle(this, Constants.Labels.HOME_FORM, labelManager.getLabel(Constants.Labels.HOME_FORM) + " - " + labelManager.getLabel(Constants.Labels.PAWN_SHOP_MANAGEMENT_SYSTEM));
                MetroUIUtil.setFullScreen(this);

                int tabControlPageWidth = this.Width - Constants.SIDE_PANEL_WIDTH;
                int tabControlPageHeight = this.Height - Constants.FORM_START_Y - Constants.VSPACE;
                metroTabControl = MetroUIUtil.createMetroTabControl(Constants.FORM_START_X, Constants.FORM_START_Y, tabControlPageWidth, tabControlPageHeight);

                int sidePanelStartX = Constants.FORM_START_X + this.Width - Constants.SIDE_PANEL_WIDTH;
                int sidePanelHeight = this.Height - Constants.FORM_START_Y - Constants.VSPACE;
                MetroPanel sideMetroPanel = MetroUIUtil.createMetroPanel(Constants.Labels.SIDE_PANEL, sidePanelStartX, Constants.FORM_START_Y, Constants.SIDE_PANEL_WIDTH, sidePanelHeight);
                
                PageBO pageBO = new PageBO();
                ICollection<Page> pages = pageBO.getAllPageAllDetails();
                Dictionary<Page, PageElement> pageElementMap = new Dictionary<Page, PageElement>();
                foreach (Page page in pages)
                {
                    int tabPageWidth = tabControlPageWidth - Constants.HSPACE - Constants.HSPACE;
                    int tabPageHeight = tabControlPageHeight - Constants.VSPACE - Constants.VSPACE;
                    MetroTabPage metroTabPage = MetroUIUtil.createMetroTabPage(page.Name, labelManager.getLabel(page.Name), tabPageWidth, tabPageHeight);
                    
                    PageElement pageElement = new PageElement(metroTabPage);
                    metroTabControl.Controls.Add(metroTabPage);
                    ICollection<FieldGroup> fieldGroups = page.FieldGroupIds;
                    foreach (FieldGroup fieldGroup in fieldGroups)
                    {
                        int groupBoxWidth = tabPageWidth - Constants.HSPACE - Constants.HSPACE;
                        int groupBoxHeight = tabPageHeight - Constants.VSPACE - Constants.VSPACE;
                        GroupBox groupBox = MetroUIUtil.createGroupBox(fieldGroup.Name, fieldGroup.Name, metroStyleManager, groupBoxWidth, groupBoxHeight);
                        FieldGroupElement fieldGroupElement = new FieldGroupElement(groupBox);                        
                        TableLayoutPanel tableLayoutPanel = MetroUIUtil.createTableLayoutPanel(fieldGroup.Name + "_tbl", fieldGroup.FieldIds.Count, 2);
                        tableLayoutPanel.Width = groupBoxWidth - Constants.HSPACE - Constants.HSPACE;
                        tableLayoutPanel.Height = groupBoxHeight - Constants.VSPACE - Constants.VSPACE;
                        metroTabPage.Controls.Add(groupBox);
                        groupBox.Controls.Add(tableLayoutPanel);
                        ICollection<Field> fields = fieldGroup.FieldIds;
                        for(int i = 0; i<fields.Count; i++)
                        {
                            Field field = fields.ElementAt(i);
                            if (MetroUIConstants.FieldType.BUTTON.Equals(field.FieldTypeId.Name))
                            {
                                FieldElement fieldElement = MetroUIUtil.createMetroButtonFieldElement(field.Name,null);
                                fieldGroupElement.addFieldElement(field, fieldElement);
                                tableLayoutPanel.Controls.Add(fieldElement.control1,0,i);
                            } 
                            else if (MetroUIConstants.FieldType.COMBOBOX.Equals(field.FieldTypeId.Name))
                            {
                                //TODO
                            }
                            else if (MetroUIConstants.FieldType.CAPTUREIMAGE.Equals(field.FieldTypeId.Name))
                            {
                                //TODO
                            }
                            else if (MetroUIConstants.FieldType.DATE.Equals(field.FieldTypeId.Name))
                            {
                                //TODO
                            }
                            else if (MetroUIConstants.FieldType.DATETIME.Equals(field.FieldTypeId.Name))
                            {
                                //TODO
                            }
                            else if (MetroUIConstants.FieldType.GRID.Equals(field.FieldTypeId.Name))
                            {
                                //TODO
                            }
                            else if (MetroUIConstants.FieldType.IMAGE.Equals(field.FieldTypeId.Name))
                            {
                                //TODO
                            }
                            else if (MetroUIConstants.FieldType.SCANFINGERPRINT.Equals(field.FieldTypeId.Name))
                            {
                                //TODO
                            }
                            else if (MetroUIConstants.FieldType.TEXTAREA.Equals(field.FieldTypeId.Name))
                            {
                                //TODO
                            }
                            else if (MetroUIConstants.FieldType.TEXTFIELD.Equals(field.FieldTypeId.Name))
                            {
                                FieldElement fieldElement = MetroUIUtil.createMetroTextboxFieldElement(field.Name, field.Sort);
                                fieldGroupElement.addFieldElement(field, fieldElement);
                                tableLayoutPanel.Controls.Add(fieldElement.control1,0,i);
                                tableLayoutPanel.Controls.Add(fieldElement.control2,1,i);
                            }
                            else if (MetroUIConstants.FieldType.IMAGEUPLOAD.Equals(field.FieldTypeId.Name))
                            {
                                //TODO
                            }
                        }
                        pageElement.addFieldGroupElement(fieldGroup, fieldGroupElement);
                    }
                    pageElementMap.Add(page, pageElement);
                }                
                this.Controls.Add(metroTabControl);
            }
            catch (Exception ex)
            {
                log.Error("Exception :", ex);
            }
            finally
            {
                log.Info("Exiting from HomeForm Start Method");
            }     
        }
    }
}
