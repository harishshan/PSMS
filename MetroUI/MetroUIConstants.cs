﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using MetroFramework;

namespace PSMS.MetroUI
{
    public class MetroUIConstants
    {
        public class FieldType
        {
            public static String BUTTON = "Button";
            public static String COMBOBOX = "ComboBox";
            public static String CAPTUREIMAGE = "CaptureImage";
            public static String DATE = "Date";
            public static String DATETIME = "DateTime";
            public static String GRID = "Grid";
            public static String IMAGE = "Image";
            public static String SCANFINGERPRINT = "ScanFingerPrint";
            public static String TEXTAREA = "TextArea";
            public static String TEXTFIELD = "TextField";
            public static String IMAGEUPLOAD = "UploadImage";
        }
        public static Hashtable MetroUIColorMap = new Hashtable()
        {
            {MetroColorStyle.Default, System.Drawing.Color.FromArgb(255, 255, 255)},
            {MetroColorStyle.Black, System.Drawing.Color.FromArgb(0, 0, 0)},
            {MetroColorStyle.White, System.Drawing.Color.FromArgb(255, 255, 255)},
            {MetroColorStyle.Silver, System.Drawing.Color.FromArgb(85, 85, 85)},
            {MetroColorStyle.Blue, System.Drawing.Color.FromArgb(0, 174, 219)},
            {MetroColorStyle.Green, System.Drawing.Color.FromArgb(0, 177, 89)},
            {MetroColorStyle.Lime, System.Drawing.Color.FromArgb(142, 188, 0)},
            {MetroColorStyle.Teal, System.Drawing.Color.FromArgb(0, 170, 173)},
            {MetroColorStyle.Orange, System.Drawing.Color.FromArgb(243, 119, 53)},
            {MetroColorStyle.Brown, System.Drawing.Color.FromArgb(165, 81, 0)},
            {MetroColorStyle.Pink, System.Drawing.Color.FromArgb(231, 113, 189)},
            {MetroColorStyle.Magenta, System.Drawing.Color.FromArgb(255, 0, 148)},
            {MetroColorStyle.Purple, System.Drawing.Color.FromArgb(124, 65, 153)},
            {MetroColorStyle.Red, System.Drawing.Color.FromArgb(209, 17, 65)},
            {MetroColorStyle.Yellow, System.Drawing.Color.FromArgb(255, 196, 37)}
        };
    }
}
