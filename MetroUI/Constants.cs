﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.MetroUI
{
    class Constants
    {
        public static  int HSPACE = 30;
        public static  int VSPACE = 50;

        public static  int LABEL_WIDTH = 150;
        public static  int WIDE_LABEL_WIDTH = 200;
        public static  int LABEL_HEIGHT = 20;

        public static  int TEXTBOX_WIDTH = 150;
        public static  int TEXTBOX_HEIGHT = 20;

        public static  int BUTTON_WIDTH = 150;
        public static  int BUTTON_HEIGHT = 20;

        public static int FORM_START_X = 0;
        public static int FORM_START_Y = 50;

        public static int PANEL_START_X = 0;
        public static int PANEL_START_Y = 0;

        public static int MARGIN_HSPACE = 20;
        public static int MARGIN_VSPACE = 50;

        public static int LOGO_WIDTH = 50;
        public static int LOGO_HEIGHT = 50;

        public static int FOOTER_HEIGHT = 70;
        public static int HEADER_HEIGHT = 90;

        public static int SIDE_PANEL_WIDTH = 250;

        public class Labels
        {
            public static String LOGIN_FORM = "Login Form";
            public static String HOME_FORM = "Home Form";
            public static String USERNAME = "Username";
            public static String PASSWORD = "Password";
            public static String LOGIN = "Login";
            public static String POWERED_BY = "PoweredBy";
            public static String SHREE_SOFT_SOLUTIONS = "ShreeSoftSolutions";
            public static String PAWN_SHOP_MANAGEMENT_SYSTEM = "Pawn Shop Management System";
            public static String USERNAME_PASSWORD_WRONG = "Username / Password wrong";
            public static String USERNAME_PASSWORD_SHOULD_BE_BLANK = "Username / Password should not be blank";
            public static String USERNAME_NOT_FOUND = "Username not found";
            public static String LOGIN_FAILED = "Login Failed";
            public static String LOGIN_USER_IS_NOT_ACTIVE = "Login user is not active";
            public static String KINDLY_CONTACT_THE_PAWN_SHOP_MANAGEMENT_SYSTEM_ADMINISTRATOR = "Kindly contact the Pawn Shop Management System Administrator";
            public static String SIDE_PANEL = "Side Panel";
        }
    }
}
