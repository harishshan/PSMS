﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using PSMS.Log4NetLibrary;
using PSMS.MultiLanguage;
using PSMS.BAL;
using PSMS.Entities;
using MetroFramework;
using MetroFramework.Forms;
using MetroFramework.Controls;
using MetroFramework.Components;
using System.Windows.Forms;
using System.IO;
using PSMS.Util;

namespace PSMS.MetroUI
{
    public partial class LoginFrom : MetroForm
    {
        ILogService log = new LogService(typeof(LoginFrom));
        
        LabelManager labelManager = new LabelManager(typeof(LoginFrom), "PSMS.MetroUI.Resource.Labels");

        private MetroStyleManager metroStyleManager;

        private TableLayoutPanel tableLayoutPanel;

        private MetroPanel headerPanel;
        private MetroPanel footerPanel;

        private MetroLabel companyNameLbl;
        private MetroLabel companyAddressLbl;

        private MetroLabel usernameLbl;
        private MetroLabel passwordLbl;        
        private MetroLabel poweredByLbl;

        private MetroTextBox usernameTxt;
        private MetroTextBox passwordTxt;

        private MetroButton loginButton;

        private PictureBox logoPicBox;
        private PictureBox companyLogoPicBox;

        private Company company;
        
        public LoginFrom()
        {
            try
            {
                log.Info("Entring into LoginForm Constructor");
                InitializeComponent();
                Start();
            }
            catch (Exception ex)
            {
                log.Error("Exception :", ex);
            }
            finally
            {
                log.Info("Exiting from LoginForm Constructor");
            }
        }
        public void Start()
        {
            try
            {
                log.Info("Entering into LoginForm Start Method");

                //Init MetroStyleManager
                metroStyleManager = new MetroStyleManager(this.components);
                MetroUIUtil.initMetroStyleManager(this, metroStyleManager);

                //Get all Company Information and Set Language, Theme, Style by First Company
                CompanyBO companyBO = new CompanyBO();
                ICollection<Company> companies = companyBO.getAllCompaniesForLoginPage();
                if (companies != null && companies.Count > 0)
                {
                    List<Company> companyList = companies.Cast<Company>().ToList();
                    company = companyList[0];
                }
                MetroUIUtil.setLabelManager(company, labelManager);
                MetroUIUtil.setMetroStyleManager(company, metroStyleManager);
                
                //Sets Name, Title and  FullScreen Form
                MetroUIUtil.setNameAndTitle(this,Constants.Labels.LOGIN_FORM, labelManager.getLabel(Constants.Labels.LOGIN_FORM) + " - " + labelManager.getLabel(Constants.Labels.PAWN_SHOP_MANAGEMENT_SYSTEM));
                MetroUIUtil.setFullScreen(this);

                this.companyNameLbl = MetroUIUtil.createMetroTitle("companyNameLbl", company.Display);
                this.companyAddressLbl = MetroUIUtil.createMetroSubTitle("companyAddressLbl", company.AddressDisplay);
                this.usernameLbl = MetroUIUtil.createMetroLabel("usernameLbl", labelManager.getLabel(Constants.Labels.USERNAME));
                this.usernameTxt = MetroUIUtil.createMetroTextBox("usernameTxt", 0);
                this.passwordLbl = MetroUIUtil.createMetroLabel("passwordLbl", labelManager.getLabel(Constants.Labels.PASSWORD));
                this.passwordTxt = MetroUIUtil.createMetroPasswordTextBox("passwordTxt", 1);
                this.loginButton = MetroUIUtil.createMetroButton("loginButton", labelManager.getLabel(Constants.Labels.LOGIN), new System.EventHandler(this.loginButton_Click));
                this.poweredByLbl = MetroUIUtil.createMetroBoldWideLabel("poweredByLbl", labelManager.getLabel(Constants.Labels.POWERED_BY) + " " + labelManager.getLabel(Constants.Labels.SHREE_SOFT_SOLUTIONS));
                
                
                //Init TableLayoutPanel add Contorls to tabelLayoutPanel 
                int totalX = Constants.LABEL_WIDTH + Constants.HSPACE + Constants.TEXTBOX_WIDTH;
                int totalY = Constants.LABEL_HEIGHT + Constants.VSPACE + Constants.LABEL_HEIGHT + Constants.VSPACE + Constants.BUTTON_HEIGHT;
                int startX = ((this.Width - totalX) / 2);
                int startY = ((this.Height - totalY) / 2);
                this.tableLayoutPanel = MetroUIUtil.createTableLayoutPanel("tableLayoutPanel", 3, 2, startX, startY, totalX, totalY);
                this.tableLayoutPanel.Controls.Add(this.usernameLbl, 0, 0);
                this.tableLayoutPanel.Controls.Add(this.usernameTxt, 1, 0);
                this.tableLayoutPanel.Controls.Add(this.passwordLbl, 0, 1);
                this.tableLayoutPanel.Controls.Add(this.passwordTxt, 1, 1);
                this.tableLayoutPanel.Controls.Add(this.loginButton, 1, 2);

                //Set Size and Location for footerPanel Controls
                this.footerPanel = MetroUIUtil.createMetroPanel("footerPanel", Constants.FORM_START_X, this.Height - Constants.FOOTER_HEIGHT, this.Width, Constants.FOOTER_HEIGHT);
                
                startX = ((this.Width - Constants.LOGO_WIDTH) / 2);
                startY = Constants.PANEL_START_Y;
                this.logoPicBox = MetroUIUtil.createLogoPictureBox(Image.FromFile("Resource/shree.jpg"), startX, startY, Constants.LOGO_WIDTH, Constants.LOGO_HEIGHT);
                startX = ((this.Width - Constants.WIDE_LABEL_WIDTH) / 2);
                startY = Constants.PANEL_START_Y + Constants.LOGO_HEIGHT;
                this.poweredByLbl.Location = new System.Drawing.Point(startX, startY);

                //Add Contorls to footerPanel 
                this.footerPanel.Controls.Add(this.poweredByLbl);
                this.footerPanel.Controls.Add(this.logoPicBox);

                //Set Size and Location for headerPanel Controls
                this.headerPanel = MetroUIUtil.createMetroPanel("headerPanel", Constants.FORM_START_X, Constants.FORM_START_Y, this.Width, Constants.HEADER_HEIGHT);
                startX = ((this.Width - (company.Display.Length * 7)) / 2);
                startY = Constants.PANEL_START_Y + Constants.LOGO_HEIGHT;
                this.companyNameLbl.Location = new System.Drawing.Point(startX, startY);
                startX = ((this.Width - (company.AddressDisplay.Length * 7)) / 2);
                startY = Constants.PANEL_START_Y + Constants.LOGO_HEIGHT + Constants.LABEL_HEIGHT;
                this.companyAddressLbl.Location = new System.Drawing.Point(startX, startY);

                startX = ((this.Width - Constants.LOGO_WIDTH) / 2);
                startY = Constants.PANEL_START_Y;

                //Add Contorls to headerPanel
                this.headerPanel.Controls.Add(this.companyNameLbl);
                this.headerPanel.Controls.Add(this.companyAddressLbl);
                if (company != null && company.Logo != null)
                {
                    this.companyLogoPicBox = MetroUIUtil.createLogoPictureBox(Image.FromStream(new MemoryStream(company.Logo)), startX, startY, Constants.LOGO_WIDTH, Constants.LOGO_HEIGHT);
                    
                }
                else
                {
                   this.companyLogoPicBox = MetroUIUtil.createLogoPictureBox(Image.FromFile("Resource/Default.png"), startX, startY, Constants.LOGO_WIDTH, Constants.LOGO_HEIGHT);
                }
                this.headerPanel.Controls.Add(this.companyLogoPicBox);
                //Add tabelLayoutPanel,footerPanel to forms
                this.Controls.Add(this.headerPanel);
                this.Controls.Add(this.tableLayoutPanel);
                this.Controls.Add(this.footerPanel);

                MetroUIUtil.endInitMetroStyleManager(metroStyleManager);
            }
            catch (Exception ex)
            {
                log.Error("Exception :", ex);
            }
            finally
            {
                log.Info("Exiting from LoginForm Start Method");
            }
        }
        private void loginButton_Click(object sender, EventArgs e)
        {
            try
            {
                log.Info("Entering into LoginForm loginButton_Click Method");
                String plainPasswordText = passwordTxt.Text;
                String username = usernameTxt.Text;
                if (StringUtil.isNotEmpty(plainPasswordText) && StringUtil.isNotEmpty(username))
                {
                    String passwordHash = AthenticationUtil.Encrypt(passwordTxt.Text);
                    CoreUserBO coreUserBO = new CoreUserBO();
                    CoreUser coreUser = coreUserBO.getCoreUserByUsername(username);
                    if (coreUser != null && passwordHash.Equals(coreUser.Password) && coreUser.Status)
                    {
                        log.Info("Login successful for User[" + username + "]");

                        ColorBO colorBO = new ColorBO();
                        coreUser.ColorId = colorBO.getColorById(coreUser.ColorId.Id);
                        ThemeBO themeBO = new ThemeBO();
                        coreUser.ThemeId = themeBO.getThemeById(coreUser.ThemeId.Id);
                        LanguageBO languageBO = new LanguageBO();
                        coreUser.LanguageId = languageBO.getLanguageById(coreUser.LanguageId.Id);

                        HomeForm homeForm = new HomeForm(coreUser);
                        homeForm.Show();
                        this.Hide();
                    }
                    else if (coreUser != null && passwordHash.Equals(coreUser.Password) && !coreUser.Status)
                    {
                        log.Info("Login[" + username + "] user is not active , Kindly contact the Pawn Shop Management System Admin");
                        MetroMessageBox.Show(this, labelManager.getLabel(Constants.Labels.LOGIN_USER_IS_NOT_ACTIVE) + "," + labelManager.getLabel(Constants.Labels.KINDLY_CONTACT_THE_PAWN_SHOP_MANAGEMENT_SYSTEM_ADMINISTRATOR), labelManager.getLabel(Constants.Labels.LOGIN_FAILED), MessageBoxButtons.OKCancel, MessageBoxIcon.Hand);
                    }
                    else if (coreUser != null && !passwordHash.Equals(coreUser.Password))
                    {
                        log.Info("Password is wrong for the User[" + username + "]");
                        MetroMessageBox.Show(this, labelManager.getLabel(Constants.Labels.USERNAME_PASSWORD_WRONG), labelManager.getLabel(Constants.Labels.LOGIN_FAILED), MessageBoxButtons.OKCancel, MessageBoxIcon.Hand);
                    }
                    else if (coreUser == null)
                    {
                        log.Info("User not found");
                        MetroMessageBox.Show(this, labelManager.getLabel(Constants.Labels.USERNAME_NOT_FOUND), labelManager.getLabel(Constants.Labels.LOGIN_FAILED), MessageBoxButtons.OKCancel, MessageBoxIcon.Hand);
                    }
                }
                else
                {
                    log.Info("Username / Password should not be blank");
                    MetroMessageBox.Show(this, labelManager.getLabel(Constants.Labels.USERNAME_PASSWORD_SHOULD_BE_BLANK), labelManager.getLabel(Constants.Labels.LOGIN_FAILED), MessageBoxButtons.OKCancel, MessageBoxIcon.Hand);                    
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception :", ex);
            }
            finally
            {
                log.Info("Exiting from LoginForm loginButton_Click Method");
            }
        }
    }
}
