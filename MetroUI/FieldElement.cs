﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PSMS.MetroUI
{
    public class FieldElement
    {
        public FieldElement()
        {
        }
        public FieldElement(Control control1)
        {
            this.control1 = control1;
        }
        public FieldElement(Control control1, Control control2)
        {
            this.control1 = control1;
            this.control2 = control2;
        }
        public Control control1 { get; set; }
        public Control control2 { get; set; }
    }
}
