﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using PSMS.MultiLanguage;
using MetroFramework.Components;
using MetroFramework;
using MetroFramework.Forms;
using MetroFramework.Controls;
using System.Windows.Forms;
using System.Drawing;
using PSMS.BAL;

namespace PSMS.MetroUI
{
    public class MetroUIUtil
    {
        public static void initMetroStyleManager(MetroForm metroForm, MetroStyleManager metroStyleManager)
        {
            ((System.ComponentModel.ISupportInitialize)(metroStyleManager)).BeginInit();
            metroForm.StyleManager = metroStyleManager;
            metroStyleManager.Owner = metroForm;
        }
        public static void endInitMetroStyleManager(MetroStyleManager metroStyleManager)
        {
            ((System.ComponentModel.ISupportInitialize)(metroStyleManager)).EndInit();
        }
        public static void setLabelManager(Company company, LabelManager labelManager)
        {
            if (company != null && company.LanguageId != null && company.LanguageId.Code != null)
            {
                labelManager.SetLanguage(company.LanguageId.Code);
            }
            else
            {
                labelManager.SetLanguage("en-US");
            }
        }
        public static void setLabelManager(CoreUser coreUser, LabelManager labelManager)
        {
            if (coreUser != null && coreUser.LanguageId != null && coreUser.LanguageId.Code != null)
            {
                labelManager.SetLanguage(coreUser.LanguageId.Code);
            }
            else
            {
                labelManager.SetLanguage("en-US");
            }
        }
        public static void setMetroStyleManager(Company company, MetroStyleManager metroStyleManager)
        {
            if (company != null && company.ThemeId != null && company.ThemeId.Code != null)
            {
                metroStyleManager.Theme = (MetroThemeStyle)Enum.Parse(typeof(MetroThemeStyle), company.ThemeId.Code);
            }
            else
            {
                metroStyleManager.Theme = MetroThemeStyle.Light;
            }
            if (company != null && company.ColorId != null && company.ColorId.Code != null)
            {
                metroStyleManager.Style = (MetroColorStyle)Enum.Parse(typeof(MetroColorStyle), company.ColorId.Code);
            }
            else
            {
                metroStyleManager.Style = MetroColorStyle.Blue;
            }
        }
        public static void setMetroStyleManager(CoreUser coreUser, MetroStyleManager metroStyleManager)
        {
            if (coreUser != null && coreUser.ThemeId != null && coreUser.ThemeId.Code != null)
            {
                metroStyleManager.Theme = (MetroThemeStyle)Enum.Parse(typeof(MetroThemeStyle), coreUser.ThemeId.Code);
            }
            else
            {
                metroStyleManager.Theme = MetroThemeStyle.Light;
            }
            if (coreUser != null && coreUser.ColorId != null && coreUser.ColorId.Code != null)
            {
                metroStyleManager.Style = (MetroColorStyle)Enum.Parse(typeof(MetroColorStyle), coreUser.ColorId.Code);
            }
            else
            {
                metroStyleManager.Style = MetroColorStyle.Blue;
            }
        }
        public static void setFullScreen(MetroForm metroForm)
        {
            metroForm.Width = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width;
            metroForm.Height = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height;
            //metroForm.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            metroForm.MaximizeBox = false;
            metroForm.Resizable = false;
            metroForm.Movable = false;
        }
        public static void setNameAndTitle(MetroForm metroForm,String name, String title)
        {
            metroForm.Name = name;
            metroForm.Text = title;

        }
        public static MetroLabel createMetroTitle(String name, String text)
        {
            MetroLabel metroLabel = new MetroLabel();
            metroLabel.Name = name;
            metroLabel.Text = text;
            metroLabel.UseStyleColors = true;
            metroLabel.Font = MetroFonts.Title;
            metroLabel.Size = new System.Drawing.Size(text.Length * 7, Constants.LABEL_HEIGHT);
            return metroLabel;
        }
        public static MetroLabel createMetroSubTitle(String name, String text)
        {
            MetroLabel metroLabel = new MetroLabel();
            metroLabel.Name = name;
            metroLabel.Text = text;
            metroLabel.UseStyleColors = true;
            metroLabel.Font = MetroFonts.Subtitle;
            metroLabel.Size = new System.Drawing.Size(text.Length * 7, Constants.LABEL_HEIGHT);
            return metroLabel;
        }
        public static MetroLabel createMetroLabel(String name, String text)
        {
            MetroLabel metroLabel = new MetroLabel();
            metroLabel.Name = name;
            metroLabel.Text = text;
            metroLabel.UseStyleColors = true;
            metroLabel.Font = MetroFonts.Label(MetroLabelSize.Tall, MetroLabelWeight.Regular);
            metroLabel.Size = new System.Drawing.Size(Constants.LABEL_WIDTH, Constants.LABEL_HEIGHT);
            return metroLabel;
        }
        public static MetroLabel createMetroBoldLabel(String name, String text)
        {
            MetroLabel metroLabel = new MetroLabel();
            metroLabel.Name = name;
            metroLabel.Text = text;
            metroLabel.UseStyleColors = true;
            metroLabel.Font = MetroFonts.Label(MetroLabelSize.Tall, MetroLabelWeight.Bold);
            return metroLabel;
        }
        public static MetroLabel createMetroWideLabel(String name, String text)
        {
            MetroLabel metroLabel = new MetroLabel();
            metroLabel.Name = name;
            metroLabel.Text = text;
            metroLabel.UseStyleColors = true;
            metroLabel.Font = MetroFonts.Label(MetroLabelSize.Tall, MetroLabelWeight.Regular);
            metroLabel.Size = new System.Drawing.Size(Constants.WIDE_LABEL_WIDTH, Constants.LABEL_HEIGHT);
            return metroLabel;
        }
        public static MetroLabel createMetroBoldWideLabel(String name, String text)
        {
            MetroLabel metroLabel = new MetroLabel();
            metroLabel.Name = name;
            metroLabel.Text = text;
            metroLabel.UseStyleColors = true;
            metroLabel.Font = MetroFonts.Label(MetroLabelSize.Tall, MetroLabelWeight.Bold);
            metroLabel.Size = new System.Drawing.Size(Constants.WIDE_LABEL_WIDTH, Constants.LABEL_HEIGHT);
            return metroLabel;
        }
        public static MetroTextBox createMetroTextBox(String name, int tabIndex)
        {
            MetroTextBox metroTextBox = new MetroTextBox();
            metroTextBox.Name = name;
            metroTextBox.TabIndex = tabIndex;
            metroTextBox.UseStyleColors = true;
            metroTextBox.IconRight = true;
            metroTextBox.MaxLength = 32767;
            metroTextBox.PasswordChar = '\0';
            metroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            metroTextBox.SelectedText = "";
            metroTextBox.Text = "";
            metroTextBox.UseSelectable = true;
            metroTextBox.Font = MetroFonts.TextBox(MetroTextBoxSize.Tall, MetroTextBoxWeight.Regular);
            metroTextBox.Size = new System.Drawing.Size(Constants.TEXTBOX_WIDTH, Constants.TEXTBOX_HEIGHT);
            return metroTextBox;
        }
        public static MetroTextBox createMetroPasswordTextBox(String name, int tabIndex)
        {
            MetroTextBox metroTextBox = new MetroTextBox();
            metroTextBox.Name = name;
            metroTextBox.TabIndex = tabIndex;
            metroTextBox.UseStyleColors = true;
            metroTextBox.IconRight = true;
            metroTextBox.MaxLength = 32767;
            metroTextBox.PasswordChar = '\u2022';
            metroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            metroTextBox.SelectedText = "";
            metroTextBox.Text = "";
            metroTextBox.UseSelectable = true;
            metroTextBox.Font = MetroFonts.TextBox(MetroTextBoxSize.Tall, MetroTextBoxWeight.Regular);
            metroTextBox.Size = new System.Drawing.Size(Constants.TEXTBOX_WIDTH, Constants.TEXTBOX_HEIGHT);
            return metroTextBox;
        }
        public static MetroButton createMetroButton(String name, String text, EventHandler eventHandler)
        {
            MetroButton metroButton = new MetroButton();
            metroButton.Name = name;
            metroButton.Text = text;
            metroButton.UseStyleColors = true;
            metroButton.UseSelectable = true;
            metroButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            metroButton.Click += eventHandler;
            metroButton.Font = MetroFonts.Button(MetroButtonSize.Tall, MetroButtonWeight.Regular);
            metroButton.Size = new System.Drawing.Size(Constants.BUTTON_WIDTH, Constants.BUTTON_HEIGHT);
            return metroButton;
        }
        public static MetroPanel createMetroPanel(String name, int formStartX, int formStartY, int width, int height)
        {
            MetroPanel metroPanel = new MetroPanel();
            metroPanel.UseStyleColors = true;
            metroPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            metroPanel.Name = name;
            metroPanel.ResumeLayout(false);
            metroPanel.PerformLayout();
            metroPanel.Location = new System.Drawing.Point(formStartX, formStartY);
            metroPanel.Size = new System.Drawing.Size(width, height);
            return metroPanel;
        }
        public static GroupBox createGroupBox(String name, String text, MetroStyleManager metroStyleManager, int width, int height)
        {
            GroupBox groupBox = new GroupBox();
            groupBox.Name = name;
            groupBox.Text = text;
            groupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            groupBox.ResumeLayout(false);
            groupBox.PerformLayout();
            if (metroStyleManager.Theme == MetroThemeStyle.Light)
            {
                groupBox.BackColor = System.Drawing.Color.White;
            }
            else if (metroStyleManager.Theme == MetroThemeStyle.Dark)
            {
                groupBox.BackColor = System.Drawing.Color.Black;
            }
            else
            {
                groupBox.BackColor = System.Drawing.Color.White;
            }
            groupBox.ForeColor = (System.Drawing.Color)MetroUIConstants.MetroUIColorMap[metroStyleManager.Style];
            groupBox.Width = width;
            groupBox.Height = height;
            return groupBox;

        }
        public static TableLayoutPanel createTableLayoutPanel(String name, int rowCount, int columnCount, int formStartX, int formStartY, int width, int height)
        {
            TableLayoutPanel tableLayoutPanel = createTableLayoutPanel(name, rowCount, columnCount);
            tableLayoutPanel.Location = new System.Drawing.Point(formStartX, formStartY);
            tableLayoutPanel.Size = new System.Drawing.Size(width, height);
            return tableLayoutPanel;
        }
        public static TableLayoutPanel createTableLayoutPanel(String name, int rowCount, int columnCount)
        {
            TableLayoutPanel tableLayoutPanel = new TableLayoutPanel();
            tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            tableLayoutPanel.ColumnCount = columnCount;
            tableLayoutPanel.RowCount = rowCount;
            tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.AutoSize));
            tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.AutoSize));
            tableLayoutPanel.Name = name;
            tableLayoutPanel.ResumeLayout(false);
            tableLayoutPanel.PerformLayout();
            return tableLayoutPanel;
        }
        public static PictureBox createLogoPictureBox(Image image, int formStartX, int formStartY, int width, int height)
        {
            PictureBox pictureBox = new PictureBox();
            pictureBox.Image = image;
            pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox.Location = new System.Drawing.Point(formStartX, formStartY);
            pictureBox.Size = new System.Drawing.Size(Constants.LOGO_WIDTH, Constants.LOGO_HEIGHT);
            return pictureBox;
        }
        public static MetroTabControl createMetroTabControl(int formStartX, int formStartY, int width, int height)
        {
            MetroTabControl metroTabControl = new MetroTabControl();
            metroTabControl.UseStyleColors = true;
            metroTabControl.Location = new System.Drawing.Point(formStartX, formStartY);
            metroTabControl.Size = new System.Drawing.Size(width, height);
            return metroTabControl;
        }
        public static MetroTabPage createMetroTabPage(String name, String text, int width, int height)
        {
            MetroTabPage metroTabPage = new MetroTabPage();
            metroTabPage.UseStyleColors = true;
            metroTabPage.Name = name;
            metroTabPage.Text = text;
            metroTabPage.Width = width;
            metroTabPage.Height = height;
            return metroTabPage;
        }
        public static FieldElement createMetroButtonFieldElement(String name, EventHandler eventHandler)
        {
            MetroButton metroButton = createMetroButton(name + "_btn",name , eventHandler);
            FieldElement fieldElement = new FieldElement(metroButton);
            return fieldElement;
        }
        public static FieldElement createMetroTextboxFieldElement(String name, int index)
        {
            MetroLabel metroLabel = createMetroLabel(name + "_lbl", name);
            MetroTextBox metroTextBox =createMetroTextBox(name+"_txt", index);
            FieldElement fieldElement = new FieldElement(metroLabel, metroTextBox);
            return fieldElement;
        }
    }
}
