﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetroFramework.Controls;
using PSMS.Entities;

namespace PSMS.MetroUI
{
    public class PageElement
    {
        public PageElement()
        {
        }
        public PageElement(MetroTabPage metroTabPage)
        {
            this.metroTabPage = metroTabPage;
        }
        public PageElement(Dictionary<FieldGroup, FieldGroupElement> fieldGroupElementMap)
        {
            this.fieldGroupElementMap = fieldGroupElementMap;
        }
        public PageElement(MetroTabPage metroTabPage, Dictionary<FieldGroup, FieldGroupElement> fieldGroupElementMap)
        {
            this.metroTabPage = metroTabPage;
            this.fieldGroupElementMap = fieldGroupElementMap;
        }
        public void addFieldGroupElement(FieldGroup fieldGroup, FieldGroupElement fieldGroupElement)
        {
            if (fieldGroupElementMap == null)
            {
                fieldGroupElementMap = new Dictionary<FieldGroup, FieldGroupElement>();
            }
            fieldGroupElementMap.Add(fieldGroup, fieldGroupElement);
        }
        public MetroTabPage metroTabPage { get; set; }
        public Dictionary<FieldGroup, FieldGroupElement> fieldGroupElementMap { get; set; }
    }
}
