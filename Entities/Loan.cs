﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Loan
    {
        public Loan() 
        {
        }
        public Loan(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public String BillNo { get; set; }
        public Customer CustomerId { get; set; }
        public DateTimeOffset OpenDate { get; set; }
        public Decimal PrincipalAmount { get; set; }
        public Decimal AdvanceAmount { get; set; }
        public Decimal NetPaidAmount { get; set; }
        public Decimal CurrentPrincipalAmount { get; set; }
        public Decimal CurrentInterestAmount { get; set; }
        public LoanState LoanStateId { get; set; }
        public DateTimeOffset LastTransaction { get; set; }
        public DateTimeOffset ExpireDate { get; set; }
        public DateTimeOffset ClosedDate { get; set; }
        public String Description { get; set; }
        public CoreUser OpenStaffId { get; set; }
        public CoreUser CloseStaffId { get; set; }
        public ICollection<Jewel> JewelIds { get; set; }
        public ICollection<Repay> RepayIds { get; set; }
    }
}
