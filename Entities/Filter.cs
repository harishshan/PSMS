﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Filter
    {
        public Filter()
        {
        }
        public Filter(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Boolean Status { get; set; }
    }
}
