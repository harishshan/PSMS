﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Authorisation
    {
        public Authorisation()
        {
        }
        public Authorisation(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public Role RoleId { get; set; }
        public Module ModuleId { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public Boolean Status { get; set; }
    }
}
