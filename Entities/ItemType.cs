﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class ItemType
    {
        public ItemType()
        {
        }
        public ItemType(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Display { get; set; }
        public String PrintText { get; set; }
        public Boolean Status { get; set; }
        public ICollection<Item> ItemIds { get; set; }
        public ICollection<ItemTypeGroupMapping> ItemTypeGroupMappingIds { get; set; }
        public ICollection<Jewel> JewelIds { get; set; }
    }
}
