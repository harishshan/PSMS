﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Field
    {
        public Field()
        {
        }
        public Field(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public FieldGroup FieldGroupId { get; set; }
        public String Name { get; set; }
        public AllowedValue AllowedValueId { get; set; }
        public FieldType FieldTypeId { get; set; }
        public String Action { get; set; }
        public Int32 Sort { get; set; }
        public Boolean Status { get; set; }
    }
}
