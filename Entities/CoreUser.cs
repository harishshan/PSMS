﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class CoreUser
    {
        public CoreUser()
        {
        }
        public CoreUser(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public Role RoleId { get; set; }
        public Company CompanyId { get; set; }
        public String Username { get; set; }
        public String Display { get; set; }
        public String PrintText { get; set; }
        public String Password { get; set; }
        public String LastPassword { get; set; }
        public Int32 PasswordFailedCount { get; set; }
        public String Firstname { get; set; }
        public String Lastname { get; set; }
        public Gender GenderId { get; set; }
        public DateTime DOB { get; set; }
        public Byte[] Photo { get; set; }
        public Byte[] Thumb { get; set; }
        public Language LanguageId { get; set; }
        public Theme ThemeId { get; set; }
        public Color ColorId { get; set; }
        public String CreatedBy { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public Boolean Status { get; set; }
        public ICollection<Loan> OpenStaffIds { get; set; }
        public ICollection<Loan> CloseStaffLoanIds { get; set; }
        public ICollection<Jewel> HoldStaffJewelIds { get; set; }
        public ICollection<Jewel> ReleaseStaffJewelIds { get; set; }
    }
}