﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Item
    {
        public Item()
        {
        }
        public Item(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public ItemType ItemTypeId { get; set; }
        public String Name { get; set; }
        public String Code { get; set; }
        public String Display { get; set; }
        public String PrintText { get; set; }
        public Boolean Status { get; set; }
        public ICollection<Jewel> JewelIds { get; set; }
    }
}
