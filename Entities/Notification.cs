﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Notification
    {
        public Notification() 
        {
        }
        public Notification(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Boolean SMS { get; set; }
        public Boolean Email { get; set; }
        public Boolean Notice { get; set; }
    }
}