﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class ItemTypeGroupMapping
    {
        public ItemTypeGroupMapping()
        {
        }
        public ItemTypeGroupMapping(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public ItemTypeGroup ItemTypeGroupId { get; set; }
        public ItemType ItemTypeId { get; set; }
        public Boolean Status { get; set; }
        public ICollection<Item> ItemIds { get; set; }
    }
}
