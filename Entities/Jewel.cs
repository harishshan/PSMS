﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Jewel
    {
        public Jewel()
        {
        }
        public Jewel(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public Loan LoanId { get; set; }
        public ItemType ItemTypeId { get; set; }
        public Item ItemId { get; set; }
        public String Description { get; set; }
        public JewelState JewelStateId { get; set; }
        public Int32 Quantity { get; set; }
        public Decimal NetWeight { get; set; }
        public Decimal GrossWeight { get; set; }
        public Decimal MarketValueAmount { get; set; }
        public Decimal LoanAmount { get; set; }
        public Decimal Purity { get; set; }
        public DateTimeOffset HoldDate { get; set; }
        public DateTimeOffset ReleaseDate { get; set; }
        public CoreUser HoldStaffId { get; set; }
        public CoreUser ReleaseStaffId { get; set; }
    }
}
