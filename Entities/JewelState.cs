﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class JewelState
    {
        public JewelState()
        {
        }
        public JewelState(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Boolean Status { get; set; }
        public ICollection<Jewel> JewelIds { get; set; }
    }
}
