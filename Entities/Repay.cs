﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Repay
    {
        public Repay() 
        {
        }
        public Repay(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public Loan LoanId { get; set; }
        public Decimal LoanAmount { get; set; }
        public Decimal LoanRepaidAmount { get; set; }
        public Decimal InterestAmount { get; set; }
        public Decimal InterestRepaidAmount { get; set; }
        public String RepaidBy { get; set; }
        public String RepayReceivedBy { get; set; }
        public DateTimeOffset RepayDate { get; set; }
    }
}
