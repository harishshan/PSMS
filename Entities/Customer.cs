﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Customer
    {
         public Customer() 
        {
        }
         public Customer(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public String FirsName { get; set; }
        public String LastName { get; set; }
        public Gender GenderId { get; set; }
        public DateTime DOB { get; set; }
        public String MobileNumber { get; set; }
        public String EmailId { get; set; }
        public Byte[] Photo { get; set; }
        public Byte[] Thumb { get; set; }
        public String Occupation { get; set; }
        public String Address { get; set; }
        public City CityId { get; set; }
        public State StateId { get; set; }
        public Country CountryId { get; set; }
        public String PINCode { get; set; }
        public ProofType ProofTypeId { get; set; }
        public String ProofId { get; set; }
        public ICollection<Loan> LoanIds { get; set; }
    }
}
