﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Theme
    {
        public Theme()
        {
        }
        public Theme(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Code { get; set; }
        public Boolean Status { get; set; }
        public ICollection<CoreUser> CoreUserIds { get; set; }
        public ICollection<Company> CompanyIds { get; set; }
    }
}
