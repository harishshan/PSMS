﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class ActivityLog
    {
        public ActivityLog()
        {
        }
        public ActivityLog(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public String Message { get; set; }
        public String CreatedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
    }
}
