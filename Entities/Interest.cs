﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Interest
    {
        public Interest()
        {
        }
        public Interest(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public ItemTypeGroup ItemTypeGroupId { get; set; }
        public InterestType InterestTypeId { get; set; }
        public Int32 MonthFrom { get; set; }
        public Int32 MonthTO { get; set; }
        public Decimal ROI { get; set; }
        public Boolean Status { get; set; }
    }
}
