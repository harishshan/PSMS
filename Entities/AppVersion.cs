﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class AppVersion
    {
        public AppVersion()
        {
        }
        public AppVersion(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Display { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
    }
}
