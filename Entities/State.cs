﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class State
    {
        public State()
        {
        }
        public State(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public Country countryId { get; set; }
        public String Name { get; set; }
        public String Code { get; set; }
        public String PrintText { get; set; }
        public Boolean Status { get; set; }
        public ICollection<City> CityIds { get; set; }
        public ICollection<Company> CompanyIds { get; set; }
        public ICollection<Customer> CustomberIds { get; set; }
    }
}
