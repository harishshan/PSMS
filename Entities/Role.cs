﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Role
    {
        public Role() 
        {
        }
        public Role(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Display { get; set; }
        public Boolean Status { get; set; }
        public ICollection<CoreUser> CoreUserIds { get; set; }
        public ICollection<Role> RoleIds { get; set; }
    }
}
