﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Language
    {
        public Language() 
        {
        }
        public Language(Int32 Id)
        {
            this.Id = Id;
        }
        public Language(int Id, string Name, string Code, bool Status)
        {
            this.Id = Id;
            this.Name = Name;
            this.Code = Code;
            this.Status = Status;
        }
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Code { get; set; }
        public Boolean Status { get; set; }
        public ICollection<CoreUser> CoreUserIds { get; set; }
        public ICollection<Company> CompanyIds { get; set; }
    }
}
