﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSMS.Entities
{
    public class Company
    {
        public Company()
        {
        }
        public Company(Int32 Id)
        {
            this.Id = Id;
        }
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Display { get; set; }
        public String PrintText { get; set; }
        public String Address { get; set; }
        public String AddressDisplay { get; set; }
        public String AddressPrint { get; set; }
        public City CityId { get; set; }
        public State StateId { get; set; }
        public Country CountryId { get; set; }
        public String PINCode { get; set; }
        public String PhoneNumber { get; set; }
        public String FaxNumber { get; set; }
        public String EmailId { get; set; }
        public String Website { get; set; }
        public Byte[] Logo { get; set; }
        public String LicenceNumber { get; set; }
        public String DBHost { get; set; }
        public String DBName { get; set; }
        public String DBUsername { get; set; }
        public String DBPassword { get; set; }
        public DBAuthenticationType DBAuthenticationTypeId { get; set; }
        public Currency CurrencyId { get; set; }
        public String LoanPrefix { get; set; }
        public String LoanPostfix { get; set; }
        public String RepayPrefix { get; set; }
        public String RepayPostfix { get; set; }
        public Byte[] ReceiptHeader { get; set; }
        public Byte[] ReceiptFooter { get; set; }
        public Byte[] Signature { get; set; }
        public String CloudBackupLocation { get; set; }
        public String CloudUsername { get; set; }
        public String CloudPassword { get; set; }
        public String LocalBackupLocation { get; set; }
        public Int32 MatureMonths { get; set; }
        public Int32 GraceDays { get; set; }
        public Boolean AlertJob { get; set; }
        public Boolean BackupJob { get; set; }
        public Language LanguageId { get; set; }
        public Theme ThemeId { get; set; }
        public Color ColorId { get; set; }
        public String CreatedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public Boolean Status { get; set; }
        public ICollection<CoreUser> CoreUserIds { get; set; }
    }
}