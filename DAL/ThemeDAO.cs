﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using System.Data.SqlClient;
using System.Data;
using PSMS.Entities;

namespace PSMS.DAL
{
    public class ThemeDAO
    {
        private ILogService log = new LogService(typeof(ThemeDAO));

        public ThemeDAO()
        {
            log.Info("Entering into ThemeDAO Default Constructor");
        }

        public ICollection<Theme> getAllThemes(SqlConnection connection)
        {
            log.Info("Entering into ThemeDAO getAllThemes Method");
            ICollection<Theme> themes = new List<Theme>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Code, Status FROM Theme", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Theme theme = new Theme();
                            theme.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            theme.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            theme.Code = reader.IsDBNull(2) ? null : reader.GetString(2);
                            theme.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            themes.Add(theme);
                        }
                    }
	            }
                return themes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Theme getThemeById(SqlConnection connection, Int32 themeId)
        {
            log.Info("Entering into ThemeDAO getThemeById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Code, Status FROM Theme WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = themeId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Theme theme = new Theme();
                            theme.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            theme.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            theme.Code = reader.IsDBNull(2) ? null : reader.GetString(2);
                            theme.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            return theme;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}