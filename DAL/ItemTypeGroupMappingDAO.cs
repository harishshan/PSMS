﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;

namespace PSMS.DAL
{
    public class ItemTypeGroupMappingDAO
    {
        private ILogService log = new LogService(typeof(ItemTypeGroupMappingDAO));

        public ItemTypeGroupMappingDAO()
        {
            log.Info("Entering into ItemTypeGroupMappingDAO Default Constructor");
        }

        public ICollection<ItemTypeGroupMapping> getAllItemTypeGroupMappings(SqlConnection connection)
        {
            log.Info("Entering into ItemTypeGroupMappingDAO getAllItemTypeGroupMappings Method");
            ICollection<ItemTypeGroupMapping> itemTypeGroupMappings = new List<ItemTypeGroupMapping>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, ItemTypeGroupId, ItemTypeId, Status FROM ItemTypeGroupMapping", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemTypeGroupMapping itemTypeGroupMapping = new ItemTypeGroupMapping();
                            itemTypeGroupMapping.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            itemTypeGroupMapping.ItemTypeGroupId = new ItemTypeGroup(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            itemTypeGroupMapping.ItemTypeId = new ItemType(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            itemTypeGroupMapping.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            itemTypeGroupMappings.Add(itemTypeGroupMapping);
                        }
                    }
	            }
                return itemTypeGroupMappings;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ItemTypeGroupMapping getItemTypeGroupMappingById(SqlConnection connection, Int32 itemTypeGroupMappingId)
        {
            log.Info("Entering into ItemTypeGroupMappingDAO getItemTypeGroupMappingById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, ItemTypeGroupId, ItemTypeId, Status FROM ItemTypeGroupMapping WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = itemTypeGroupMappingId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            ItemTypeGroupMapping itemTypeGroupMapping = new ItemTypeGroupMapping();
                            itemTypeGroupMapping.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            itemTypeGroupMapping.ItemTypeGroupId = new ItemTypeGroup(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            itemTypeGroupMapping.ItemTypeId = new ItemType(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            itemTypeGroupMapping.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            return itemTypeGroupMapping;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
