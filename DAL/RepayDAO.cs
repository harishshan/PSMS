﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data;
using System.Data.SqlClient;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class RepayDAO
    {
        private ILogService log = new LogService(typeof(RepayDAO));

        public RepayDAO()
        {
            log.Info("Entering into RepayDAO Default Constructor");
        }

        public ICollection<Repay> getAllRepays(SqlConnection connection)
        {
            log.Info("Entering into RepayDAO getAllRepays Method");
            ICollection<Repay> repays = new List<Repay>();            
            try
            {
                StringBuilder queryBuilder = new StringBuilder("SELECT Id, LoanId, LoanAmount, LoanRepaidAmount, InterestAmount, InterestRepaidAmount, ")
                .Append("RepaidBy, RepayReceivedBy, RepayDate FORM Repay");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Repay repay = new Repay();
                            repay.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            repay.LoanId = new Loan(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            repay.LoanAmount = reader.IsDBNull(2) ? 0 : reader.GetDecimal(2);
                            repay.LoanRepaidAmount = reader.IsDBNull(3) ? 0 : reader.GetDecimal(3);
                            repay.InterestAmount = reader.IsDBNull(4) ? 0 : reader.GetDecimal(4);
                            repay.InterestRepaidAmount = reader.IsDBNull(5) ? 0 : reader.GetDecimal(5);
                            repay.RepaidBy = reader.IsDBNull(6) ? null : reader.GetString(6);
                            repay.RepayReceivedBy = reader.IsDBNull(7) ? null : reader.GetString(7);
                            repay.RepayDate = reader.IsDBNull(8) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(8);
                            repays.Add(repay);
                        }
                    }
	            }
                return repays;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Repay getRepayById(SqlConnection connection, Int32 repayId)
        {
            log.Info("Entering into RepayDAO getRepayById Method");
            try
            {
                StringBuilder queryBuilder = new StringBuilder("SELECT Id, LoanId, LoanAmount, LoanRepaidAmount, InterestAmount, InterestRepaidAmount, ")
                .Append("RepaidBy, RepayReceivedBy, RepayDate FORM Repay WHERE Id =@Id");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = repayId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Repay repay = new Repay();
                            repay.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            repay.LoanId = new Loan(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            repay.LoanAmount = reader.IsDBNull(2) ? 0 : reader.GetDecimal(2);
                            repay.LoanRepaidAmount = reader.IsDBNull(3) ? 0 : reader.GetDecimal(3);
                            repay.InterestAmount = reader.IsDBNull(4) ? 0 : reader.GetDecimal(4);
                            repay.InterestRepaidAmount = reader.IsDBNull(5) ? 0 : reader.GetDecimal(5);
                            repay.RepaidBy = reader.IsDBNull(6) ? null : reader.GetString(6);
                            repay.RepayReceivedBy = reader.IsDBNull(7) ? null : reader.GetString(7);
                            repay.RepayDate = reader.IsDBNull(8) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(8);
                            return repay;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
