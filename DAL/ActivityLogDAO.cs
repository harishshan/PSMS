﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;

namespace PSMS.DAL
{
    public class ActivityLogDAO
    {
        private ILogService log = new LogService(typeof(ActivityLogDAO));

        public ActivityLogDAO()
        {
            log.Info("Entering into ActivityLogDAO Default Constructor");
        }

        public ICollection<ActivityLog> getAllActivityLogs(SqlConnection connection)
        {
            log.Info("Entering into ActivityLogDAO getAllActivityLogs Method");
            ICollection<ActivityLog> activityLogs = new List<ActivityLog>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Mesage, CreatedBy, CreatedDate FROM ActivityLog", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ActivityLog activityLog = new ActivityLog();
                            activityLog.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            activityLog.Message = reader.IsDBNull(1) ? null : reader.GetString(1);
                            activityLog.CreatedBy = reader.IsDBNull(2) ? null : reader.GetString(2);
                            activityLog.CreatedDate = reader.IsDBNull(3) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(3);
                            activityLogs.Add(activityLog);
                        }
                    }
	            }
                return activityLogs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActivityLog getActivityLogById(SqlConnection connection, Int32 activityLogId)
        {
            log.Info("Entering into ActivityLogDAO getActivityLogById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Mesage, CreatedBy, CreatedDate FROM ActivityLog WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = activityLogId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            ActivityLog activityLog = new ActivityLog();
                            activityLog.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            activityLog.Message = reader.IsDBNull(1) ? null : reader.GetString(1);
                            activityLog.CreatedBy = reader.IsDBNull(2) ? null : reader.GetString(2);
                            activityLog.CreatedDate = reader.IsDBNull(3) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(3);
                            return activityLog;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
