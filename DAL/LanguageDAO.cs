﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;


namespace PSMS.DAL
{
    public class LanguageDAO
    {
        private ILogService log = new LogService(typeof(LanguageDAO));

        public LanguageDAO()
        {
            log.Info("Entering into LanguageDAO Default Constructor");
        }

        public ICollection<Language> getAllLanguages(SqlConnection connection)
        {
            log.Info("Entering into LanguageDAO getAllLanguages Method");
            ICollection<Language> languages = new List<Language>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Code, Status FROM Language", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Language language = new Language();
                            language.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            language.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            language.Code = reader.IsDBNull(2) ? null : reader.GetString(2);
                            language.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            languages.Add(language);
                        }
                    }
	            }
                return languages;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Language getLanguageById(SqlConnection connection, Int32 languageId)
        {
            log.Info("Entering into LanguageDAO getLanguageById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Code, Status FROM Language WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = languageId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Language language = new Language();
                            language.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            language.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            language.Code = reader.IsDBNull(2) ? null : reader.GetString(2);
                            language.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            return language;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}