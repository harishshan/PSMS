﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class JewelStateDAO
    {
        private ILogService log = new LogService(typeof(JewelStateDAO));

        public JewelStateDAO()
        {
            log.Info("Entering into JewelStateDAO Default Constructor");
        }

        public ICollection<JewelState> getAllJewelStates(SqlConnection connection)
        {
            log.Info("Entering into JewelStateDAO getAllJewelStates Method");
            ICollection<JewelState> jewelStates = new List<JewelState>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM JewelState", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            JewelState jewelState = new JewelState();
                            jewelState.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            jewelState.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            jewelState.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            jewelStates.Add(jewelState);
                        }
                    }
	            }
                return jewelStates;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JewelState getJewelStateById(SqlConnection connection, Int32 jewelStateId)
        {
            log.Info("Entering into JewelStateDAO getJewelStateById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM JewelState WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = jewelStateId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            JewelState jewelState = new JewelState();
                            jewelState.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            jewelState.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            jewelState.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            return jewelState;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
