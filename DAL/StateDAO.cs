﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Data;

namespace PSMS.DAL
{
    public class StateDAO
    {
        private ILogService log = new LogService(typeof(StateDAO));

        public StateDAO()
        {
            log.Info("Entering into StateDAO Default Constructor");
        }

        public ICollection<State> getAllStates(SqlConnection connection)
        {
            log.Info("Entering into StateDAO getAllStates Method");
            ICollection<State> states = new List<State>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, CountryId, Name, Code, PrintText, Status FROM State", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            State state = new State();
                            state.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            state.countryId = new Country(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            state.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            state.Code = reader.IsDBNull(3) ? null : reader.GetString(3);
                            state.PrintText = reader.IsDBNull(4) ? null : reader.GetString(4);
                            state.Status = reader.IsDBNull(5) ? false : reader.GetBoolean(5);
                            states.Add(state);
                        }
                    }
	            }
                return states;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public State getStateById(SqlConnection connection, Int32 stateId)
        {
            log.Info("Entering into StateDAO getStateById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, CountryId, Name, Code, PrintText, Status FROM State WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = stateId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            State state = new State();
                            state.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            state.countryId = new Country(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            state.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            state.Code = reader.IsDBNull(3) ? null : reader.GetString(3);
                            state.PrintText = reader.IsDBNull(4) ? null : reader.GetString(4);
                            state.Status = reader.IsDBNull(5) ? false : reader.GetBoolean(5);
                            return state;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}