﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using System.Data.SqlClient;
using System.Data;
using PSMS.Entities;

namespace PSMS.DAL
{
    public class AppVersionDAO
    {
        private ILogService log = new LogService(typeof(AppVersionDAO));

        public AppVersionDAO()
        {
            log.Info("Entering into AppVersionDAO Default Constructor");
        }

        public ICollection<AppVersion> getAllAppVersions(SqlConnection connection)
        {
            log.Info("Entering into AppVersionDAO getAllVersions Method");
            ICollection<AppVersion> appVersions = new List<AppVersion>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Display, CreatedDate FROM AppVersion", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AppVersion appVersion = new AppVersion();
                            appVersion.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            appVersion.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            appVersion.Display = reader.IsDBNull(2) ? null : reader.GetString(2);
                            appVersion.CreatedDate = reader.IsDBNull(3) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(3);
                            appVersions.Add(appVersion);
                        }
                    }
	            }
                return appVersions;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AppVersion getAppVersionById(SqlConnection connection, Int32 appVersionId)
        {
            log.Info("Entering into VersionDAO getVersionById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Display, CreatedDate FROM AppVersion WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = appVersionId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            AppVersion appVersion = new AppVersion();
                            appVersion.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            appVersion.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            appVersion.Display = reader.IsDBNull(2) ? null : reader.GetString(2);
                            appVersion.CreatedDate = reader.IsDBNull(3) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(3);
                            return appVersion;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
