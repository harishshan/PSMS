﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;

namespace PSMS.DAL
{
    public class InterestTypeDAO
    {
        private ILogService log = new LogService(typeof(InterestTypeDAO));

        public InterestTypeDAO()
        {
            log.Info("Entering into InterestTypeDAO Default Constructor");
        }

        public ICollection<InterestType> getAllInterestTypes(SqlConnection connection)
        {
            log.Info("Entering into InterestTypeDAO getAllInterestTypes Method");
            ICollection<InterestType> interestTypes = new List<InterestType>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM InterestType", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            InterestType interestType = new InterestType();
                            interestType.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            interestType.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            interestType.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            interestTypes.Add(interestType);
                        }
                    }
	            }
                return interestTypes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InterestType getInterestTypeById(SqlConnection connection, Int32 interestTypeId)
        {
            log.Info("Entering into InterestTypeDAO getInterestTypeById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM InterestType WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = interestTypeId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            InterestType interestType = new InterestType();
                            interestType.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            interestType.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            interestType.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            return interestType;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
