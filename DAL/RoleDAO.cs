﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class RoleDAO
    {
        private ILogService log = new LogService(typeof(RoleDAO));

        public RoleDAO()
        {
            log.Info("Entering into RoleDAO Default Constructor");
        }

        public ICollection<Role> getAllRoles(SqlConnection connection)
        {
            log.Info("Entering into RoleDAO getAllRoles Method");
            ICollection<Role> roles = new List<Role>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Display, Status FROM Role", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Role role = new Role();
                            role.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            role.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            role.Display = reader.IsDBNull(2) ? null : reader.GetString(2);
                            role.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            roles.Add(role);
                        }
                    }
	            }
                return roles;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Role getRoleById(SqlConnection connection, Int32 roleId)
        {
            log.Info("Entering into RoleDAO getRoleById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Display, Status FROM Role WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = roleId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Role role = new Role();
                            role.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            role.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            role.Display = reader.IsDBNull(2) ? null : reader.GetString(2);
                            role.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            return role;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
