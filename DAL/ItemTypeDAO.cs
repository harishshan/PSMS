﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;

namespace PSMS.DAL
{
    public class ItemTypeDAO
    {
        private ILogService log = new LogService(typeof(ItemTypeDAO));

        public ItemTypeDAO()
        {
            log.Info("Entering into ItemTypeDAO Default Constructor");
        }

        public ICollection<ItemType> getAllItemTypes(SqlConnection connection)
        {
            log.Info("Entering into ItemTypeDAO getAllItemTypes Method");
            ICollection<ItemType> itemTypes = new List<ItemType>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Display, PrintText, Status FROM ItemType", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemType itemType = new ItemType();
                            itemType.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            itemType.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            itemType.Display = reader.IsDBNull(2) ? null : reader.GetString(2);
                            itemType.PrintText = reader.IsDBNull(3) ? null : reader.GetString(3);
                            itemType.Status = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                            itemTypes.Add(itemType);
                        }
                    }
	            }
                return itemTypes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ItemType getItemTypeById(SqlConnection connection, Int32 itemTypeId)
        {
            log.Info("Entering into ItemTypeDAO getItemTypeById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Display, PrintText, Status FROM ItemType WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = itemTypeId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            ItemType itemType = new ItemType();
                            itemType.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            itemType.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            itemType.Display = reader.IsDBNull(2) ? null : reader.GetString(2);
                            itemType.PrintText = reader.IsDBNull(3) ? null : reader.GetString(3);
                            itemType.Status = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                            return itemType;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
