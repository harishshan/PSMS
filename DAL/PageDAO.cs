﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class PageDAO
    {
        private ILogService log = new LogService(typeof(PageDAO));

        public PageDAO()
        {
            log.Info("Entering into PageDAO Default Constructor");
        }

        public ICollection<Page> getAllPages(SqlConnection connection)
        {
            log.Info("Entering into PageDAO getAllPages Method");
            ICollection<Page> pages = new List<Page>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Sort, Status FROM Page", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Page page = new Page();
                            page.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            page.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            page.Sort = reader.IsDBNull(2) ? -1 : reader.GetInt32(2);
                            page.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            pages.Add(page);
                        }
                    }
	            }
                return pages;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageDAO getAllPages Method");
            }
        }
        public ICollection<Page> getAllPagesSorted(SqlConnection connection)
        {
            log.Info("Entering into PageDAO getAllPagesSorted Method");
            ICollection<Page> pages = new List<Page>();
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Sort, Status FROM Page ORDER BY Sort", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Page page = new Page();
                            page.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            page.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            page.Sort = reader.IsDBNull(2) ? -1 : reader.GetInt32(2);
                            page.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            pages.Add(page);
                        }
                    }
                }
                return pages;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageDAO getAllPagesSorted Method");
            }
        }

        public Page getPageById(SqlConnection connection, Int32 pageId)
        {
            log.Info("Entering into PageDAO getPageById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Sort, Status FROM Page WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = pageId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Page page = new Page();
                            page.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            page.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            page.Sort = reader.IsDBNull(2) ? -1 : reader.GetInt32(2);
                            page.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            return page;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from PageDAO getPageById Method");
            }
        }
    }
}
