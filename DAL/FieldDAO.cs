﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class FieldDAO
    {
        private ILogService log = new LogService(typeof(FieldDAO));

        public FieldDAO()
        {
            log.Info("Entering into FieldDAO Default Constructor");
        }

        public ICollection<Field> getAllFields(SqlConnection connection)
        {
            log.Info("Entering into FieldDAO getAllFields Method");
            ICollection<Field> fields = new List<Field>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, FieldGroupId, Name, AllowedValueId, FieldTypeId, Action, Sort, Status FROM Field", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Field field = new Field();
                            field.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            field.FieldGroupId = new FieldGroup(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            field.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            field.AllowedValueId = new AllowedValue(reader.IsDBNull(3) ? -1 : reader.GetInt32(3));
                            field.FieldTypeId = new FieldType(reader.IsDBNull(4) ? -1 : reader.GetInt32(4));
                            field.Action = reader.IsDBNull(5) ? null : reader.GetString(5);
                            field.Sort = reader.IsDBNull(6) ? -1 : reader.GetInt32(6);
                            field.Status = reader.IsDBNull(7) ? false : reader.GetBoolean(7);
                            fields.Add(field);
                        }
                    }
	            }
                return fields;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldDAO getAllFields Method");
            }
        }

        public Field getFieldById(SqlConnection connection, Int32 fieldId)
        {
            log.Info("Entering into FieldDAO getFieldById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, FieldGroupId, Name, AllowedValueId, FieldTypeId, Action, Sort, Status FROM Field WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = fieldId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Field field = new Field();
                            field.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            field.FieldGroupId = new FieldGroup(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            field.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            field.AllowedValueId = new AllowedValue(reader.IsDBNull(3) ? -1 : reader.GetInt32(3));
                            field.FieldTypeId = new FieldType(reader.IsDBNull(4) ? -1 : reader.GetInt32(4));
                            field.Action = reader.IsDBNull(5) ? null : reader.GetString(5);
                            field.Sort = reader.IsDBNull(6) ? -1 : reader.GetInt32(6);
                            field.Status = reader.IsDBNull(7) ? false : reader.GetBoolean(7);
                            return field;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldDAO getFieldById Method");
            }
        }
        public ICollection<Field> getFieldsByFieldGroupId(SqlConnection connection, Int32 fieldGroupId)
        {
            log.Info("Entering into FieldDAO getFieldsByFieldGroupId Method");
            ICollection<Field> fields = new List<Field>(); 
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, FieldGroupId, Name, AllowedValueId, FieldTypeId, Action, Sort, Status FROM Field WHERE FieldGroupId = @FieldGroupId", connection))
                {
                    SqlParameter idParam = new SqlParameter("@FieldGroupId", SqlDbType.Int);
                    idParam.Value = fieldGroupId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Field field = new Field();
                            field.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            field.FieldGroupId = new FieldGroup(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            field.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            field.AllowedValueId = new AllowedValue(reader.IsDBNull(3) ? -1 : reader.GetInt32(3));
                            field.FieldTypeId = new FieldType(reader.IsDBNull(4) ? -1 : reader.GetInt32(4));
                            field.Action = reader.IsDBNull(5) ? null : reader.GetString(5);
                            field.Sort = reader.IsDBNull(6) ? -1 : reader.GetInt32(6);
                            field.Status = reader.IsDBNull(7) ? false : reader.GetBoolean(7);
                            fields.Add(field);
                        }
                    }
                }
                return fields;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldDAO getFieldsByFieldGroupId Method");
            }
        }
        public ICollection<Field> getFieldsSortedByFieldGroupId(SqlConnection connection, Int32 fieldGroupId)
        {
            log.Info("Entering into FieldDAO getFieldsSortedByFieldGroupId Method");
            ICollection<Field> fields = new List<Field>();
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, FieldGroupId, Name, AllowedValueId, FieldTypeId, Action, Sort, Status FROM Field WHERE FieldGroupId = @FieldGroupId ORDER BY Sort", connection))
                {
                    SqlParameter idParam = new SqlParameter("@FieldGroupId", SqlDbType.Int);
                    idParam.Value = fieldGroupId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Field field = new Field();
                            field.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            field.FieldGroupId = new FieldGroup(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            field.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            field.AllowedValueId = new AllowedValue(reader.IsDBNull(3) ? -1 : reader.GetInt32(3));
                            field.FieldTypeId = new FieldType(reader.IsDBNull(4) ? -1 : reader.GetInt32(4));
                            field.Action = reader.IsDBNull(5) ? null : reader.GetString(5);
                            field.Sort = reader.IsDBNull(6) ? -1 : reader.GetInt32(6);
                            field.Status = reader.IsDBNull(7) ? false : reader.GetBoolean(7);
                            fields.Add(field);
                        }
                    }
                }
                return fields;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldDAO getFieldsSortedByFieldGroupId Method");
            }
        }
    }
}
