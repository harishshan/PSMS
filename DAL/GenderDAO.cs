﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;

namespace PSMS.DAL
{
    public class GenderDAO
    {
        private ILogService log = new LogService(typeof(GenderDAO));

        public GenderDAO()
        {
            log.Info("Entering into GenderDAO Default Constructor");
        }

        public ICollection<Gender> getAllGenders(SqlConnection connection)
        {
            log.Info("Entering into GenderDAO getAllGenders Method");
            ICollection<Gender> genders = new List<Gender>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM Gender", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Gender gender = new Gender();
                            gender.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            gender.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            gender.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            genders.Add(gender);
                        }
                    }
	            }
                return genders;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Gender getGenderById(SqlConnection connection, Int32 genderId)
        {
            log.Info("Entering into GenderDAO getGenderById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM Gender WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = genderId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Gender gender = new Gender();
                            gender.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            gender.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            gender.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            return gender;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
