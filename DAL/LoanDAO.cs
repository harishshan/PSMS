﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class LoanDAO
    {
        private ILogService log = new LogService(typeof(LoanDAO));

        public LoanDAO()
        {
            log.Info("Entering into LoanDAO Default Constructor");
        }

        public ICollection<Loan> getAllLoans(SqlConnection connection)
        {
            log.Info("Entering into LoanDAO getAllLoans Method");
            ICollection<Loan> loans = new List<Loan>();            
            try
            {
                StringBuilder queryBuilder = new StringBuilder("Select Id, BillNo, CustomerId, OpenDate, PrincipalAmount, AdvanceAmount, NetPaidAmount, ")
                .Append("CurrentPrincipalAmount, CurrentInterestAmount, LoanStateId, LastTransaction, ExpireDate, ClosedDate, Description, OpenStaffId, ")
                .Append("CloseStaffId FROM Loan");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Loan loan = new Loan();
                            loan.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            loan.BillNo = reader.IsDBNull(1) ? null : reader.GetString(1);
                            loan.CustomerId = new Customer(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            loan.OpenDate = reader.IsDBNull(3) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(3);
                            loan.PrincipalAmount = reader.IsDBNull(4) ? 0 : reader.GetDecimal(4);
                            loan.AdvanceAmount = reader.IsDBNull(5) ? 0 : reader.GetDecimal(5);
                            loan.NetPaidAmount = reader.IsDBNull(6) ? 0 : reader.GetDecimal(6);
                            loan.CurrentPrincipalAmount = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                            loan.CurrentInterestAmount = reader.IsDBNull(8) ? 0 : reader.GetDecimal(8);
                            loan.LoanStateId = new LoanState(reader.IsDBNull(9) ? -1 : reader.GetInt32(9));
                            loan.LastTransaction = reader.IsDBNull(10) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(10);
                            loan.ExpireDate = reader.IsDBNull(11) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(11);
                            loan.ClosedDate = reader.IsDBNull(12) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(12);                           
                            loan.Description = reader.IsDBNull(13) ? null : reader.GetString(13);
                            loan.OpenStaffId = new CoreUser(reader.IsDBNull(14) ? -1 : reader.GetInt32(14));
                            loan.CloseStaffId = new CoreUser(reader.IsDBNull(15) ? -1 : reader.GetInt32(15));
                            loans.Add(loan);
                        }
                    }
	            }
                return loans;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Loan getLoanById(SqlConnection connection, Int32 loanId)
        {
            log.Info("Entering into LoanDAO getLoanById Method");
            try
            {
                StringBuilder queryBuilder = new StringBuilder("Select Id, BillNo, CustomerId, OpenDate, PrincipalAmount, AdvanceAmount, NetPaidAmount, ")
                .Append("CurrentPrincipalAmount, CurrentInterestAmount, LoanStateId, LastTransaction, ExpireDate, ClosedDate, Description, OpenStaffId, ")
                .Append("CloseStaffId FROM Loan WHERE Id = @Id");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = loanId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Loan loan = new Loan();
                            loan.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            loan.BillNo = reader.IsDBNull(1) ? null : reader.GetString(1);
                            loan.CustomerId = new Customer(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            loan.OpenDate = reader.IsDBNull(3) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(3);
                            loan.PrincipalAmount = reader.IsDBNull(4) ? 0 : reader.GetDecimal(4);
                            loan.AdvanceAmount = reader.IsDBNull(5) ? 0 : reader.GetDecimal(5);
                            loan.NetPaidAmount = reader.IsDBNull(6) ? 0 : reader.GetDecimal(6);
                            loan.CurrentPrincipalAmount = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                            loan.CurrentInterestAmount = reader.IsDBNull(8) ? 0 : reader.GetDecimal(8);
                            loan.LoanStateId = new LoanState(reader.IsDBNull(9) ? -1 : reader.GetInt32(9));
                            loan.LastTransaction = reader.IsDBNull(10) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(10);
                            loan.ExpireDate = reader.IsDBNull(11) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(11);
                            loan.ClosedDate = reader.IsDBNull(12) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(12);
                            loan.Description = reader.IsDBNull(13) ? null : reader.GetString(13);
                            loan.OpenStaffId = new CoreUser(reader.IsDBNull(14) ? -1 : reader.GetInt32(14));
                            loan.CloseStaffId = new CoreUser(reader.IsDBNull(15) ? -1 : reader.GetInt32(15));
                            return loan;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
