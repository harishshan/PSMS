﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;

namespace PSMS.DAL
{
    public class ItemTypeGroupDAO
    {
        private ILogService log = new LogService(typeof(ItemTypeGroupDAO));

        public ItemTypeGroupDAO()
        {
            log.Info("Entering into ItemTypeGroupDAO Default Constructor");
        }

        public ICollection<ItemTypeGroup> getAllItemTypeGroups(SqlConnection connection)
        {
            log.Info("Entering into ItemTypeGroupDAO getAllItemTypeGroups Method");
            ICollection<ItemTypeGroup> itemTypeGroups = new List<ItemTypeGroup>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Display, Status FROM ItemTypeGroup", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemTypeGroup itemTypeGroup = new ItemTypeGroup();
                            itemTypeGroup.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            itemTypeGroup.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            itemTypeGroup.Display = reader.IsDBNull(2) ? null : reader.GetString(2);
                            itemTypeGroup.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            itemTypeGroups.Add(itemTypeGroup);
                        }
                    }
	            }
                return itemTypeGroups;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ItemTypeGroup getItemTypeGroupById(SqlConnection connection, Int32 itemTypeGroupId)
        {
            log.Info("Entering into ItemTypeGroupDAO getItemTypeGroupById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Display, Status FROM ItemTypeGroup WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = itemTypeGroupId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            ItemTypeGroup itemTypeGroup = new ItemTypeGroup();
                            itemTypeGroup.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            itemTypeGroup.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            itemTypeGroup.Display = reader.IsDBNull(2) ? null : reader.GetString(2);
                            itemTypeGroup.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            return itemTypeGroup;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
