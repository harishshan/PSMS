﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class AllowedValueDAO
    {
        private ILogService log = new LogService(typeof(AllowedValueDAO));

        public AllowedValueDAO()
        {
            log.Info("Entering into AllowedValueDAO Default Constructor");
        }

        public ICollection<AllowedValue> getAllAllowedValues(SqlConnection connection)
        {
            log.Info("Entering into AllowedValueDAO getAllAllowedValues Method");
            ICollection<AllowedValue> allowedValues = new List<AllowedValue>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM AllowedValue", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AllowedValue allowedValue = new AllowedValue();
                            allowedValue.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            allowedValue.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            allowedValue.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            allowedValues.Add(allowedValue);
                        }
                    }
	            }
                return allowedValues;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AllowedValue getAllowedValueById(SqlConnection connection, Int32 allowedValueId)
        {
            log.Info("Entering into AllowedValueDAO getAllowedValueById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM AllowedValue WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = allowedValueId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            AllowedValue allowedValue = new AllowedValue();
                            allowedValue.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            allowedValue.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            allowedValue.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            return allowedValue;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
