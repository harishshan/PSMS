﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Data;

namespace PSMS.DAL
{
    public class CustomerDAO
    {
        private ILogService log = new LogService(typeof(CustomerDAO));

        public CustomerDAO()
        {
            log.Info("Entering into CustomerDAO Default Constructor");
        }

        public ICollection<Customer> getAllCustomers(SqlConnection connection)
        {
            log.Info("Entering into CustomerDAO getAllCustomers Method");
            ICollection<Customer> customers = new List<Customer>();            
            try
            {
                StringBuilder queryBuilder = new StringBuilder("Select Id, FirstName, LastName, GenderId, DOB, MobileNumber, EmailId, Photo, Thumb, ")
                .Append("Occupation, Address, CityId, StateId, CountryId, PINCode, ProofTypeId, ProofId FROM Customer");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Customer customer = new Customer();
                            customer.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            customer.FirsName = reader.IsDBNull(1) ? null : reader.GetString(1);
                            customer.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                            customer.GenderId = new Gender(reader.IsDBNull(3) ? -1 : reader.GetInt32(3));
                            customer.DOB = reader.IsDBNull(4) ? DateTime.MinValue : reader.GetDateTime(4);
                            customer.MobileNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                            customer.EmailId = reader.IsDBNull(6) ? null : reader.GetString(6);
                            customer.Photo = reader.IsDBNull(7) ? null : reader.GetSqlBinary(7).Value;
                            customer.Thumb = reader.IsDBNull(8) ? null : reader.GetSqlBinary(8).Value;
                            customer.Occupation = reader.IsDBNull(9) ? null : reader.GetString(9);
                            customer.Address = reader.IsDBNull(10) ? null : reader.GetString(10);
                            customer.CityId = new City(reader.IsDBNull(11) ? -1 : reader.GetInt32(11));
                            customer.StateId = new State(reader.IsDBNull(12) ? -1 : reader.GetInt32(12));
                            customer.CountryId = new Country(reader.IsDBNull(13) ? -1 : reader.GetInt32(13));
                            customer.PINCode = reader.IsDBNull(14) ? null : reader.GetString(14);
                            customer.ProofTypeId = new ProofType(reader.IsDBNull(15) ? -1 : reader.GetInt32(15));
                            customer.ProofId = reader.IsDBNull(16) ? null : reader.GetString(15);
                            customers.Add(customer);
                        }
                    }
	            }
                return customers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Customer getCustomerById(SqlConnection connection, Int32 customerId)
        {
            log.Info("Entering into CustomerDAO getCustomerById Method");
            try
            {
                StringBuilder queryBuilder = new StringBuilder("Select Id, FirstName, LastName, GenderId, DOB, MobileNumber, EmailId, Photo, Thumb, ")
                .Append("Occupation, Address, CityId, StateId, CountryId, PINCode, ProofTypeId, ProofId FROM Customer WHERE Id = @Id");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = customerId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Customer customer = new Customer();
                            customer.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            customer.FirsName = reader.IsDBNull(1) ? null : reader.GetString(1);
                            customer.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                            customer.GenderId = new Gender(reader.IsDBNull(3) ? -1 : reader.GetInt32(3));
                            customer.DOB = reader.IsDBNull(4) ? DateTime.MinValue : reader.GetDateTime(4);
                            customer.MobileNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                            customer.EmailId = reader.IsDBNull(6) ? null : reader.GetString(6);
                            customer.Photo = reader.IsDBNull(7) ? null : reader.GetSqlBinary(7).Value;
                            customer.Thumb = reader.IsDBNull(8) ? null : reader.GetSqlBinary(8).Value;
                            customer.Occupation = reader.IsDBNull(9) ? null : reader.GetString(9);
                            customer.Address = reader.IsDBNull(10) ? null : reader.GetString(10);
                            customer.CityId = new City(reader.IsDBNull(11) ? -1 : reader.GetInt32(11));
                            customer.StateId = new State(reader.IsDBNull(12) ? -1 : reader.GetInt32(12));
                            customer.CountryId = new Country(reader.IsDBNull(13) ? -1 : reader.GetInt32(13));
                            customer.PINCode = reader.IsDBNull(14) ? null : reader.GetString(14);
                            customer.ProofTypeId = new ProofType(reader.IsDBNull(15) ? -1 : reader.GetInt32(15));
                            customer.ProofId = reader.IsDBNull(16) ? null : reader.GetString(15);
                            return customer;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
