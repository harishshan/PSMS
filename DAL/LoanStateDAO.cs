﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class LoanStateDAO
    {
        private ILogService log = new LogService(typeof(LoanStateDAO));

        public LoanStateDAO()
        {
            log.Info("Entering into LoanStateDAO Default Constructor");
        }

        public ICollection<LoanState> getAllLoanStates(SqlConnection connection)
        {
            log.Info("Entering into LoanStateDAO getAllLoanStates Method");
            ICollection<LoanState> loanStates = new List<LoanState>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM LoanState", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LoanState loanState = new LoanState();
                            loanState.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            loanState.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            loanState.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            loanStates.Add(loanState);
                        }
                    }
	            }
                return loanStates;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public LoanState getLoanStateById(SqlConnection connection, Int32 loanStateId)
        {
            log.Info("Entering into LoanStateDAO getLoanStateById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM LoanState WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = loanStateId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            LoanState loanState = new LoanState();
                            loanState.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            loanState.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            loanState.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            return loanState;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
