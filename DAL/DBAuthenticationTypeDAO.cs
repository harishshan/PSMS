﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class DBAuthenticationTypeDAO
    {
        private ILogService log = new LogService(typeof(DBAuthenticationTypeDAO));

        public DBAuthenticationTypeDAO()
        {
            log.Info("Entering into DBAuthenticationTypeDAO Default Constructor");
        }

        public ICollection<DBAuthenticationType> getAllDBAuthenticationTypes(SqlConnection connection)
        {
            log.Info("Entering into DBAuthenticationTypeDAO getAllDBAuthenticationTypes Method");
            ICollection<DBAuthenticationType> dbAuthenticationTypes = new List<DBAuthenticationType>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM DBAuthenticationType", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DBAuthenticationType dbAuthenticationType = new DBAuthenticationType();
                            dbAuthenticationType.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            dbAuthenticationType.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            dbAuthenticationType.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            dbAuthenticationTypes.Add(dbAuthenticationType);
                        }
                    }
	            }
                return dbAuthenticationTypes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DBAuthenticationType getDBAuthenticationTypeById(SqlConnection connection, Int32 dbAuthenticationTypeId)
        {
            log.Info("Entering into DBAuthenticationTypeDAO getDBAuthenticationTypeById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM DBAuthenticationType WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = dbAuthenticationTypeId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            DBAuthenticationType dbAuthenticationType = new DBAuthenticationType();
                            dbAuthenticationType.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            dbAuthenticationType.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            dbAuthenticationType.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            return dbAuthenticationType;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
