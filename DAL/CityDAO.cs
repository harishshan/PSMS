﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Data;

namespace PSMS.DAL
{
    public class CityDAO
    {
        private ILogService log = new LogService(typeof(CityDAO));

        public CityDAO()
        {
            log.Info("Entering into CityDAO Default Constructor");
        }

        public ICollection<City> getAllCities(SqlConnection connection)
        {
            log.Info("Entering into CityDAO getAllCities Method");
            ICollection<City> cities = new List<City>(); 
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, StateId, Name, Code, PrintText, Status FROM City", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            City city = new City();
                            city.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            city.StateId = new State(reader.IsDBNull(1) ? -1 :  reader.GetInt32(1));
                            city.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            city.Code = reader.IsDBNull(3) ? null : reader.GetString(3);
                            city.PrintText = reader.IsDBNull(4) ? null : reader.GetString(4);
                            city.Status = reader.IsDBNull(5) ? false : reader.GetBoolean(5);
                            cities.Add(city);
                        }
                    }
	            }
                return cities;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public City getCityById(SqlConnection connection, Int32 cityId)
        {
            log.Info("Entering into CityDAO getCityById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, StateId, Name, Code, PrintText, Status FROM City WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = cityId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            City city = new City();
                            city.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            city.StateId = new State(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            city.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            city.Code = reader.IsDBNull(3) ? null : reader.GetString(3);
                            city.PrintText = reader.IsDBNull(4) ? null : reader.GetString(4);
                            city.Status = reader.IsDBNull(5) ? false : reader.GetBoolean(5);
                            return city;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
