﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;

namespace PSMS.DAL
{
    public class ItemDAO
    {
        private ILogService log = new LogService(typeof(ItemDAO));

        public ItemDAO()
        {
            log.Info("Entering into ItemDAO Default Constructor");
        }

        public ICollection<Item> getAllItems(SqlConnection connection)
        {
            log.Info("Entering into ItemDAO getAllItems Method");
            ICollection<Item> items = new List<Item>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, ItemTypeId, Name, Code, Display, PrintText, Status FROM Item", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Item item = new Item();
                            item.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            item.ItemTypeId = new ItemType(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            item.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            item.Code = reader.IsDBNull(3) ? null : reader.GetString(3);
                            item.Display = reader.IsDBNull(4) ? null : reader.GetString(4);
                            item.PrintText = reader.IsDBNull(5) ? null : reader.GetString(5);
                            item.Status = reader.IsDBNull(6) ? false : reader.GetBoolean(6);
                            items.Add(item);
                        }
                    }
	            }
                return items;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Item getItemById(SqlConnection connection, Int32 itemId)
        {
            log.Info("Entering into ItemDAO getItemById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, ItemTypeId, Name, Code, Display, PrintText, Status FROM Item WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = itemId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Item item = new Item();
                            item.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            item.ItemTypeId = new ItemType(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            item.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            item.Code = reader.IsDBNull(3) ? null : reader.GetString(3);
                            item.Display = reader.IsDBNull(4) ? null : reader.GetString(4);
                            item.PrintText = reader.IsDBNull(5) ? null : reader.GetString(5);
                            item.Status = reader.IsDBNull(6) ? false : reader.GetBoolean(6);
                            return item;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
