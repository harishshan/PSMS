﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;

namespace PSMS.DAL
{
    public class FieldTypeDAO
    {
        private ILogService log = new LogService(typeof(FieldTypeDAO));

        public FieldTypeDAO()
        {
            log.Info("Entering into FieldTypeDAO Default Constructor");
        }

        public ICollection<FieldType> getAllFieldTypes(SqlConnection connection)
        {
            log.Info("Entering into FieldTypeDAO getAllFieldTypes Method");
            ICollection<FieldType> fieldTypes = new List<FieldType>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM FieldType", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FieldType fieldType = new FieldType();
                            fieldType.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            fieldType.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            fieldType.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            fieldTypes.Add(fieldType);
                        }
                    }
	            }
                return fieldTypes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldTypeDAO getAllFieldTypes Method");
            }
        }

        public FieldType getFieldTypeById(SqlConnection connection, Int32 fieldTypeId)
        {
            log.Info("Entering into FieldTypeDAO getFieldTypeById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM FieldType WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = fieldTypeId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            FieldType fieldType = new FieldType();
                            fieldType.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            fieldType.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            fieldType.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            return fieldType;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldTypeDAO getFieldTypeById Method");
            }
        }       
    }
}
