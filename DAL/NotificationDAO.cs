﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class NotificationDAO
    {
        private ILogService log = new LogService(typeof(NotificationDAO));

        public NotificationDAO()
        {
            log.Info("Entering into NotificationDAO Default Constructor");
        }

        public ICollection<Notification> getAllNotifications(SqlConnection connection)
        {
            log.Info("Entering into NotificationDAO getAllNotifications Method");
            ICollection<Notification> notifications = new List<Notification>();
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, SMS, Email, Notice FROM Notification", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Notification notification = new Notification();
                            notification.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            notification.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            notification.SMS = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            notification.Email = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            notification.Notice = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                            notifications.Add(notification);
                        }
                    }
                }
                return notifications;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Notification getNotificationById(SqlConnection connection, Int32 notificationId)
        {
            log.Info("Entering into NotificationDAO getNotificationById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, SMS, Email, Notice FROM Notification WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = notificationId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Notification notification = new Notification();
                            notification.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            notification.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            notification.SMS = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            notification.Email = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            notification.Notice = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                            return notification;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}