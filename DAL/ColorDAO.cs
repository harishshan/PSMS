﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;

namespace PSMS.DAL
{
    public class ColorDAO
    {
        private ILogService log = new LogService(typeof(ColorDAO));

        public ColorDAO()
        {
            log.Info("Entering into ColorDAO Default Constructor");
        }

        public ICollection<Color> getAllColors(SqlConnection connection)
        {
            log.Info("Entering into ColorDAO getAllColors Method");
            ICollection<Color> colors = new List<Color>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Code, Status FROM Color", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Color color = new Color();
                            color.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            color.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            color.Code = reader.IsDBNull(2) ? null : reader.GetString(2);
                            color.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            colors.Add(color);
                        }
                    }
	            }
                return colors;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Color getColorById(SqlConnection connection, Int32 colorId)
        {
            log.Info("Entering into ColorDAO getColorById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Code, Status FROM Color WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = colorId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Color color = new Color();
                            color.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            color.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            color.Code = reader.IsDBNull(2) ? null : reader.GetString(2);
                            color.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            return color;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}