﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Data;

namespace PSMS.DAL
{
    public class CoreUserDAO
    {
        private ILogService log = new LogService(typeof(CoreUserDAO));

        public CoreUserDAO()
        {
            log.Info("Entering into CoreUserDAO Default Constructor");
        }

        public ICollection<CoreUser> getAllCoreUsers(SqlConnection connection)
        {
            log.Info("Entering into CoreUserDAO getAllCoreUsers Method");
            ICollection<CoreUser> coreUsers = new List<CoreUser>();            
            try
            {
                StringBuilder queryBuilder = new StringBuilder("SELECT Id, RoleId, CompanyId, Username, Display, PrintText, Password, LastPassword, PasswordFiledCount, FirstName, ");
                queryBuilder.Append("LastName, GenderId, DOB, Photo, Thumb, LanguageId, ThemeId, ColorId, CreatedBy, CreatedDate, Status FROM CoreUser");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CoreUser coreUser = new CoreUser();
                            coreUser.Id = reader.IsDBNull(1) ? -1 : reader.GetInt32(0);
                            coreUser.RoleId = new Role(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            coreUser.CompanyId = new Company(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            coreUser.Username = reader.IsDBNull(3) ? null : reader.GetString(3);
                            coreUser.Display = reader.IsDBNull(4) ? null : reader.GetString(4);
                            coreUser.PrintText = reader.IsDBNull(5) ? null : reader.GetString(5);
                            coreUser.Password = reader.IsDBNull(6) ? null : reader.GetString(6);
                            coreUser.LastPassword = reader.IsDBNull(7) ? null : reader.GetString(7);
                            coreUser.PasswordFailedCount = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                            coreUser.Firstname = reader.IsDBNull(9) ? null : reader.GetString(9);
                            coreUser.Lastname = reader.IsDBNull(10) ? null : reader.GetString(10);
                            coreUser.GenderId = new Gender(reader.IsDBNull(11) ? -1 : reader.GetInt32(11));
                            coreUser.DOB = reader.IsDBNull(12) ? DateTime.MinValue : reader.GetDateTime(12);
                            coreUser.Photo = reader.IsDBNull(13) ? null : reader.GetSqlBinary(13).Value;
                            coreUser.Thumb = reader.IsDBNull(14) ? null : reader.GetSqlBinary(14).Value;
                            coreUser.LanguageId = new Language(reader.IsDBNull(15) ? -1 : reader.GetInt32(15));
                            coreUser.ThemeId = new Theme(reader.IsDBNull(16) ? -1 : reader.GetInt32(16));
                            coreUser.ColorId = new Color(reader.IsDBNull(17) ? -1 : reader.GetInt32(17));
                            coreUser.CreatedBy = reader.IsDBNull(18) ? null : reader.GetString(18);
                            coreUser.CreateDate = reader.IsDBNull(19) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(19);
                            coreUser.Status = reader.IsDBNull(20) ? false : reader.GetBoolean(20);
                            coreUsers.Add(coreUser);
                        }
                    }
	            }
                return coreUsers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CoreUserDAO getAllCoreUsers Method");
            }
        }

        public CoreUser getCoreUserById(SqlConnection connection, Int32 userId)
        {
            log.Info("Entering into CoreUserDAO getCoreUserById Method");
            try
            {
                StringBuilder queryBuilder = new StringBuilder("SELECT Id, RoleId, CompanyId, Username, Display, PrintText, Password, LastPassword, PasswordFiledCount, FirstName, ");
                queryBuilder.Append("LastName, GenderId, DOB, Photo, Thumb, LanguageId, ThemeId, ColorId, CreatedBy, CreatedDate, Status FROM CoreUser WHERE Id = @Id");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = userId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            CoreUser coreUser = new CoreUser();
                            coreUser.Id = reader.IsDBNull(1) ? -1 : reader.GetInt32(0);
                            coreUser.RoleId = new Role(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            coreUser.CompanyId = new Company(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            coreUser.Username = reader.IsDBNull(3) ? null : reader.GetString(3);
                            coreUser.Display = reader.IsDBNull(4) ? null : reader.GetString(4);
                            coreUser.PrintText = reader.IsDBNull(5) ? null : reader.GetString(5);
                            coreUser.Password = reader.IsDBNull(6) ? null : reader.GetString(6);
                            coreUser.LastPassword = reader.IsDBNull(7) ? null : reader.GetString(7);
                            coreUser.PasswordFailedCount = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                            coreUser.Firstname = reader.IsDBNull(9) ? null : reader.GetString(9);
                            coreUser.Lastname = reader.IsDBNull(10) ? null : reader.GetString(10);
                            coreUser.GenderId = new Gender(reader.IsDBNull(11) ? -1 : reader.GetInt32(11));
                            coreUser.DOB = reader.IsDBNull(12) ? DateTime.MinValue : reader.GetDateTime(12);
                            coreUser.Photo = reader.IsDBNull(13) ? null : reader.GetSqlBinary(13).Value;
                            coreUser.Thumb = reader.IsDBNull(14) ? null : reader.GetSqlBinary(14).Value;
                            coreUser.LanguageId = new Language(reader.IsDBNull(15) ? -1 : reader.GetInt32(15));
                            coreUser.ThemeId = new Theme(reader.IsDBNull(16) ? -1 : reader.GetInt32(16));
                            coreUser.ColorId = new Color(reader.IsDBNull(17) ? -1 : reader.GetInt32(17));
                            coreUser.CreatedBy = reader.IsDBNull(18) ? null : reader.GetString(18);
                            coreUser.CreateDate = reader.IsDBNull(19) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(19);
                            coreUser.Status = reader.IsDBNull(20) ? false : reader.GetBoolean(20);
                            return coreUser;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CoreUserDAO getCoreUserById Method");
            }
        }

        public CoreUser getCoreUserByUsername(SqlConnection connection, String Username)
        {
            log.Info("Entering into CoreUserDAO getCoreUserByUsername Method");
            try
            {
                StringBuilder queryBuilder = new StringBuilder("SELECT Id, RoleId, CompanyId, Username, Display, PrintText, Password, LastPassword, PasswordFiledCount, FirstName, ");
                queryBuilder.Append("LastName, GenderId, DOB, Photo, Thumb, LanguageId, ThemeId, ColorId, CreatedBy, CreatedDate, Status FROM CoreUser WHERE Username = @Username");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    SqlParameter idParam = new SqlParameter("@Username", SqlDbType.VarChar,50 );
                    idParam.Value = Username;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            CoreUser coreUser = new CoreUser();
                            coreUser.Id = reader.IsDBNull(1) ? -1 : reader.GetInt32(0);
                            coreUser.RoleId = new Role(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            coreUser.CompanyId = new Company(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            coreUser.Username = reader.IsDBNull(3) ? null : reader.GetString(3);
                            coreUser.Display = reader.IsDBNull(4) ? null : reader.GetString(4);
                            coreUser.PrintText = reader.IsDBNull(5) ? null : reader.GetString(5);
                            coreUser.Password = reader.IsDBNull(6) ? null : reader.GetString(6);
                            coreUser.LastPassword = reader.IsDBNull(7) ? null : reader.GetString(7);
                            coreUser.PasswordFailedCount = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                            coreUser.Firstname = reader.IsDBNull(9) ? null : reader.GetString(9);
                            coreUser.Lastname = reader.IsDBNull(10) ? null : reader.GetString(10);
                            coreUser.GenderId = new Gender(reader.IsDBNull(11) ? -1 : reader.GetInt32(11));
                            coreUser.DOB = reader.IsDBNull(12) ? DateTime.MinValue : reader.GetDateTime(12);
                            coreUser.Photo = reader.IsDBNull(13) ? null : reader.GetSqlBinary(13).Value;
                            coreUser.Thumb = reader.IsDBNull(14) ? null : reader.GetSqlBinary(14).Value;
                            coreUser.LanguageId = new Language(reader.IsDBNull(15) ? -1 : reader.GetInt32(15));
                            coreUser.ThemeId = new Theme(reader.IsDBNull(16) ? -1 : reader.GetInt32(16));
                            coreUser.ColorId = new Color(reader.IsDBNull(17) ? -1 : reader.GetInt32(17));
                            coreUser.CreatedBy = reader.IsDBNull(18) ? null : reader.GetString(18);
                            coreUser.CreateDate = reader.IsDBNull(19) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(19);
                            coreUser.Status = reader.IsDBNull(20) ? false : reader.GetBoolean(20);
                            return coreUser;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CoreUserDAO getCoreUserByUsername Method");
            }
        }
        public void updateLoginFailedCount(SqlConnection connection, String Username)
        {
            log.Info("Entering into CoreUserDAO updateLoginFailedCount Method");
            try
            {
                StringBuilder queryBuilder = new StringBuilder("UPDATE CoreUser set PasswordFiledCount =  PasswordFiledCount + 1 WHERE Username = @Username");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    SqlParameter idParam = new SqlParameter("@Username", SqlDbType.VarChar);
                    idParam.Value = Username;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    int updatedRowCount = command.ExecuteNonQuery();
                    log.Info("No of CoreUser Records updated:"+updatedRowCount);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CoreUserDAO updateLoginFailedCount Method");
            }
        }
    }
}
