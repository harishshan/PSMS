﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class ModuleDAO
    {
        private ILogService log = new LogService(typeof(ModuleDAO));

        public ModuleDAO()
        {
            log.Info("Entering into ModuleDAO Default Constructor");
        }

        public ICollection<Module> getAllModules(SqlConnection connection)
        {
            log.Info("Entering into ModuleDAO getAllModules Method");
            ICollection<Module> modules = new List<Module>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM Module", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Module module = new Module();
                            module.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            module.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            module.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            modules.Add(module);
                        }
                    }
	            }
                return modules;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Module getModuleById(SqlConnection connection, Int32 moduleId)
        {
            log.Info("Entering into ModuleDAO getModuleById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM Module WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = moduleId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Module module = new Module();
                            module.Id = reader.GetInt32(0);
                            module.Name = reader.GetString(1);
                            module.Status = reader.GetBoolean(2);
                            return module;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
