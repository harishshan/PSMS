﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class ProofTypeDAO
    {
        private ILogService log = new LogService(typeof(ProofTypeDAO));

        public ProofTypeDAO()
        {
            log.Info("Entering into ProofTypeDAO Default Constructor");
        }

        public ICollection<ProofType> getAllProofTypes(SqlConnection connection)
        {
            log.Info("Entering into ProofTypeDAO getAllProofTypes Method");
            ICollection<ProofType> proofTypes = new List<ProofType>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM ProofType", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ProofType proofType = new ProofType();
                            proofType.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            proofType.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            proofType.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            proofTypes.Add(proofType);
                        }
                    }
	            }
                return proofTypes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProofType getProofTypeById(SqlConnection connection, Int32 proofTypeId)
        {
            log.Info("Entering into ProofTypeDAO getProofTypeById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM ProofType WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = proofTypeId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            ProofType proofType = new ProofType();
                            proofType.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            proofType.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            proofType.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            return proofType;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
