﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using PSMS.Entities;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class JewelDAO
    {
        private ILogService log = new LogService(typeof(JewelDAO));

        public JewelDAO()
        {
            log.Info("Entering into JewelDAO Default Constructor");
        }

        public ICollection<Jewel> getAllJewels(SqlConnection connection)
        {
            log.Info("Entering into JewelDAO getAllJewels Method");
            ICollection<Jewel> jewels = new List<Jewel>();            
            try
            {
                StringBuilder queryBuilder = new StringBuilder("SELECT Id, LoanId, ItemTypeId, ItemId, Description, JewelStateId, Quantity, ")
                .Append("NetWeight, GrossWeight, MarketValueAmount, LoanAmount, Purity, HoldDate, ReleaseDate, HoldStaffId, ReleaseStaffId From Jewel");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Jewel jewel = new Jewel();
                            jewel.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            jewel.LoanId = new Loan(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            jewel.ItemTypeId = new ItemType(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            jewel.ItemId = new Item(reader.IsDBNull(3) ? -1 : reader.GetInt32(3));
                            jewel.Description = reader.IsDBNull(4) ? null : reader.GetString(4);
                            jewel.JewelStateId = new JewelState(reader.IsDBNull(5) ? -1 : reader.GetInt32(5));
                            jewel.Quantity = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                            jewel.NetWeight = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                            jewel.GrossWeight = reader.IsDBNull(8) ? 0 : reader.GetDecimal(8);
                            jewel.MarketValueAmount = reader.IsDBNull(9) ? 0 : reader.GetDecimal(9);
                            jewel.LoanAmount = reader.IsDBNull(10) ? 0 : reader.GetDecimal(10);
                            jewel.Purity = reader.IsDBNull(11) ? 0 : reader.GetDecimal(11);
                            jewel.HoldDate = reader.IsDBNull(12) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(12);
                            jewel.ReleaseDate = reader.IsDBNull(13) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(13);
                            jewel.HoldStaffId = new CoreUser(reader.IsDBNull(14) ? -1 : reader.GetInt32(14));
                            jewel.ReleaseStaffId = new CoreUser(reader.IsDBNull(15) ? -1 : reader.GetInt32(15));
                            jewels.Add(jewel);
                        }
                    }
	            }
                return jewels;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Jewel getJewelById(SqlConnection connection, Int32 jewelId)
        {
            log.Info("Entering into JewelDAO getJewelById Method");
            try
            {
                StringBuilder queryBuilder = new StringBuilder("SELECT Id, LoanId, ItemTypeId, ItemId, Description, JewelStateId, Quantity, ")
                .Append("NetWeight, GrossWeight, MarketValueAmount, LoanAmount, Purity, HoldDate, ReleaseDate, HoldStaffId, ReleaseStaffId From Jewel WHERE Id = @Id");
                using (SqlCommand command = new SqlCommand(queryBuilder.ToString(), connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = jewelId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Jewel jewel = new Jewel();
                            jewel.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            jewel.LoanId = new Loan(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            jewel.ItemTypeId = new ItemType(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            jewel.ItemId = new Item(reader.IsDBNull(3) ? -1 : reader.GetInt32(3));
                            jewel.Description = reader.IsDBNull(4) ? null : reader.GetString(4);
                            jewel.JewelStateId = new JewelState(reader.IsDBNull(5) ? -1 : reader.GetInt32(5));
                            jewel.Quantity = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                            jewel.NetWeight = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                            jewel.GrossWeight = reader.IsDBNull(8) ? 0 : reader.GetDecimal(8);
                            jewel.MarketValueAmount = reader.IsDBNull(9) ? 0 : reader.GetDecimal(9);
                            jewel.LoanAmount = reader.IsDBNull(10) ? 0 : reader.GetDecimal(10);
                            jewel.Purity = reader.IsDBNull(11) ? 0 : reader.GetDecimal(11);
                            jewel.HoldDate = reader.IsDBNull(12) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(12);
                            jewel.ReleaseDate = reader.IsDBNull(13) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(13);
                            jewel.HoldStaffId = new CoreUser(reader.IsDBNull(14) ? -1 : reader.GetInt32(14));
                            jewel.ReleaseStaffId = new CoreUser(reader.IsDBNull(15) ? -1 : reader.GetInt32(15));
                            return jewel;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
