﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using System.Data.SqlClient;
using PSMS.Entities;
using System.Data;

namespace PSMS.DAL
{
    public class CountryDAO
    {
        private ILogService log = new LogService(typeof(CountryDAO));

        public CountryDAO()
        {
            log.Info("Entering into CountryDAO Default Constructor");
        }

        public ICollection<Country> getAllCountries(SqlConnection connection)
        {
            log.Info("Entering into CountryDAO getAllCountries Method");
            ICollection<Country> countries = new List<Country>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Code, Status FROM Country", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Country country = new Country();
                            country.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            country.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            country.Code = reader.IsDBNull(2) ? null : reader.GetString(2);
                            country.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            countries.Add(country);
                        }
                    }
	            }
                return countries;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Country getCountryById(SqlConnection connection, Int32 countryId)
        {
            log.Info("Entering into CountryDAO getCountryById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Code, Status FROM Country WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = countryId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Country country = new Country();
                            country.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            country.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            country.Code = reader.IsDBNull(2) ? null : reader.GetString(2);
                            country.Status = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                            return country;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
