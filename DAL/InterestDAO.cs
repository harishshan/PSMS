﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;

namespace PSMS.DAL
{
    public class InterestDAO
    {
        private ILogService log = new LogService(typeof(InterestDAO));

        public InterestDAO()
        {
            log.Info("Entering into InterestDAO Default Constructor");
        }

        public ICollection<Interest> getAllInterests(SqlConnection connection)
        {
            log.Info("Entering into InterestDAO getAllInterests Method");
            ICollection<Interest> interests = new List<Interest>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, ItemTypeGroupId, InterestTypeId, MonthFrom, MonthTo, ROI, Status FROM Interest", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Interest interest = new Interest();
                            interest.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            interest.ItemTypeGroupId = new ItemTypeGroup(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            interest.InterestTypeId = new InterestType(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            interest.MonthFrom = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                            interest.MonthTO = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                            interest.ROI = reader.IsDBNull(5) ? 0 : reader.GetDecimal(5);
                            interest.Status = reader.IsDBNull(6) ? false : reader.GetBoolean(6);
                            interests.Add(interest);
                        }
                    }
	            }
                return interests;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Interest getInterestById(SqlConnection connection, Int32 interestId)
        {
            log.Info("Entering into InterestDAO getInterestById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, ItemTypeGroupId, InterestTypeId, MonthFrom, MonthTo, ROI, Status FROM Interest WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = interestId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Interest interest = new Interest();
                            interest.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            interest.ItemTypeGroupId = new ItemTypeGroup(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            interest.InterestTypeId = new InterestType(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            interest.MonthFrom = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                            interest.MonthTO = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                            interest.ROI = reader.IsDBNull(5) ? 0 : reader.GetDecimal(5);
                            interest.Status = reader.IsDBNull(6) ? false : reader.GetBoolean(6);
                            return interest;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
