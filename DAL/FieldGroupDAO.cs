﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using PSMS.Entities;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class FieldGroupDAO
    {
        private ILogService log = new LogService(typeof(FieldGroupDAO));

        public FieldGroupDAO()
        {
            log.Info("Entering into FieldGroupDAO Default Constructor");
        }

        public ICollection<FieldGroup> getAllFieldGroups(SqlConnection connection)
        {
            log.Info("Entering into FieldGroupDAO getAllFieldGroups Method");
            ICollection<FieldGroup> fieldGroups = new List<FieldGroup>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, PageId, Name, Sort, Status FROM FieldGroup", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FieldGroup fieldGroup = new FieldGroup();
                            fieldGroup.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            fieldGroup.PageId = new Page(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            fieldGroup.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            fieldGroup.Sort = reader.IsDBNull(3) ? -1 : reader.GetInt32(3);
                            fieldGroup.Status = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                            fieldGroups.Add(fieldGroup);
                        }
                    }
	            }
                return fieldGroups;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupDAO getAllFieldGroups Method");
            }
        }

        public FieldGroup getFieldGroupById(SqlConnection connection, Int32 fieldGroupId)
        {
            log.Info("Entering into FieldGroupDAO getFieldGroupById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, PageId, Name, Sort, Status FROM FieldGroup WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = fieldGroupId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            FieldGroup fieldGroup = new FieldGroup();
                            fieldGroup.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            fieldGroup.PageId = new Page(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            fieldGroup.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            fieldGroup.Sort = reader.IsDBNull(3) ? -1 : reader.GetInt32(3);
                            fieldGroup.Status = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                            return fieldGroup;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupDAO getFieldGroupById Method");
            }
        }
        public ICollection<FieldGroup> getFieldGroupsByPageId(SqlConnection connection, Int32 PageId)
        {
            log.Info("Entering into FieldGroupDAO getFieldGroupsByPageId Method");
            ICollection<FieldGroup> fieldGroups = new List<FieldGroup>(); 
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, PageId, Name, Sort, Status FROM FieldGroup WHERE PageId = @PageId", connection))
                {
                    SqlParameter idParam = new SqlParameter("@PageId", SqlDbType.Int);
                    idParam.Value = PageId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FieldGroup fieldGroup = new FieldGroup();
                            fieldGroup.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            fieldGroup.PageId = new Page(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            fieldGroup.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            fieldGroup.Sort = reader.IsDBNull(3) ? -1 : reader.GetInt32(3);
                            fieldGroup.Status = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                            fieldGroups.Add(fieldGroup);
                        }
                    }

                }
                return fieldGroups;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupDAO getFieldGroupsByPageId Method");
            }
        }
        public ICollection<FieldGroup> getFieldGroupsSortedByPageId(SqlConnection connection, Int32 PageId)
        {
            log.Info("Entering into FieldGroupDAO getFieldGroupsSortedByPageId Method");
            ICollection<FieldGroup> fieldGroups = new List<FieldGroup>();
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, PageId, Name, Sort, Status FROM FieldGroup WHERE PageId = @PageId ORDER BY Sort", connection))
                {
                    SqlParameter idParam = new SqlParameter("@PageId", SqlDbType.Int);
                    idParam.Value = PageId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FieldGroup fieldGroup = new FieldGroup();
                            fieldGroup.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            fieldGroup.PageId = new Page(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            fieldGroup.Name = reader.IsDBNull(2) ? null : reader.GetString(2);
                            fieldGroup.Sort = reader.IsDBNull(3) ? -1 : reader.GetInt32(3);
                            fieldGroup.Status = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                            fieldGroups.Add(fieldGroup);
                        }
                    }

                }
                return fieldGroups;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from FieldGroupDAO getFieldGroupsSortedByPageId Method");
            }
        }

    }
}
