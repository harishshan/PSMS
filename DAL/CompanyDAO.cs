﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using PSMS.Log4NetLibrary;
using System.Data;

namespace PSMS.DAL
{
    public class CompanyDAO
    {
        private ILogService log = new LogService(typeof(CompanyDAO));

        public CompanyDAO()
        {
            log.Info("Entering into CompanyDAO Default Constructor");
        }
        public ICollection<Company> getAllCompanies(SqlConnection connection)
        {
            log.Info("Entering into CompanyDAO getAllCompanies Method");
            ICollection<Company> companies = new List<Company>();            
            try
            {
                StringBuilder commandBuilder = new StringBuilder();
                commandBuilder.Append("SELECT Id, Name, Display, PrintText, Address, AddressDisplay, AddressPrint, CityId, StateId, CountryId, PINCode, PhoneNumber, FaxNumber");
                commandBuilder.Append(", EmailId, Website, Logo, LicenceNumber, DBHost, DBName, DBUsername, DBPassword, DBAuthenticationTypeId, CurrencyId, LoanPrefix");
                commandBuilder.Append(", LoanPostfix, RepayPrefix, RepayPostfix, ReceiptHeader, ReceiptFooter, Signature, CloudBackupLocation, CloudUsername, CloudPassword");
                commandBuilder.Append(", LocalBackupLocation, MatureMonths, GraceDays, AlertJob, BackupJob, LanguageId, ThemeId, ColorId, CreatedBy, CreatedDate, Status FROM Company");
                using (SqlCommand command = new SqlCommand(commandBuilder.ToString(),connection))     
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Company company = new Company();
                            company.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            company.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            company.Display = reader.IsDBNull(2) ? null : reader.GetString(2);
                            company.PrintText = reader.IsDBNull(3) ? null : reader.GetString(3);
                            company.Address = reader.IsDBNull(4) ? null : reader.GetString(4);
                            company.AddressDisplay = reader.IsDBNull(5) ? null : reader.GetString(5);
                            company.AddressPrint = reader.IsDBNull(6) ? null : reader.GetString(6);
                            company.CityId = new City(reader.IsDBNull(7) ? -1 : reader.GetInt32(7));
                            company.StateId = new State(reader.IsDBNull(8) ? -1 : reader.GetInt32(8));
                            company.CountryId = new Country(reader.IsDBNull(9) ? -1 : reader.GetInt32(9));
                            company.PINCode = reader.IsDBNull(10) ? null : reader.GetString(10);
                            company.PhoneNumber = reader.IsDBNull(11) ? null : reader.GetString(11);
                            company.FaxNumber = reader.IsDBNull(12) ? null : reader.GetString(12);
                            company.EmailId = reader.IsDBNull(13) ? null : reader.GetString(13);
                            company.Website = reader.IsDBNull(14) ? null : reader.GetString(14);
                            company.Logo = reader.IsDBNull(15) ? null : reader.GetSqlBinary(15).Value;
                            company.LicenceNumber = reader.IsDBNull(16) ? null : reader.GetString(16);
                            company.DBHost = reader.IsDBNull(17) ? null : reader.GetString(17);
                            company.DBName = reader.IsDBNull(18) ? null : reader.GetString(18);
                            company.DBUsername = reader.IsDBNull(19) ? null : reader.GetString(19);
                            company.DBPassword = reader.IsDBNull(20) ? null : reader.GetString(20);
                            company.DBAuthenticationTypeId = new DBAuthenticationType(reader.IsDBNull(21) ? -1 : reader.GetInt32(21));
                            company.CurrencyId = new Currency(reader.IsDBNull(22) ? -1 : reader.GetInt32(22));
                            company.LoanPrefix = reader.IsDBNull(23) ? null : reader.GetString(23);
                            company.LoanPostfix = reader.IsDBNull(24) ? null : reader.GetString(24);
                            company.RepayPrefix = reader.IsDBNull(25) ? null : reader.GetString(25);
                            company.RepayPostfix = reader.IsDBNull(26) ? null : reader.GetString(26);
                            company.ReceiptHeader = reader.IsDBNull(27) ? null : reader.GetSqlBinary(27).Value;
                            company.ReceiptFooter = reader.IsDBNull(28) ? null : reader.GetSqlBinary(28).Value;
                            company.Signature = reader.IsDBNull(29) ? null : reader.GetSqlBinary(29).Value;
                            company.CloudBackupLocation = reader.IsDBNull(30) ? null : reader.GetString(30);
                            company.CloudUsername = reader.IsDBNull(31) ? null : reader.GetString(31);
                            company.CloudPassword = reader.IsDBNull(32) ? null : reader.GetString(32);
                            company.LocalBackupLocation = reader.IsDBNull(33) ? null : reader.GetString(33);
                            company.MatureMonths = reader.IsDBNull(34) ? 0 : reader.GetInt32(34);
                            company.GraceDays = reader.IsDBNull(35) ? 0 : reader.GetInt32(35);
                            company.AlertJob = reader.IsDBNull(36) ? false : reader.GetBoolean(36);
                            company.BackupJob = reader.IsDBNull(37) ? false : reader.GetBoolean(37);
                            company.LanguageId = new Language(reader.IsDBNull(38) ? -1 : reader.GetInt32(38));
                            company.ThemeId = new Theme(reader.IsDBNull(39) ? -1 : reader.GetInt32(39));
                            company.ColorId = new Color(reader.IsDBNull(40) ? -1 : reader.GetInt32(40));
                            company.CreatedBy = reader.IsDBNull(41) ? null : reader.GetString(41);
                            company.CreatedDate = reader.IsDBNull(42) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(42);
                            company.Status = reader.IsDBNull(43) ? false : reader.GetBoolean(43);
                            companies.Add(company);
                        }
                    }
	            }
                return companies;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CompanyBO getCompanyById Method");
            } 
        }

        public Company getCompanyById(SqlConnection connection, Int32 companyId)
        {
            log.Info("Entering into CompanyBO getCompanyById Method");
            try
            {
                StringBuilder commandBuilder = new StringBuilder();
                commandBuilder.Append("SELECT Id, Name, Display, PrintText, Address, AddressDisplay, AddressPrint, CityId, StateId, CountryId, PINCode, PhoneNumber, FaxNumber");
                commandBuilder.Append(", EmailId, Website, Logo, LicenceNumber, DBHost, DBName, DBUsername, DBPassword, DBAuthenticationTypeId, CurrencyId, LoanPrefix");
                commandBuilder.Append(", LoanPostfix, RepayPrefix, RepayPostfix, ReceiptHeader, ReceiptFooter, Signature, CloudBackupLocation, CloudUsername, CloudPassword");
                commandBuilder.Append(", LocalBackupLocation, MatureMonths, GraceDays, AlertJob, BackupJob, LanguageId, ThemeId, ColorId, CreatedBy, CreatedDate, Status FROM Company WHERE  Id = @Id and Status = 1");
                using (SqlCommand command = new SqlCommand(commandBuilder.ToString(), connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = companyId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Company company = new Company();
                            company.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            company.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            company.Display = reader.IsDBNull(2) ? null : reader.GetString(2);
                            company.PrintText = reader.IsDBNull(3) ? null : reader.GetString(3);
                            company.Address = reader.IsDBNull(4) ? null : reader.GetString(4);
                            company.AddressDisplay = reader.IsDBNull(5) ? null : reader.GetString(5);
                            company.AddressPrint = reader.IsDBNull(6) ? null : reader.GetString(6);
                            company.CityId = new City(reader.IsDBNull(7) ? -1 : reader.GetInt32(7));
                            company.StateId = new State(reader.IsDBNull(8) ? -1 : reader.GetInt32(8));
                            company.CountryId = new Country(reader.IsDBNull(9) ? -1 : reader.GetInt32(9));
                            company.PINCode = reader.IsDBNull(10) ? null : reader.GetString(10);
                            company.PhoneNumber = reader.IsDBNull(11) ? null : reader.GetString(11);
                            company.FaxNumber = reader.IsDBNull(12) ? null : reader.GetString(12);
                            company.EmailId = reader.IsDBNull(13) ? null : reader.GetString(13);
                            company.Website = reader.IsDBNull(14) ? null : reader.GetString(14);
                            company.Logo = reader.IsDBNull(15) ? null : reader.GetSqlBinary(15).Value;
                            company.LicenceNumber = reader.IsDBNull(16) ? null : reader.GetString(16);
                            company.DBHost = reader.IsDBNull(17) ? null : reader.GetString(17);
                            company.DBName = reader.IsDBNull(18) ? null : reader.GetString(18);
                            company.DBUsername = reader.IsDBNull(19) ? null : reader.GetString(19);
                            company.DBPassword = reader.IsDBNull(20) ? null : reader.GetString(20);
                            company.DBAuthenticationTypeId = new DBAuthenticationType(reader.IsDBNull(21) ? -1 : reader.GetInt32(21));
                            company.CurrencyId = new Currency(reader.IsDBNull(22) ? -1 : reader.GetInt32(22));
                            company.LoanPrefix = reader.IsDBNull(23) ? null : reader.GetString(23);
                            company.LoanPostfix = reader.IsDBNull(24) ? null : reader.GetString(24);
                            company.RepayPrefix = reader.IsDBNull(25) ? null : reader.GetString(25);
                            company.RepayPostfix = reader.IsDBNull(26) ? null : reader.GetString(26);
                            company.ReceiptHeader = reader.IsDBNull(27) ? null : reader.GetSqlBinary(27).Value;
                            company.ReceiptFooter = reader.IsDBNull(28) ? null : reader.GetSqlBinary(28).Value;
                            company.Signature = reader.IsDBNull(29) ? null : reader.GetSqlBinary(29).Value;
                            company.CloudBackupLocation = reader.IsDBNull(30) ? null : reader.GetString(30);
                            company.CloudUsername = reader.IsDBNull(31) ? null : reader.GetString(31);
                            company.CloudPassword = reader.IsDBNull(32) ? null : reader.GetString(32);
                            company.LocalBackupLocation = reader.IsDBNull(33) ? null : reader.GetString(33);
                            company.MatureMonths = reader.IsDBNull(34) ? 0 : reader.GetInt32(34);
                            company.GraceDays = reader.IsDBNull(35) ? 0 : reader.GetInt32(35);
                            company.AlertJob = reader.IsDBNull(36) ? false : reader.GetBoolean(36);
                            company.BackupJob = reader.IsDBNull(37) ? false : reader.GetBoolean(37);
                            company.LanguageId = new Language(reader.IsDBNull(38) ? -1 : reader.GetInt32(38));
                            company.ThemeId = new Theme(reader.IsDBNull(39) ? -1 : reader.GetInt32(39));
                            company.ColorId = new Color(reader.IsDBNull(40) ? -1 : reader.GetInt32(40));
                            company.CreatedBy = reader.IsDBNull(41) ? null : reader.GetString(41);
                            company.CreatedDate = reader.IsDBNull(42) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(42);
                            company.Status = reader.IsDBNull(43) ? false : reader.GetBoolean(43);
                            return company;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                log.Info("Exiting from CompanyBO getCompanyById Method");
            } 
        }    
    }
}