﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class FilterDAO
    {
        private ILogService log = new LogService(typeof(FilterDAO));

        public FilterDAO()
        {
            log.Info("Entering into FilterDAO Default Constructor");
        }

        public ICollection<Filter> getAllFilters(SqlConnection connection)
        {
            log.Info("Entering into FilterDAO getAllFilters Method");
            ICollection<Filter> filters = new List<Filter>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM Filter", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Filter filter = new Filter();
                            filter.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            filter.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            filter.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            filters.Add(filter);
                        }
                    }
	            }
                return filters;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Filter getFilterById(SqlConnection connection, Int32 filterId)
        {
            log.Info("Entering into FilterDAO getFilterById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Status FROM Filter WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = filterId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Filter filter = new Filter();
                            filter.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            filter.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            filter.Status = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                            return filter;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
