﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Log4NetLibrary;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;

namespace PSMS.DAL
{
    public class CurrencyDAO
    {
        private ILogService log = new LogService(typeof(CurrencyDAO));

        public CurrencyDAO()
        {
            log.Info("Entering into CurrencyDAO Default Constructor");
        }

        public ICollection<Currency> getAllCurrencies(SqlConnection connection)
        {
            log.Info("Entering into CurrencyDAO getAllCurrencies Method");
            ICollection<Currency> currencies = new List<Currency>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Code, Display, PrintText, Status FROM Currency", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Currency currency = new Currency();
                            currency.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            currency.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            currency.Code = reader.IsDBNull(2) ? null : reader.GetString(2);
                            currency.Display = reader.IsDBNull(3) ? null : reader.GetString(3);
                            currency.PrintText = reader.IsDBNull(4) ? null : reader.GetString(4);
                            currency.Status = reader.IsDBNull(5) ? false : reader.GetBoolean(5);
                            currencies.Add(currency);
                        }
                    }
	            }
                return currencies;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Currency getCurrencyById(SqlConnection connection, Int32 currencyId)
        {
            log.Info("Entering into CurrencyDAO getCurrencyById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, Name, Code, Display, PrintText, Status FROM Currency WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = currencyId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Currency currency = new Currency();
                            currency.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            currency.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                            currency.Code = reader.IsDBNull(2) ? null : reader.GetString(2);
                            currency.Display = reader.IsDBNull(3) ? null : reader.GetString(3);
                            currency.PrintText = reader.IsDBNull(4) ? null : reader.GetString(4);
                            currency.Status = reader.IsDBNull(5) ? false : reader.GetBoolean(5);
                            return currency;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}