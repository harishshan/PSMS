﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSMS.Entities;
using System.Data.SqlClient;
using System.Data;
using PSMS.Log4NetLibrary;

namespace PSMS.DAL
{
    public class AuthorisationDAO
    {
        private ILogService log = new LogService(typeof(AuthorisationDAO));

        public AuthorisationDAO()
        {
            log.Info("Entering into AuthorisationDAO Default Constructor");
        }

        public ICollection<Authorisation> getAllAuthorisations(SqlConnection connection)
        {
            log.Info("Entering into AuthorisationDAO getAllAuthorisations Method");
            ICollection<Authorisation> authorisations = new List<Authorisation>();            
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, RoleId, ModuleId, CreatedDate, Status FROM Authorisation", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Authorisation authorisation = new Authorisation();
                            authorisation.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            authorisation.RoleId = new Role(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            authorisation.ModuleId = new Module(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            authorisation.CreatedDate = reader.IsDBNull(3) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(3);
                            authorisation.Status = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                            authorisations.Add(authorisation);
                        }
                    }
	            }
                return authorisations;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Authorisation getAuthorisationById(SqlConnection connection, Int32 authorisationId)
        {
            log.Info("Entering into AuthorisationDAO getAuthorisationById Method");
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT Id, RoleId, ModuleId, CreatedDate, Status FROM Authorisation WHERE Id = @Id", connection))
                {
                    SqlParameter idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = authorisationId;
                    command.Parameters.Add(idParam);
                    command.Prepare();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Authorisation authorisation = new Authorisation();
                            authorisation.Id = reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                            authorisation.RoleId = new Role(reader.IsDBNull(1) ? -1 : reader.GetInt32(1));
                            authorisation.ModuleId = new Module(reader.IsDBNull(2) ? -1 : reader.GetInt32(2));
                            authorisation.CreatedDate = reader.IsDBNull(3) ? DateTimeOffset.MinValue : reader.GetDateTimeOffset(3);
                            authorisation.Status = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                            return authorisation;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
