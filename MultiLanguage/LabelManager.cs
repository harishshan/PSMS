﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Threading;
using System.Globalization;
using PSMS.Log4NetLibrary;

namespace PSMS.MultiLanguage
{
    public class LabelManager
    {
        private ResourceManager resourceManager;
        ILogService log = new LogService(typeof(LabelManager));
        public LabelManager(Type formClasss, String labelResource)
        {
            resourceManager = new ResourceManager(labelResource, formClasss.Assembly);
        }
        public void SetLanguage(String language)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(language);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
        }
        public String getLabel(String key)
        {
            String value = null;
            try
            {
                log.Debug("Entering into LabelManager getLabel for key:" + key);
                value = resourceManager.GetString(key, Thread.CurrentThread.CurrentCulture);
                return value;
            }
            catch (Exception)
            {
                return key;
            }
            finally
            {
                log.Debug("Exiting from LabelManager getLabel by returning value:" + value + " for key:" + key);
            }
        }
    }
}
